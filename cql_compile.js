'use strict'
/*
   Inside the compiler expressions are handled as a POJO of
   - $ctype
   - js - javascript
   - dependancies {parameters: [], ... }

   At run-time values are handled by executing javascript as pojo's
   with:
   - $type,
   - value,
   - simple (boolean)

   (Subject to change without notice)

*/

// note pojo = 'plain ol' JS object'
var Promise = require('bluebird')
, _ = require('lodash')
, moment = require('moment')
, request = require('request-promise') // bluebird wrapper around request
, expect = require('chai').expect // bluebird wrapper around request
, toJs = JSON.stringify // shortcut for escaping values into valid js
, debug = require('debug')('compiler')
// final export object
var f = {}

var optional_new_line = '\n'

// main compiler
// params
// - library json ELM
// returns
// - promise of a string
f.compile = function (params) {
  var library = params.elm.library
  if (!isKnownSchema(library)) {
    throw new Error('unknown schema')
  }
  // an array of lines to accumate text
  var lines = []
  // index of collections
  var index = {}
  // escape and enrap library name
  var libraryName = idToJs(library.identifier.id)
  lines.push("'use strict';") // this semicolon is needed
  lines.push('(function(exports){' + optional_new_line)
  lines.push('exports.module = {')
  var meta = {}
  meta.id = libraryName
  if (!_.isUndefined(library.identifier.version)) {
    meta.version = idToJs(library.identifier.version)
  }
  lines.push('meta: ' + renderPojo(meta) + ',' + optional_new_line)
  lines.push('constructor: function (system) {')
  lines.push('var r = system.runtime') // custom runtime
  lines.push(', _ = system._') // lodash injected in
  lines.push(', pub = {}') // public methods
  lines.push(', priv = {}') // private (and public) methods
  lines.push(', context = {}') // private context

  // index collections (if they exist)
  function indexBy(collection, key) {
    return collection ? _.keyBy(collection.def, key) : undefined
  }
  index.usings = indexBy(library.usings, 'localIdentifier')
  index.includes = indexBy(library.includes, 'localIdentifier')
  index.codeSystems = indexBy(library.codeSystems, 'name')
  index.valueSets = indexBy(library.valueSets, 'name')
  index.parameters = indexBy(library.parameters, 'name')
  index.statements = indexBy(library.statements, 'name')

  // compile all the statements
  _.forEach(index.statements, function(statement) {
    f.compileStatement(statement, lines, index)
  })

  lines.push('return pub' + optional_new_line)
  lines.push('}}')

  lines.push("})(typeof exports === 'undefined' ? this[" + libraryName
      + ']={} : exports)')
  return Promise.resolve(lines.join('\n'))
}

// debugging counter
var been_here

// translate a statement into a function
// public statements are added to the 'pub' object
// all methods are added to the `priv` libray (to simplify access)
f.compileStatement = function (statement, lines, index) {
  been_here = 0
  var operands = _.keyBy(statement.operand, 'name')
  var params = _.keys(operands).join(',')
  var name = idToJs(statement.name)
  // expose statement if public
  debug('name', name)
  lines.push(statement.accessLevel == 'Public'
    ? 'pub[' + name + '] = '
    : '')
  lines.push('priv[' + name + '] = function(' + params + '){')
  var p = {
    statement: statement,
    index: index,
    expression: statement.expression,
  }
  var expression = f.compileExpression(p)
  lines.push('// type: ' + expression.type)
  lines.push('// deps: ' + toJs(expression.deps))
  lines.push('return (' + expression.js + ')') // brackets need to prevent ASI
  lines.push('}' + optional_new_line)
}

// simple wrapper around compileExpression to merge parameters
// returns object {js, deps, $ctype}
function compileNextExpression(next, p) {
  return f.compileExpression(_.defaults({expression: next}, p))
}

// simple wrapper around compileExpression to handle collections
// returns collection (either {} or []) of {js, deps, $ctype}
function compileExpressionCollection(expressions, p) {
  // make the same type we got in:
  var result = _.isArray(expressions) ? [] : {};
  _.forEach(expressions, function(expression, k) {
    result[k] = compileNextExpression(expression, p)
  })
  return result;
}

// return string (javascript to calculate the exrpession
// walks the tree recursively
// returns object
// js - string javascript
// deps - dependancies on sources and parameters
// $ctype
f.compileExpression = function(p) {
  var result = {}
  , sub
  switch (p.expression.type) {
    case 'SingletonFrom': //
      sub = compileNextExpression(p.expression.operand, p)
      result.$ctype = p.expression.operand.dataType
      result.js = '{$type: ' + idToJs(result.$ctype) +
        ', value: r.SingletonFrom(' + sub.js + ')}'
      result.deps = sub.deps
      break
    case 'Retrieve':
      result = f.compileRetrieve(p)
      break
    case 'Query':
      result = f.compileQuery(p)
      break
    case 'ParameterRef':
      result = {
        js: 'params[' + idToJs(p.expression.name) + ']',
        $ctype: '*****paramtype tbd****',
        deps: {params: p.expression.name}
      }
      break
    case 'Property':
      result = compileProperty(p)
      break
    case 'ExpressionRef':
      result = {
        js: 'priv[' + idToJs(p.expression.name) + ']()',
        $ctype: '*****Expretype tbd****',
        deps: {expression: p.expression.name}
      }
      break
    case 'Literal':
      result = compileLiteral(p)
      break
    case 'ToDecimal': // does this ever happen???
    case 'Convert':
      result = compileCast(p)
      break
    case 'As':
      result = compileAs(p)
      break
    case 'Null':
      result = {
          js: 'null',
          $ctype: 'Null'
        }
      break
    case 'DateTime':
    case 'Time':
      result = renderDateTime(p.expression)
      break
    // has precision:
    case 'Contains': // sends [list, index]
    case 'In':
    case 'Includes': // sends [list, index]
    case 'IncludedIn':
    case 'ProperIn':
    case 'ProperContains':
    case 'ProperIncludes':
    case 'ProperIncludedIn':
    case 'DurationBetween':
    case 'DifferenceBetween':
    case 'Before':
    case 'After':
    case 'SameAs':
    case 'Meets':
    case 'MeetsBefore':
    case 'MeetsAfter':
    case 'SameOrBefore':
    case 'SameOrAfter':
    case 'Overlaps':
    case 'OverlapsBefore':
    case 'OverlapsAfter':
    case 'Starts':
    case 'Ends':
    case 'CalculateAgeAt':
  // has precision
      result = compilePreciseExpression(p, {unary: false})
      break
    case 'DateTimeComponentFrom':
    case 'Round':
  // has precision
      result = compilePreciseExpression(p, {unary: true})
      break
    // operators with operands
    case 'Add':
    case 'Divide':
    case 'TruncatedDivide': // need test cases!
    case 'Log':
    case 'Modulo':
    case 'Multiply':
    case 'Power':
    case 'Subtract':
    case 'And':
    case 'Or':
    case 'Xor':
    case 'Equal':
    case 'NotEqual':
    case 'Matches':
    case 'Times':
    case 'Greater':
    case 'GreaterOrEqual':
    case 'Less':
    case 'LessOrEqual':
    case 'Union':
    case 'Intersect':
    case 'Except':
    case 'Indexer': // sends [list, index]
    case 'Coalesce':
    case 'Concatenate':
      result = compileNaryExpression(p)
      break
    case 'Not':
    case 'IsNull':
    case 'IsTrue':
    case 'IsFalse':
    // case 'Is': (has an strict & asTypeSpecifier)
    case 'Ceiling':
    case 'Floor':
    case 'Truncate':
    case 'Abs':
    case 'Ln':
    case 'Successor':
    case 'Predecessor':
    case 'Length':
    case 'Upper':
    case 'Lower':
    case 'CalculateAge':
    case 'DateFrom':
    case 'TimeFrom':
    case 'TimezoneFrom':
    case 'Collapse':
    case 'Current': // may need context??
    case 'Exists':
    case 'Negate':
    case 'Expand':
    case 'Distinct':
      result = compileUnaryExpression(p)
      break
    case 'AllTrue':
    case 'AnyTrue':
    case 'First':
    case 'Last':
    case 'Avg':
    case 'Count':
    case 'Max':
    case 'Min':
    case 'Median':
    case 'Mode':
    case 'Variance':
    case 'PopulationVariance':
    case 'PopulationStdDev':
    case 'StdDev':
    case 'Sum':
      result = compileUnaryExpression(p, 'source')
      break
    case 'IndexOf': // sends [list, item] (ie haystack, needle)
      result = compileIndexOfExpression(p)
      break
    case 'List':
      result = compileListExpression(p)
      break
    case 'Tuple':
      result = compileTupleExpression(p)
      break
    case 'Concept':
      result = compileConceptExpression(p)
      break
    case 'Code':
      result = compileCodeExpression(p)
      break
    case 'Start':
    case 'End':
      result = compileBoundExpression(p) // get the bounds of an interval
      break
    case 'Interval':
      result = compileIntervalExpression(p)
      break
    case 'Quantity':
      result = compileQuantityExpression(p)
      break
    case 'Instance':
      result = compileInstanceExpression(p)
      break
    default:
      result = {
        js: "'*****unknown $type*****" + p.expression.type + "'",
        $ctype: p.expression.type, // this will be wrong but meh
      }
      break
  }
  return result
}

// returns object {js, deps, $ctype}
function compileAs(p) {
//  {
//  "asType" : "{urn:hl7-org:elm-types:r1}Boolean",
//  "$type" : "As",
//  "operand" : { "$type" : "Null" },
//  }
  var raw_type = p.expression.asType
    || p.expression.asTypeSpecifier.name
    || p.expression.asTypeSpecifier.elementType.name
    || p.expression.asTypeSpecifier.elementType.elementType.name
  if (!raw_type) {
    debug('bad $type $type deep', p.expression.asTypeSpecifier.elementType.elementType.name)
  }
  var parsed_type = modelType(raw_type)
  expect(parsed_type.uri).to.equal('urn:hl7-org:elm-types:r1')
  var result = compileNextExpression(p.expression.operand, p)
//  var result = {
//    deps: null // fixme need to standardise
//  }
//  switch (p.expression.operand.type) {
//    case 'Null': //
//      result.js = 'null'
//      break;
//    case 'Literal': // fixme - need to enforce type...
//      result.js = toJs(p.expression.operand.value)
//      break;
//    default:
//      debug('p.expression.operand', p.expression.operand)
//      debug('p.expression.operand.type', p.expression.operand.type)
//      throw new Error('unknown data type')
//  }
  result.$ctype = parsed_type.$type
  return result
}

// returns object {js, deps, $ctype}
// (JS renders a literal into a pojo with {$type, value, simple})
function compileCast(p) {

// this is based around this example:
//  todo - why both "toType" & "toTypeSpecifier"
//  {
//  "type" : "Convert",
//  "operand" : {
//     "name" : "Y",
//     "type" : "ExpressionRef"
//  },
//  "toType" : "{urn:hl7-org:elm-types:r1}Decimal",
//  "toTypeSpecifier" : {
//     "name" : "{urn:hl7-org:elm-types:r1}Decimal",
//     "type" : "NamedTypeSpecifier"
//  }

  var type
  if (p.expression.type === 'Convert') {
    let parsed_type = modelType(p.expression.toType) // see note above - bit confused
    expect(parsed_type.uri).to.equal('urn:hl7-org:elm-types:r1')
    type = parsed_type.type
  } else {
    type = resultType(p.expression.type, e)
  }
  var result = {simple: true}
  expect(type).to.be.ok
  var e = compileNextExpression(p.expression.operand, p)
  return {
    js: 'r.Do(' + toJs(p.expression.type) + ', [' + [toJs(type), e.js].join(',') + '])',
    deps: e.deps,
    $ctype: type
  }
}

// returns object {js, deps, $ctype}
// (JS renders a literal into a pojo with {type, value, simple})
function compileLiteral(p) {
  var paramJs = {}
  var type = modelType(p.expression.valueType)
  var result = {simple: true}
  expect(type).to.be.ok
  if (type.uri === 'urn:hl7-org:elm-types:r1') {
    switch (type.type) {
      case 'Boolean': //false..true
        result.value = p.expression.value // ?? should confirm valid true/false string?
        break
      case 'Integer': //-2^31..2^31 – 1 1
      case 'Decimal': //-10^28 – 10^-8..10^28 – 10^-8 10^-8
        // could use _.isInteger _.isSafeInteger and _.isFinite
        result.value = _.toNumber(p.expression.value) // ?? should do checking ??
        break
//String All strings of length 2^31-1 or less
      case 'String':
        result.value = toJs(p.expression.value)
        break
    default:
      result.value = "'***** unhandled type *****'"
    }
    result.$type = toJs(type.type)
    return {
        js: renderPojo(result),
        $ctype: type.type,
      }
  }
  else {
    throw new Error('unknown data type')
  }
}

// table 4-G arithmatic operators:

//Add Performs numeric addition of its arguments
//Subtract Performs numeric subtraction of its arguments
//Multiply Performs numeric multiplication of its arguments
//Divide Performs numeric division of its arguments
//TruncatedDivide Performs integer division of its arguments
//Modulo Computes the remainder of the division of its arguments
//Log Computes the logarithm of its first argument, using the second argument as the base
//Power Raises the first argument to the power given by the second argument

//Round Returns the nearest numeric value to its argument, optionally specified to a number of decimal places for rounding

//Ceiling Returns the first integer greater than or equal to its argument
//Floor Returns the first integer less than or equal to its argument
//Truncate Returns the integer component of its argument
//Abs Returns the absolute value of its argument
//Negate Returns the negative value of its argument
//Ln Computes the natural logarithm of its argument
//Successor Returns the successor of its argument
//Predecessor Returns the predecessor of its argument
//MinValue Returns the minimum representable value for a type
//MaxValue Returns the maximum representable value for a type

// render function with 2 parameters AND a precision
// todo!!! (need example)
// @param p object
// -expression
//   - operand - array
//   - type - string
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compilePreciseExpression(p, params) {
  var e = params.unary
    ? [compileNextExpression(p.expression.operand, p)]
    : compileExpressionCollection(p.expression.operand, p)
  debug('e', e)
  // add precision if it exists
  // adding to ops (i may regret that)
  if (p.expression.precision) {
    e.push(compileNextExpression(p.expression.precision, p))
  }
  var deps = collapseDeps(_.map(e, 'deps'))
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      'r.Do(' + toJs(p.expression.type) +
      ', [' + _.map(e, 'js').join(',') + '])',
    deps: deps,
    $ctype: resultType(p.expression.type, e)
  }
}

// render function with n parameters
// @param p object
// -expression
//   - operand - array
//   - type - string
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileNaryExpression(p) {
  var e = compileExpressionCollection(p.expression.operand, p)
  var deps = collapseDeps(_.map(e, 'deps'))
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      'r.Do(' + toJs(p.expression.type) +
      ', [' + _.map(e, 'js').join(',') + '])',
    deps: deps,
    $ctype: resultType(p.expression.type, e)
  }
}

// render function with n parameters
// @param p object
// -expression
//   - operand - array
//   - type - string
// @param string value_key defaults to 'operand'
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileUnaryExpression(p, value_key) {
  var key = value_key || 'operand'
  var e = compileNextExpression(p.expression[key], p)
  var deps = e.deps
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      'r.Do(' + toJs(p.expression.type) + ', [' + e.js + '])',
    deps: deps,
    $ctype: resultType(p.expression.type, e)
  }
}

// IndexOf
// @param p object
// -expression
//   - type - "IndexOf"
//   - element - array
//   - source - array
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileIndexOfExpression(p) {
  var e = [ // probably a neater way to do this
    compileNextExpression(p.expression.source, p), // list/haystack
    compileNextExpression(p.expression.element, p) // item/needle
  ]
  var deps = collapseDeps(_.map(e, 'deps'))
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      'r.Do(' + toJs(p.expression.type) + ', [' + _.map(e, 'js').join(',') + '])',
    deps: deps,
    $ctype: "Integer"
  }
}

// List
// @param p object
// -expression
//   - element - array
//   - type - "List"
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileSourceExpression(p) {
  var e = compileExpressionCollection(p.expression.source, p)
  var deps = collapseDeps(_.map(e, 'deps'))
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      "{$type: 'List', value: [" + _.map(e, "js").join(",") + "]}",
    deps: deps,
    $ctype: "zzList." + (e.length ? e[0].$ctype : 'zz' + toJs(e))
  }
}

// Source (list) functions
// @param p object
// -expression
//   - element - array
//   - type - "List"
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileListExpression(p) {
  var e = compileExpressionCollection(p.expression.element, p)
  var deps = collapseDeps(_.map(e, 'deps'))
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      "{$type: 'List', value: [" + _.map(e, "js").join(",") + "]}",
    deps: deps,
    $ctype: "zzList." + (e.length ? e[0].$ctype : 'zz' + toJs(e))
  }
}

// Tuple
// @param p object
// -expression
//   - element - array of {name, value}
//   - type - "Tuple"
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileTupleExpression(p) {
  var js = {}, deps = []
  _.forEach(p.expression.element, function(expression) {
    var e = compileNextExpression(expression.value, p)
    js[expression.name] = e.js
    deps.push(e.deps)
  })
  return {
    js: '//deps: ' + toJs(deps) + '\n' +
      "{$type: 'Tuple', value: " + renderPojo(js) + "}",
    deps: collapseDeps(deps),
    $ctype: "Tuple"
  }
}

// Concept
// @param p object
// -expression
//   - display
//   - code
//   - type - "Concept"
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileConceptExpression(p) {
  var result = {}
  extractProperties(result, p, ['display', 'code'])
  return {
    js: renderPojo(result),
    $ctype: 'Concept'
  }
}

// Code
// @param p object
// -expression
//   - display
//   - code
//   - system
//   - type - "Code"
// @returns object {js, deps, $ctype}
// (js renders a literal into a pojo with {$type, value, simple})
function compileCodeExpression(p) {
  var result = {}
  extractProperties(result, p, ['display', 'code', 'system'])
  result.$type = 'Code'
  return {
    js: renderPojo(result),
    $ctype: 'Code'
  }
}



//Intervals have low and high points, either maybe inclusive or exclusive
//- inclusive (closed) square brackets,
//- exclusive (open) boundaries standard round parentheses.
function compileBoundExpression(p) {
  var bindings = {Start: 'low', End: 'high'}
  var property = bindings[p.expression.type]
  expect(property).to.be.ok
  let expression = compileNextExpression(p.expression.operand, p)
  return {
    js: 'r.ExtractProperty(' + expression.js + ',' + toJs(property) + ')',
    $ctype: expression.type + "***the thing this is range of***",
    deps: expression.deps
  }
}

/**
  * Extract properties from p.expression, encode and add to POJO
  * @param objext pojo object - mutated by method
  * @param object p
  * @param array properties
  * @retrun null
  */
function extractProperties(pojo, p, properties) {
  _.forEach(properties, function(k) {
      pojo[k] = toJs(p.expression[k])
  })
}


//Intervals have low and high points, either maybe inclusive or exclusive
// This function creates on e
//- inclusive (closed) square brackets,
//- exclusive (open) boundaries standard round parentheses.
function compileIntervalExpression(p) {
  var result = {}, e = []
  // take these 3 assuming they are JS constants
  extractProperties(result, p, ['lowClosed', 'highClosed'])
  _.forEach(['low', 'high'], function(k) {
    e[k] = compileNextExpression(p.expression[k], p)
    result[k] = e[k].js
  })
  result.$type = toJs(p.expression.type)
  return {
    js: renderPojo(result),
    deps: collapseDeps(_.map(e, 'deps')),
    $ctype: resultType(p.expression.type, e)
  }
}

function compileQuantityExpression(p) {
  var result = {}
  extractProperties(result, p, ['value', 'unit'])
  result.$type = toJs(p.expression.type)
  return {
    js: renderPojo(result),
    $ctype: 'Quantity'
  }
}

// instances
// array
function compileInstanceExpression(p) {
  var js = {}, deps = [];
  var type = modelType(p.expression.classType)
  _.forEach(p.expression.element, function(e) {
    var c = compileNextExpression(e.value, p)
    js[e.name] = c.js
    deps.push(c.deps)
  })
  js.$type = toJs(type.type)
  var result = {
    js: renderPojo(js),
    $ctype: type.type,
    deps: collapseDeps(deps),
  }
  return result
}


// ?? make date times into a moment object ??
// (all times in ISO-8601)
//Time @T00:00:00.0..@T23:59:59.999 1 millisecond
//DateTime @0001-01-01T00:00:00.0..@9999-12-31T23:59:59.999 1 millisecond
// returns object {js, deps, $ctype}
// (JS renders a literal into a pojo with {$type, value, simple})
function renderDateTime(expression) {
  var d = {}
  var offset = 'system.now.utcOffset()' // default offset as specified by time
  expect(expression.type).to.be.oneOf(['Time', 'DateTime'])
  _.forEach(expression, function(v, k) {
    if (k !== 'type') {
      switch (k) {
        case 'year':
        case 'month':
        case 'day':
        case 'hour':
        case 'minute':
        case 'second':
        case 'millisecond':
          // break if assumptions are violated
          expect(v.type).to.equal('Literal', 'invalid datetime literal')
          expect(v.valueType).to.equal('{urn:hl7-org:elm-types:r1}Integer',
            'invalid datetime literal')
          d[k] = _.toInteger(v.value)
          break
        case 'timezoneOffset':
          // this is a bit untidy - should probably use compileExpression
          var o
          if (v.type === 'Negate' ) {
            o = v.operand
            o.value = -o.value
          }
          else {
            o = v
          }
          expect(o.value).to.be.within(-15, 15)
          expect(o.valueType).to.equal('{urn:hl7-org:elm-types:r1}Decimal',
            'invalid datetime literal')
          offset = o.value
          break
        default:
          throw new Error('unknown date property:' + k)
      }
    }
  })
  d.timezoneOffset = offset
  var result = {}
  result.$type = toJs(expression.type)
  result.value = renderPojo(d)
  return {
    js: renderPojo(result),
    $ctype: expression.type
  }
}

// deal with an expression that looks like:
//"expression" : {
//   "dataType" : "{http://hl7.org/fhir}Encounter",
//   "type" : "Retrieve",
//   "codeProperty" : "type",
//   "codes" : {
//      "name" : "Encounter Inpatient",
//      "type" : "ValueSetRef"
//   },
//   "dateProperty" : "period.end.value",
//   "dateRange" : {
//      "name" : "Measurement Period",
//      "type" : "ParameterRef"
//   }
// returns object {js, deps, ctype}
f.compileRetrieve = function(p) {
  var paramJs = {}
  // set up model and data type
  var type = modelType(p.expression.dataType)
  expect(type).to.be.ok
  paramJs.$type = toJs(type.type)
  paramJs.typeUri = toJs(type.uri)
  // accumulator for dependancies
  var deps = [{system: p.expression.dataType}]
  // set up codes
  if (p.expression.codes) {
    paramJs.codeProperty = idToJs(p.expression.codeProperty)
    let codes = compileNextExpression(p.expression.codes, p)
    paramJs.codes = codes.js
    deps.push(codes.deps)
  }
  // set up data range
  if (p.expression.dateRange) {
    paramJs.dateProperty = idToJs(p.expression.dateProperty)
    let dateRange = compileNextExpression(p.expression.dateRange, p)
    paramJs.dateRange = dateRange.js
    deps.push(dateRange.deps)
  }
  if (p.statement.context == 'Patient') {
    paramJs.patient = 'context.patient.id'
    deps.push({context: {patient: 'id'}})
  }
  return {
    js: 'system.interface.Retrieve(' + renderPojo(paramJs) + ')',
    $ctype: type,
    deps: collapseDeps(deps)
  }
}

// deal with an expression that looks like:
//  source define? queryInclusion* where? return? sort?
// returns object {js, deps, $ctype}
f.compileQuery = function(p) {
  been_here++
  if (been_here > 8) {
    debug('p.expression', p.expression)
    throw new Error('blah')
  }
  var sources = {}, deps = [], type
  expect(p.expression.source.length).to.be.ok
  // todo
  // relate
  // return
  // sort
  // where
  var pre = [], post = [], mid = []
  // make an IIFE
  pre.push('(function (sources) {')
  pre.push('var result = []')
  // holder
  pre.push('var cursor = {}')
  // code to join  all sources as n dimensional matix
  var depth = 1 // simple counter to support variable naming (?unneeeded)
  // loop over all sources
  _.forEach(p.expression.source, function(source) {
    var expression = compileNextExpression(source.expression, p)
    deps.push(expression.deps)
    var alias = source.alias
    , alias_id = idToJs(alias) // js escaped alias
    if (!type) type = expression.type // take the first source type (fixme)
    sources[alias] = expression.js
    // open loop
    pre.push('_.forEach(sources[' + alias_id + '].value, ' +
        'function (rec' + depth + ', index' + depth + ') {')
    pre.push('cursor[' + alias_id + '] = rec' + depth)
    // close loop
    post.push('})')
    depth++
  })
  //  mid.push("console.log('result', result)")
  post.push('return result')
  // deal with where clause
  // todo deal with deps
  // we could inject in the query context here to enable validataion
  debug('p.expression.where', p.expression.where)
  var where = p.expression.where
    ? compileNextExpression(p.expression.where, p)
    : {js: "true"} // fixme
  // create core of loop
  mid.push('var include = ' + where.js)
  mid.push('if (include) {')
  mid.push("console.log('Included', cursor)")
  mid.push('result.push(_.clone(cursor))')
  mid.push('} else {')
  mid.push("console.log('excluded')")
  mid.push('}')

  // code to execute the IIFE
  post.push('}(' + renderPojo(sources) + '))')


  var js = _.concat(pre, mid, post).join('\n')
  var result


//    "return" : {
//      "expression" : {
//          "$type" : "Convert",
//          "operand" : { "name" : "X", "$type" : "AliasRef" },
//          "toType" : "{urn:hl7-org:elm-types:r1}Decimal",
//          "toTypeSpecifier" : {
//             "name" : "{urn:hl7-org:elm-types:r1}Decimal",
//             "$type" : "NamedTypeSpecifier"
//          }
//       }
//      "distinct" : false,

  if (_.get(p.expression, 'return.expression.operand.type') === 'AliasRef') {
    let name = p.expression.return.expression.operand.name
    let parsed_type = modelType(p.expression.return.expression.toType)
    result = {
      value: "_.map(" + js + ", '" + name + "')",
      $type: toJs(parsed_type.type)
    }
  } else {
    result = {
      value: js,
      $type: "'I do not know'"
    }
  }
  return {
    // combine outputs to single string
    js: renderPojo(result),
    $ctype: "List.ofsomthing**",
    deps: collapseDeps(deps)
  }
}
// { "path" : "id", "scope" : "C", "type" : "Property" }
// returns object {js, deps, $ctype}
function compileProperty(p) {
  var result
  var source = p.expression.source || p.expression
  // todo - explain why this is necessary @bryn
  if (source.scope) {
    result = {
      js: 'cursor[' + toJs(source.scope) + '][' +
         toJs(source.path) + ']',
      $ctype: '*****Propytp tbd****',
      deps: {params: p.expression.name}
    }
  } else { //sometimes the ELM nests a bit funky
    result = compileNextExpression(source, p)
  }
  return result
}

// confirm this code understands what to do with this object
function isKnownSchema(library) {
  return library.schemaIdentifier.id === 'urn:hl7-org:elm'
    && library.schemaIdentifier.version === 'r1'
}

// translate an ELM `identifier` into an js safe string
function idToJs(name) {
  // if starts with single or double quote then escape
  return (name[0] == "'" || name[0] == '"') ? name : "'" + name + "'"
}

// return string javascript
function renderPojo(jsobject) {
  var y = function(res, v, k) {
    res.push(k + ':' + v)
    return res
  }
  return '{' + _.reduce(jsobject, y, []).join(',') + '}'
}

// deeply collpase an array of dependancies from multiple sources
function collapseDeps(deps) {
  // not sure if this is what i need (needs testing - todo)
  return _.merge({}, deps)
}

// see if type is in {using_uri}name pattern
// @params string
// @return false if not, otherwise
// - uri
// - type
function modelType(dataType) {
  var matches = dataType.match(/{(.*)}(.*)/)
  return matches.length == 3 ? {uri: matches[1], type: matches[2] } : false
}

module.exports = f

// This function aims to handle the common case type transformations and apply
// basic heuristics. Complex overloaded functions must access dedicated logic
// as needed.
// @param operation string
// @param array of {$type, js, deps}
// @return string type
function resultType(operation, e) {
var operationTypes = {
As: '??',
Convert: '??',
Maximum: '??',
Minimum: '??',
Predecessor: '??',
Successor: '??',
Coalesce: '??',
//The Coalesce operator returns the first non-null result in a list of arguments.
//If all arguments evaluate to null, the result is null.  The static type of the
//first argument determines the type of the result, and all subsequent arguments
//must be of that same type
//9.6.4 Divide  168
///(left Decimal, right Decimal) Decimal
///(left Quantity, right Decimal) Quantity
///(left Quantity, right Quantity) Quantity
//9.8.5 Date/Time Component From  177
//The component-from operator returns the specified component of the
//argument.  For DateTime values, precision must be one of: year,
//  month, day, hour, minute, second, or millisecond.  For Time
//  values, precision must be one of: hour, minute, second, or
//  millisecond.
And: 'Boolean',
Not: 'Boolean',
Or: 'Boolean',
Xor: 'Boolean',
Is: 'Boolean',
ToBoolean: 'Boolean',
ToConcept: 'Concept',
ToDateTime: 'DateTime',
ToDecimal: 'Decimal',
ToInteger: 'Integer',
ToQuantity: 'Quantity',
ToString: 'String',
ToTime: 'Time',
IsNull: 'Boolean',
IsFalse: 'Boolean',
IsTrue: 'Boolean',
Between: 'Boolean',
Equal: 'Boolean',
Greater: 'Boolean',
GreaterOrEqual: 'Boolean',
Less: 'Boolean',
LessOrEqual: 'Boolean',
Matches: 'Boolean',
Not: 'Boolean',
Abs: '_IDQ',
Add: '_IDQDT',
Subtract: '_IDQDT',
Ceiling: 'Integer',
Floor: 'Integer',
Log: 'Decimal',
Ln: 'Decimal',
Modulo: '_ID',
Multiply: '_IDQ',
Divide: '_DQ',
Negate: '_IDQ',
Power: '_ID',
Round: 'Decimal',
Truncate: 'Integer',
TruncatedDivide: '_IDQ', // check !!
Combine: 'String',
Concatenate: 'String',
Indexer: 'String',
Length: 'Integer',
Lower: 'String',
PositionOf: 'Integer',
Split: 'List.String',
Substring: 'String',
Upper: 'String',
// Add  175 -- see above
Difference: 'Integer',
Duration: 'Integer',
SameAs: 'Boolean',
SameOrBefore: 'Boolean', // check!!! (i made this up)
SameOrAfter: 'Boolean', // check!!
// 9.8.12 Subtract  180 -- see above
Time: 'Time',
After: 'Boolean',
Before: 'Boolean',
//9.9.3 Collapse  182
Contains: 'Boolean',
End: '_subinterval',
Ends: 'Boolean',
Equal: 'Boolean',
Except: 'Interval',
In: 'Boolean', // multiple types (code and value set see below)
Includes: 'Boolean',
IncludedIn: 'Boolean',
Intersect: 'Boolean',
Matches: 'Boolean',
Meets: 'Boolean',
MeetsBefore: 'Boolean',
MeetsAfter: 'Boolean',
NotEqual: 'Boolean',
Overlaps: 'Boolean',
OverlapsBefore: 'Boolean',
OverlapsAfter: 'Boolean',
ProperIn: 'Boolean',
ProperContains: 'Boolean',
ProperIncludes: 'Boolean',
ProperIncludedIn: 'Boolean',
Start: '_subinterval',
Starts: 'Boolean',
Union: 'Interval',
Width: '???what is width??',
Distinct: 'List',
Equal: 'Boolean',
Except: 'List',
Exists: 'Boolean',
Expand: 'List', // 189 flattens a list of lists into a single list.
First: '****Item****',
Indexer: '****Item****',
IndexOf: 'Integer',
Intersect: 'List',
Last: '****Item****',
Length: 'Integer',
Matches: 'Boolean',
Singleton: '****Item****',
Union: 'List',
AllTrue: 'Boolean',
AnyTrue: 'Boolean',
Avg: '_DQ',
Count: 'Integer',
Max: '****Item****',
Min: '****Item****',
Median: '_DQ',
Mode: '****Item****',
PopulationStdDev: '_DQ',
PopulationVariance: '_DQ',
StdDev: '_DQ',
Sum: '_IDQ',
Variance: '_DQ',
Age: 'Integer',
AgeAt: 'Integer',
CalculateAge: 'Integer',
CalculateAgeAt: 'Integer',
Equal: 'Boolean',
Matches: 'Boolean',
//9.12.7 In (Codesystem)  199
//9.12.8 In (Valueset)  199

Interval: '_Interval',
// DateTimeComponentFrom Returns a specified component of its argument
DateTimeComponentFrom: 'Integer', // todo - verify this
// Today Returns the date (with no time components specified) of the start timestamp associated with the evaluation request
Today: 'DateTime',
// Now Returns the date and time of the start timestamp associated with the evaluation request
Now: 'DateTime',
// TimeOfDay Returns the time-of-day of the start timestamp associated with the evaluation request
TimeOfDay: 'Time',
// DateTime Constructs a date/time value from its arguments
DateTime: 'DateTime',
// DateFrom Returns the date (with no time component) of the argument
DateFrom: 'DateTime',
// TimeFrom Returns the time of the argument
TimeFrom: 'Time',
// TimezoneFrom Returns the timezone offset (in hours) of the argument
TimezoneFrom: 'Decimal',
// DurationBetween Computes the number of whole periods between two dates
DurationBetween: 'Integer',

}

// we use operationTypesmaps to quickly deal with with fixed type outcomes
debug('operation', operation)
var type = operationTypes[operation]
// if type is not in the above list we should not be here!
expect(type).to.be.ok
// the answer is simple if we only have one type:
if (type[0] !== '_') return type

// having dealth with the simple cases we need to deal with more complex
// scenarios

// make a list of unique types in play
var unique_types = _(e).map('type').uniq().sort().value()
// if we have only one type in our operands then generally the result is same
// type - fixme - when is this not the case? need to code exceptions
if (unique_types.length === 1) {
  switch (operation) {
    case '_Interval':
      return 'Interval.' + unique_types[0]
    default:
      return unique_types[0]
  }
}
// mixed integer and decimal types will end up being decimal
// are there exceptions? they will need encoding
if (_.isEqual(unique_types, ['Decimal', 'Integer'])) {
  return 'Decimal'
}

// if we have Quantity plus either Decimal or Integer - assume Quantity
if (!_.difference(unique_types, ['Decimal', 'Integer', 'Quantity']).length) {
  return 'Quantity'
}

  return 'i give up'
}

