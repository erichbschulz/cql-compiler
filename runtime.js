'use strict'

var Promise = require('bluebird')
, _ = require('lodash')
, moment = require('moment')
, request = require('request-promise') // bluebird wrapper around request
, debug = console.log
, time_units = ['year', 'month', 'day', 'hour', 'minute', 'second', 'millisecond']
, time_units_r = ['year', 'month', 'date', 'hour', 'minute', 'second', 'millisecond']
, time_units_count = time_units.length
// because... Math.floor(4.3) = 4, but Math.floor(-4.3) = -5
// thanks to http://stackoverflow.com/questions/4912788/truncate-not-round-off-decimal-numbers-in-javascript
function Truncate(number) {
  return Math[number < 0 ? 'ceil' : 'floor'](number)
}

// take and array of simple elements and add units and type etc
// mutates array
function formalise(array, defaults) {
  _.forEach(array, function (value, i) {
    array[i] = _.defaults({value: value}, defaults)
  })
}

// get a zero based index (NB CQL uses 1 based)
function IndexOf(needle, haystack) {
  return _.findIndex(haystack, function (o) {
    return _.isEqual(o, needle)
  })
}

// used for Predecessor and Successor
function step(type) {
  return type === 'Integer' ? 1 : 0.00000001
}

// used for Predecessor and Successor
// date and time values:
// in the range @0001-01-01T00:00:00.0 to @9999-12-31T23:59:59.999
// with a 1 millisecond step size.
function add(ob, sign, unit) {
  if (isDate(ob)) {
    let precision = get_precision(ob.value)
    let m = date_to_moment(ob.value)
    m.add(sign, unit || precision)
    let year = m.get('year')
    if (year > 9999 || year < 1) throw new Error('date time out of range')
    return get_to_precision(m, precision)
  } else {
    return ob.value + sign * (unit || step(ob.$type))
  }
}

function isDate(value) {
  return _.has(value, 'value.year')
}

function date_to_moment(date) {
  let value = _.clone(date)
  if (value.month) value.month--
  return moment(value)
    .utcOffset(date.timezoneOffset)
    .subtract(date.timezoneOffset, 'hours')
}

// used for Predecessor and Successor
// finds the first missing date time value
function get_precision(value) {
  var prev
  _.forEach(time_units, function(unit) {
    if (_.isNil(value[unit])) return prev
    prev = unit
  })
  return prev
}

// extract a DateTime from at a specified level of precsions
// finds the first missing date time value
function get_to_precision(m, precision) {
  var result = {timezoneOffset: -m.utcOffset()}
    , u
  for (var i = 0; i < time_units_count; i++) {
    u = time_units[i]
    // get value coping with momentjs's weird 0-based monts
    // cope with moment using 'day' for "day of the week"
    result[u] = m.get(time_units_r[i]) + (u === 'month' ? 1 : 0)
    if (u === precision) return result
  }
  throw new Error('bad precision')
}

function intersection(A, B) {
  return _.intersectionWith(A, B, _.isEqual)
}

function median(values) {
  if (!values.length) return null
  var sorted = _.sortBy(values)
  var half = Math.floor(values.length/2)
  return (values.length % 2)
    ? sorted[half]
    : (sorted[half-1] + sorted[half]) / 2
}

function avg(values) {
  if (!values.length) return null
	return _.sum(values) / values.length
}

// in population mode the divisor is n rather than n-1
function variance(values, population_mode) {
  if (!values.length) return null
	var a = avg(values), i = values.length, v = 0
	while (i--) {
		v += Math.pow(values[i] - a, 2)
	}
	v /= values.length - (population_mode ? 0 : 1)
	return v
}

function stddev(values, population_mode) {
	var stdDev = Math.sqrt(variance(values, population_mode))
	return stdDev
}

function mode(values, defaults) {
  if (!values.length) return null
  var clusterFunction = function(a) { return a.toString() }
  var max = 0, mode = []
  var counted = _.countBy(values, clusterFunction)
  max = _.max(_.values(counted))
  _.forIn(counted, function(v,k) {
    if (v === max) mode.push(parseFloat(k))
  })
  formalise(mode, defaults) // add in $type etc
  var res = mode.length > 1
    ? {value: mode, $type: 'List'}
    : mode[0]
  return res
}

//
function alltrue(values) {
	var i = values.length
	while (i--) { if (!values[i]) return false }
	return true
}

function anytrue(values) {
	var i = values.length
	while (i--) { if (values[i]) return true }
	return false
}


// process an array of values
// returns object
// - nullCount
// - array mixed
// - unit
// - $type - $type hint
// throws error on mixed types
function getValues(values, operator) {
  var nullCount = 0, units = {}, value = [], unit, type
  _.forEach(values, function(e, k) {
    if (_.isNil(e) || _.isUndefined(e.value)) {
      nullCount++
      value.push(null)
    } else {
      value.push(resolve(e))
      // if mixed $type take Decimal in prefernce to Integer
      if (!type || (type === 'Integer' && e.$type === 'Decimal')) {
        type = e.$type
      }
      if (type === 'Quantity') {
        // classify and count the units
        unit = resolveString(e.unit) || '_no_units_'
        // increment counter (pro
        units[unit] = (units[unit] || 0) + 1
      }
    }
  })
  // test for mixed units - this behavour seems safest for now
  if (_.size(units) > 1 && !allowMixed(operator)) {
    debug('mixed units', units, 'from', values)
    throw new Error('mixed units')
  }
  return {
    nullCount: nullCount,
    value: value,
    $type: type,
    unit: unit,
  }
}

// get string value
function resolveString(x) {
  if (_.isObject(x)) {
    if (x.$type === 'String') return x.value
    throw new Error('bad string')
  }
  return x
}

// get any value
function resolve(x) {
  if (_.has(x, '$type')) {
    if (isDate(x)) {
      return {
        $moment: date_to_moment(x.value),
        $precision: get_precision(x.value)
      }
    } else {
      return resolve(x.value)
    }
  }
  return x
}

// compares value
function compare(v, operator) {
  if (v[0].$moment) {
    //      debug('is a date!')
    //      debug('v[0].$moment.utc()', v[0].$moment.utc())
    //      debug('v[1].$moment.utc()', v[1].$moment.utc())
    switch (operator) {
      case 'Equal': return v[0].$moment.utc().isSame(v[1].$moment.utc())
      case 'Greater': return v[0] > v[1]
      case 'GreaterOrEqual': return v[0] >= v[1]
      case 'Less': return v[0] > v[1]
      case 'LessOrEqual': return v[0] >= v[1]
    }
  } else {
    switch (operator) {
      case 'Equal': return _.isEqual(v[0], v[1])
      case 'Greater': return v[0] > v[1]
      case 'GreaterOrEqual': return v[0] >= v[1]
      case 'Less': return v[0] < v[1]
      case 'LessOrEqual': return v[0] <= v[1]
    }
  }
}

// same as getValues but includes a filtered set of values
function getRealValues(x) {
  var v = getValues(x)
  v.real = _.reject(v.value, _.isNull)
  return v
}

//
function allowMixed(operator) {
  switch (operator) {
  case 'Multiply':
  case 'Divide':
    return true
  default:
    return false
  }
}


// definition of the run-time
var r = {
  SingletonFrom: function(list) {
    var result
    if (list.value.length > 1) {
      throw new Error('list is longer than 1')
    }
    result = list.value[0]
    return {value: result, $type: "one of list.$type", deps: "do i know?"}
  },
  Do: function(operator, ops) {
    var extracted = getValues(ops, operator)
    var nullCount = extracted.nullCount // integer
    var unit = extracted.unit // integer
    var value = extracted.value // array of values
    var result
    // todo check null
    //    console.log('runtime operator', operator)
    //    console.log('runtime ops', ops)
    switch (operator) {
    case 'Add':
    case 'Subtract':
      var sign = operator === 'Add' ? 1 : -1
      result = {
        $type: extracted.$type,
        value: add(ops[0], value[1] * sign, (extracted.$type === 'DateTime' ? ops[1].unit : 1))
      }
      if (unit !== '_no_units_') result.unit = unit
      break
    case 'Round':
      result = _.defaults({
        value: _.round(value[0], (value[1] || 0))
      }, _.pick(ops[0], ['unit', '$type']))
      break
    case 'Convert':
      result = {$type: ops[0]}
      // handle null values
      if (nullCount > 1) return null
      // first value is a string
      switch (ops[0]) {
      case 'Decimal':
        result.value = _.toNumber(value[1])
        break
      default:
        throw new Error('not yet')
      }
      break
    case 'Not':
      result = {
        $type: 'Boolean',
        value: _.isNull(value[0]) ? null : !value[0]
      }
      break
    case 'First':
      result = _.isArray(value[0]) && value[0].length ? value[0][0] : null
      break
    case 'Length':
      if (nullCount) return null
      result = {
        $type: 'Integer',
        value: value[0].length
      }
      break
    case 'Count':
      if (nullCount) return null
      result = {
        $type: 'Integer',
        value: getRealValues(value[0]).real.length
      }
      break
    case 'Sum':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: v.real.length === 0 ? null : _.sum(v.real)
      }, _.pick(ops[0].value[0], ['unit', '$type']))
      break
    case 'Avg':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: avg(v.real)
      }, _.pick(v, ['unit', '$type']))
      break
    case 'Median':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: median(v.real)
      }, _.pick(v, ['unit', '$type']))
      break
    case 'Mode': // tricky in can give an array of results
      if (nullCount) return null
      var v = getRealValues(value[0])
      var defaults = _.pick(v, ['unit', '$type'])
      result = mode(v.real, defaults)
      break
    case 'Variance':
    case 'PopulationVariance':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: variance(v.real, operator === 'PopulationVariance')
      }, _.pick(v, ['unit', '$type']))
      break
    case 'StdDev':
    case 'PopulationStdDev':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: stddev(v.real, operator === 'PopulationStdDev')
      }, _.pick(v, ['unit', '$type']))
      break
    case 'AnyTrue':
      var v = getRealValues(value[0])
      result = {
        $type: 'Boolean',
        value: anytrue(v.real)
      }
      break
    case 'AllTrue':
      var v = getValues(value[0])
      result = {
        $type: 'Boolean',
        value: alltrue(v.value)
      }
      break
    case 'Min':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: v.real.length === 0 ? null : _.min(v.real)
      }, _.pick(v, ['unit', '$type']))
      break
    case 'Max':
      if (nullCount) return null
      var v = getRealValues(value[0])
      result = _.defaults({
        value: v.real.length === 0 ? null : _.max(v.real)
      }, _.pick(v, ['unit', '$type']))
      break
    case 'Last':
      result = _.isArray(value[0]) && value[0].length ? _.last(value[0]) : null
      break
    case 'Exists':
      result = {
        $type: 'Boolean',
        value: _.isArray(value[0]) && (value[0].length > 0)
      }
      break
    case 'Union':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: _.uniq(_.flatten(value), _.isEqual)
      }
      break
    case 'Expand':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: _.flatten(_.map(value[0], 'value'))
      }
      break
    case 'Except':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: _.differenceWith(value[0], value[1], _.isEqual)
      }
      break
    case 'Expand':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: _.flatten(getValues(value[0]).value)
      }
      break
    case 'Distinct':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: _.uniqWith(value[0], _.isEqual)
      }
      break
    case 'Intersect':
      if (nullCount) return null
      result = {
        $type: 'List',
        value: intersection(value[0], value[1])
      }
      break
    case 'Includes':
    case 'IncludedIn':
    case 'ProperIncludes':
    case 'ProperIncludedIn':
      if (nullCount) return null
      var reverse = (operator === 'Includes' || operator === 'ProperIncludes')
      var proper = (operator === 'ProperIncludes' || operator === 'ProperIncludedIn')
      var A = value[reverse ? 0 : 1]
      var B = value[reverse ? 1 : 0]
      var overlap = intersection(A, B).length
      var x = (overlap === B.length) && !(proper && B.length >= A.length)
      result = {
        $type: 'List',
        value: x
      }
      break
    case 'IndexOf':
      // this is how the spec reads:
      //    if (_.isNull(value[0])) return null
      // see https://jira.oncprojectracking.org/browse/CQLIT-15
      if (nullCount) return null
      var list = getValues(value[0]).value
      result = {
        $type: 'Integer',
        value: IndexOf(value[1], list) + 1
      }
      break
    case 'In':
    case 'ProperInIn':
    case 'Contains': // todo find out what
    case 'ProperContains':
      if (nullCount) return null
      // reverse operand order In/Contains
      var list = getValues(value[operator === 'In' || operator === 'ProperIn' ? 1 : 0]).value
      var needle = value[operator === 'In' ? 0 : 1]
      result = {
        $type: 'Boolean',
        value: (IndexOf(needle, list) >= 0)
      }
      break
    case 'Successor':
    case 'Predecessor':
      var sign = operator === 'Successor' ? 1 : -1
      result = _.defaults({
        value: add(ops[0], sign)
      }, _.pick(ops[0], ['unit', '$type', 'simple']))
      break
    case 'Indexer':
      if (nullCount) return null
      if (value[1] < 1) throw new Error('index below 1')
      var list = getValues(value[0]).value
      result = {
        $type: 'Integer',
        value: list.length >= value[1] ? list[value[1]-1] : null
      }
      break
    case 'And':
      result = {
        $type: 'Boolean',
        value: nullCount
         ? ((value[0] === false || value[1] === false) ? false : null )
         : value[0] && value[1]
      }
      break
    case 'Or':
      result = {
        $type: 'Boolean',
        value: nullCount
         ? ((value[0] === true || value[1] === true) ? true : null )
         : value[0] || value[1]
      }
      break
    case 'Xor':
      result = {
        $type: 'Boolean',
        value: nullCount ? null : value[0] !== value[1]
      }
      break
    case 'Equal':
    case 'Greater':
    case 'GreaterOrEqual':
    case 'Less':
    case 'LessOrEqual':
      result = {
        $type: 'Boolean',
        value: compare(value, operator)
      }
      break
    default:
        switch ((ops[0] || {}).$type) {
        case 'DateTime':
          throw new Error('not yet')
        case 'Integer':
        case 'Decimal':
        case 'Quantity':
          switch (ops.length) {
            case 1:
              switch (operator) {
              case 'Negate':
                result = _.defaults({
                  value: -value[0]
                }, _.pick(ops[0], ['unit', '$type', 'simple']))
                break
              case 'Floor':
                result = _.defaults({
                  value: Math.floor(value[0])
                }, _.pick(ops[0], ['unit', '$type', 'simple']))
                break
              case 'Ceiling':
                result = _.defaults({
                  value: Math.ceil(value[0])
                }, _.pick(ops[0], ['unit', '$type', 'simple']))
                break
              case 'Truncate':
                result = _.defaults({
                  value: Truncate(value[0])
                }, _.pick(ops[0], ['unit', '$type', 'simple']))
                break
              case 'Abs':
                result = _.defaults({
                  value: Math.abs(value[0])
                }, _.pick(ops[0], ['unit', '$type', 'simple']))
                break
              case 'Ln':
                result = {
                  $type: 'Decimal',
                  value: Math.log(value[0])
                }
                break
                  default:
                    throw new Error('unhandled0 ' + operator)
              }
              break
            case 2:
              let fred = resolveString(ops[1].$type)
              switch (fred) {
                case 'Integer':
                case 'Decimal':
                case 'Quantity':
                  switch (operator) {
                  case 'Divide': // fixme oversipimplification of units...
                    result = _.defaults({
                      value: value[0] / value[1]
                    }, _.pick(ops[0], ['unit', '$type', 'simple']))
                    break
                  case 'Log':
                    result = {
                      $type: 'Decimal',
                      value: Math.log(value[0]) / Math.log(value[1])
                    }
                    break
                  case 'Modulo':
                    result = {
                      $type: extracted.$type,
                      value: value[0] % value[1]
                    }
                    break
                  case 'Multiply': // fixme oversipimplification of units...
                    result = _.defaults({
                      value: value[0] * value[1]
                    }, _.pick(ops[0], ['$type', 'simple']))
                    result.unit = resolve(ops[0].unit || ops[1].unit) // fixme - oversimplification!
                    break
                  case 'Power':
                    result = {
                      $type: extracted.$type,
                      value: Math.pow(value[0], value[1])
                    }
                    break
                  case 'TruncatedDivide':
                    result = {
                      $type: 'Decimal',
                      value: Truncate(value[0] / value[1])
                    }
                    break
                  default:
                    throw new Error('unhandled1' + operator)
              }
              break
            default:
            throw new Error('unhandled2 operation: ' + operator + ', $type: ' + value[1].$type)
          }
          break
        default:
          throw new Error('unhandled3 operation: ' + operator + ', length: ' + ops.length)
        }
      }
    }
    if (result && result.$type === 'Integer'
      && (result.value >= 2147483648 || result.value < -2147483648)) {
      throw new Error('out of range integer')
    }
    return result
  },
  ExtractProperty: function(o, k) {
    return {
      $type: o.$type, // this is broken, but it is a start
      value: o.value.k
    }
  }
}

module.exports = r
