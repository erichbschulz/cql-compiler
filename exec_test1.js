#!/usr/bin/env node
"use strict"

// this file is a test harness for test1.js file

var Promise = require('bluebird')
var _ = require('lodash')
var moment = require('moment')
var runtime = require('./runtime')

var system = {
  Promise: Promise,
  _: _,
  moment: moment,
  runtime: runtime,
  now: moment()
}

var library_name = './test_cds/test1'
// get the module instance
var module = require(library_name).module
var test1 = module.constructor(system)
console.log('test1', test1.library)
console.log('X', test1.X())
console.log('XPlusY', test1.XPlusY())

var X = 4;
var Y = 5;

var tests = {
  XPlusY: X + Y,
  XMinusY: X - Y,
  XTimesY: X * Y,
  XDivideY: X / Y,
  XLogY: 3,
  XModuloY: X % Y,
  XPowerY: X ^ Y
}

_.forEach(tests, function(expected, method) {
  var result = test1[method]().value;
  console.log(method, (result==expected ? 'pass' : 'fail'), result);
})
