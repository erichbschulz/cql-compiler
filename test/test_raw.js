
/*
// wviwyvip;s/^\(\s*�kb\s\)*�kb�kb�kb*)�kb\)\(\w\)/pjjua\1pa_\1�kb2//\/
////// wviwyvip;pa_\2/0f>as/^\(\s\s*\)\(\w\)/\1/\/
*/

module.exports = {
path: '../test_cds/raw1',
equals: {


// Count
//  should be able to count lists without nulls
    Count_not_null: {value: 5},
//  should be able to count lists with nulls
    Count_has_null: {value: 2},
//  should be able to count empty list
    Count_empty: {value: 0},

// Sum
//  should be able to sum lists without nulls
    Sum_not_null: {value: 15},
//  should be able to sum lists with nulls
    Sum_has_null: {value: 3},
//  should be able to sum empty list
    Sum_empty: {value: null},
//  should be able to sum quantity lists without nulls
    Sum_not_null_q: {value: 15, unit: 'ml'},

//  should be able to sum  quantity lists with nulls
    Sum_has_null_q: {value: 3, unit: 'ml'},


//  should return null for unmatched units in a list of quantiies
    Sum_unmatched_units_q: {throws: true},

// Min
//  should be able to find min in lists without nulls
    Min_not_null: {value: 0},
//  should be able to find min in lists with nulls
    Min_has_null: {value: -1},
//  should be return null for empty list
    Min_empty: {value: null},
//  should be able to find min in lists of quantiies without nulls
    Min_not_null_q: {value: 0, unit: 'ml'},
//  should be able to find min in lists of quantiies with nulls
    Min_has_null_q: {value: -1 , unit: 'ml'},

// Max
//  should be able to find max in lists without nulls
    Max_not_null: {value: 10},
//  should be able to find max in lists with nulls
    Max_has_null: {value: 2},
//  should be return null for empty list
    Max_empty: {value: null},
//  should be able to find max in lists of quantiies without nulls
    Max_not_null_q: {value:  10, unit: 'ml'},
//  should be able to find max in lists of quantiies with nulls
    Max_has_null_q: {value: 2, unit: 'ml'},

// Avg
//  should be able to find average for lists without nulls
    Avg_not_null: {value: 3},
//  should be able to find average for lists with nulls
    Avg_has_null: {value: 1.5},
//  should be return null for empty list
    Avg_empty: {value: null},
//  should be able to find average for lists of quantiies without nulls
    Avg_not_null_q: {value: 3, unit: 'ml'},
//  should be able to find average for lists of quantiies with nulls
    Avg_has_null_q: {value: 1.5 , unit: 'ml'},

// Median
//  should be able to find median of odd numbered list
    Median_odd: {value: 3},
//  should be able to find median of even numbered list
    Median_even: {value: 3.5},
//  should be able to find median of odd numbered list that contains duplicates
    Median_dup_vals_odd: {value: 3},
//  should be able to find median of even numbered list that contians duplicates
    Median_dup_vals_even: {value: 2.5},
//  should be return null for empty list
    Median_empty: {value: null},
//  should be able to find median of odd numbered list
    Median_odd_q: {value:  3 , unit: 'ml'},
//  should be able to find median of even numbered list
    Median_even_q: {value: 3.5, unit: 'ml'},
//  should be able to find median of odd numbered list that contains duplicates
    Median_dup_vals_odd_q: {value: 3, unit: 'ml'},
//  should be able to find median of even numbered list that contians duplicates
    Median_dup_vals_even_q: {value: 2.5, unit: 'ml'},

// Mode
//  should be able to find mode of lists without nulls
    Mode_not_null: {value: 2},
//  should be able to find Mode lists with nulls
    Mode_has_null: {value: 2},
//  should be return null for empty list
    Mode_empty: {value: null},
//  should be able to find bimodal
    Mode_bi_modal: {value: [2,3]}, // return list


/////////// !!!!!!!!! see https://chat.fhir.org/#narrow/stream/CQL/topic/variance.20and.20pop.20variance // fixme

// PopulationVariance
//  should be able to find PopulationVariance of a list
    PopulationVariance_v: {value: 2},//was 2.5 nb this is different to origingal ?? bug ??
//  should be able to find PopulationVariance of a list
    PopulationVariance_v_q: {value: 2, unit: 'ml'}, // fixme!!

// Variance
//  should be able to find Variance of a list
    Variance_v: {value: 2.5},// was 2
//  should be able to find Variance of a list
    Variance_v_q: {value: 2.5, unit: 'ml'},

// StdDev
//  should be able to find Standard Dev of a list
    StdDev_std: {value: 1.5811388300841898}, // was 1.4142135623730951
//  should be able to find Standard Dev of a list
    StdDev_std_q: {value: 1.5811388300841898, unit: 'ml'},

// PopulationStdDev
//  should be able to find Population Standard Dev of a list
    PopulationStdDev_dev: {value: 1.4142135623730951}, // 1.5811388300841898
//  should be able to find Population Standard Dev of a list
    PopulationStdDev_dev_q: {value: 1.4142135623730951, unit: 'ml'},

// AllTrue
//  should be able to calculate all true
    AllTrue_at: {value: true},
    AllTrue_atwn: {value: false},
    AllTrue_atf: false,
    AllTrue_atfwn: false,

// AnyTrue
//  should be able to calculate any true
    AnyTrue_at: {value: true},
    AnyTrue_atwn: {value: true},
    AnyTrue_atf: {value: false},
    AnyTrue_atfwn: {value: false},

// Add
//  should add two numbers
    Add_OnePlusTwo: {value: 3},
//  should add multiple numbers
    Add_AddMultiple: {value: 55},
//  should add variables
    Add_AddVariables: {value: 21},

// Subtract
//  should subtract two numbers
    Subtract_FiveMinusTwo: {value: 3},
//  should subtract multiple numbers
    Subtract_SubtractMultiple: {value: 15},
//  should subtract variables
    Subtract_SubtractVariables: {value: 1},

// Multiply
//  should multiply two numbers
    Multiply_FiveTimesTwo: {value: 10},
//  should multiply multiple numbers
    Multiply_MultiplyMultiple: {value: 120},
//  should multiply variables
    Multiply_MultiplyVariables: {value: 110},

// Divide
//  should divide two numbers
    Divide_TenDividedByTwo: {value: 5},
//  should divide two numbers that don\'t evenly divide
    Divide_TenDividedByFour: {value: 2.5},
//  should divide multiple numbers
    Divide_DivideMultiple: {value: 5},
//  should divide variables
    Divide_DivideVariables: {value: 25},

// Negate
//  should negate a number
    Negate_NegativeOne: {value: -1},

// MathPrecedence
//  should follow order of operations
    MathPrecedence_Mixed: {value: 46},
//  should allow parentheses to override order of operations
    MathPrecedence_Parenthetical: {value: -10},

// Power
//  should be able to calculate the power of a number
    Power_Pow: {value: 81},

// TruncatedDivide
//  should be able to return just the integer portion of a division
    TruncatedDivide_Trunc: {value: 3},
    TruncatedDivide_Even: {value: 3},

// Truncate
//  should be able to return the integer portion of a number
    Truncate_Trunc: {value: 10},
    Truncate_Even: {value: 10},

// Floor
//  should be able to round down to the closest integer
    Floor_flr: {value: 10},
    Floor_Even: {value: 10},

// Ceiling
//  should be able to round up to the closest integer
      Ceiling_Ceil: {value: 11},
      Ceiling_Even: {value: 10},

// Ln
//  should be able to return the natural log of a number
    Ln_ln: {value: Math.log(4)},

// Log
//  should be able to return the log of a number based on an arbitary base value
      Log_log: {value: 0.25},

// Modulo
//  should be able to return the remainder of a division
      Modulo_Mod: {value: 1},

// Abs
//  should be able to return the absolute value of a positive number
    Abs_Pos: {value: 10},
//  should be able to return the absolute value of a negative number
    Abs_Neg: {value: 10},
//  should be able to return the absolute value of 0
    Abs_Zero: {value: 0},

// Round
//  should be able to round a number up or down to the closest integer value
    Round_Up: {value: 5},
    Round_Down: {value: 4},
//  should be able to round a number up or down to the closest decimal place
    Round_Up_percent: {value: 4.6},
    Round_Down_percent: {value: 4.4},

// Successor
//  should be able to get Integer Successor
    Successor_Is: {value: 3},
//  should be able to get Real Successor
    Successor_Rs: {value: ( 2.2  + Math.pow(10,-8) )},
//  should cause runtime error for Successor greater than Integer Max value
    Successor_ofr: {throws: true},

//  should be able to get Date Successor for year
    Successor_y_date: {
      properties: {year: 2016},
      not: ['month', 'day', 'hour', 'minute', 'second', 'millisecond']},

//  should be able to get Date Successor for year,month
    Successor_ym_date: {
      properties: {year: 2015, month: 2},
      not: ['day', 'hour', 'minute', 'second', 'millisecond']},

//  should be able to get Date Successor for year,month,day
    Successor_ymd_date: {
      properties: {year: 2015, month: 1, day: 2},
      not: [ 'hour', 'minute', 'second', 'millisecond']},
//  should be able to get Date Successor for year,month,day,hour
    Successor_ymdh_date: {
      properties: {year: 2015, month: 1, day: 1, hour: 1},
      not: ['minute', 'second', 'millisecond']},

//  should be able to get Date Successor for year,month,day,hour,minute
    Successor_ymdhm_date: {
      properties: {year: 2015, month: 1, day: 1, hour: 0, minute: 1},
      not: ['second', 'millisecond']},

//  should be able to get Date Successor for year,month,day,hour,minute,seconds
    Successor_ymdhms_date: {
      properties: {year: 2015, month: 1, day: 1, hour: 0, minute: 0, second: 1},
      not: ['millisecond']},

//  should be able to get Date Successor for year,month,day,hour,minute,seconds,milliseconds
    Successor_ymdhmsm_date: {
      properties: {year: 2015, month: 1, day: 1, hour: 0, minute: 0, second: 0,
millisecond: 1}},

//  should throw an exception when attempting to get the Successor of the maximum allowed date
     Successor_max_date: {throws: true},

// Predecessor
//  should be able to get Integer Predecessor
    Predecessor_Is: {value: 1},
//  should be able to get Real Predecessor
    Predecessor_Rs: {value: ( 2.2  - Math.pow(10,-8))},
//  should cause runtime error for Predecessor greater than Integer Max value
    Predecessor_ufr: {throws: true},
//  should be able to get Date Predecessor for year
    Predecessor_y_date: {
      properties: {year: 2014},
      not: ['month', 'day', 'hour', 'minute', 'second', 'millisecond']},
//  should be able to get Date Predecessor for year,month
    Predecessor_ym_date: {
      properties: {year: 2014, month: 12},
      not: ['day', 'hour', 'minute', 'second', 'millisecond']},
//  should be able to get Date Predecessor for year,month,day
    Predecessor_ymd_date: {
      properties: {year: 2014, month: 12, day: 31},
      not: ['hour', 'minute', 'second', 'millisecond']},
//  should be able to get Date Predecessor for year,month,day,hour
    Predecessor_ymdh_date: {
      properties: {year: 2014, month: 12, day: 31, hour: 23},
      not: ['minute', 'second', 'millisecond']},
//  should be able to get Date Predecessor for year,month,day,hour,minute
    Predecessor_ymdhm_date: {
      properties: {year: 2014, month: 12, day: 31, hour: 23, minute: 59},
      not: ['second', 'millisecond']},
//  should be able to get Date Predecessor for year,month,day,hour,minute,seconds
    Predecessor_ymdhms_date: {
      properties: {year: 2014, month: 12, day: 31, hour: 23, minute: 59, second: 59},
      not: ['millisecond']},

//  should be able to get Date Predecessor for year,month,day,hour,minute,seconds,milliseconds
    Predecessor_ymdhmsm_date: {
      properties: {year: 2014, month: 12, day: 31, hour: 23, minute: 59, millisecond: 999}},

//  should throw an exception when attempting to get the Predecessor of the minimum allowed date
    //- see https://jira.oncprojectracking.org/browse/CQLIT-17
    // Predecessor_min_date: {throws: true},

// Quantity
//  should be able to perform Quantity Addition
Quantity_add_q_q: {value: 20, unit: 'days'},
Quantity_add_d_q: {
    type: "DateTime",
    properties: {year: 2000, month: 1, day: 11}
},
//  should be able to perform Quantity Subtraction
Quantity_sub_q_q: {value: 0, unit: 'days'},
Quantity_sub_d_q: {
    type: "DateTime",
    properties: {year: 1999, month: 12, day: 22}
},
//  should be able to perform Quantity Division
Quantity_div_q_d: {
    type: "Quantity",
    unit: "days",
    value: 5,
},
Quantity_div_q_q: {value: 1},
//  should be able to perform Quantity Multiplication
Quantity_mul_d_q: {
    type: "Quantity",
    unit: "days",
    value: 20,
},
Quantity_mul_q_d: {
    type: "Quantity",
    unit: "days",
    value: 20,
},
//  should be able to perform Quantity Absolution
Quantity_abs: {value: 10, unit: 'days'},
//  should be able to perform Quantity Negation
Quantity_neg: {value: -10, unit: 'days'},

/*
vsets = require './valuesets'
{ p1, p2 } = require './patients'
{ PatientSource} = require '../../../lib/cql-patient'


// ValueSetDef
  , [], vsets
//  should return a resolved value set when the codeService knows about it
    vs = @known.exec(@ctx)
    vs.oid.should.equal '2.16.840.1.113883.3.464.1003.101.12.1061'
    vs.version.should.equal '20140501'
    vs.codes.length.should.equal 3
//  should execute one-arg to ValueSet with ID
    vs = @['unknown One Arg'].exec(@ctx)
    vs.oid.should.equal '1.2.3.4.5.6.7.8.9'
    should.not.exist vs.version
//  should execute two-arg to ValueSet with ID and version
    vs = @['unknown Two Arg'].exec(@ctx)
    vs.oid.should.equal '1.2.3.4.5.6.7.8.9'
    vs.version.should.equal '1'

// ValueSetRef
//  should have a name
    @foo.name.should.equal 'Acute Pharyngitis'
//  should execute to the defined value set
    @foo.exec(@ctx).oid.should.equal '2.16.840.1.113883.3.464.1003.101.12.1001'

// InValueSet
  , [], vsets
//  should find string code in value set
    string: {value: true},
//  should find string code in versioned value set
    stringInVersionedValueSet: {value: true},
//  should find short code in value set
    shortCode: {value: true},
//  should find medium code in value set
    mediumCode: {value: true},
//  should find long code in value set
    longCode: {value: true},
//  should not find string code in value set
    wrongString: {value: false},
//  should not find string code in versioned value set
    wrongStringInVersionedValueSet: {value: false},
//  should not find short code in value set
    wrongShortCode: {value: false},
//  should not find medium code in value set
    wrongMediumCode: {value: false},
//  should not find long code in value set
    wrongLongCode: {value: false},

// Patient Property In ValueSet
  , [], vsets
//  should find that John is not female
    @ctx.patient =  new PatientSource([ p1 ]).currentPatient()
    isFemale: {value: false},
//  should find that Sally is female
    @ctx.patient =  new PatientSource([ p2 ]).currentPatient()
    isFemale: {value: true},

// CalculateAge
  , [ p1 ]
    # Note, tests are inexact (otherwise test needs to repeat exact logic we're testing)
    # p1 birth date is 1980-06-17
    @bday = new Date(1980, 5, 17)
    @today = new Date()
    # according to spec, dates without timezones are in *current* time offset, so need to adjust
    offsetDiff = @today.getTimezoneOffset() - @bday.getTimezoneOffset()
    @bday.setMinutes(@bday.getMinutes() + offsetDiff)

    # this is getting the possible number of months in years with the addtion of an offset
    # to get the correct number of months
    @full_months = ((@today.getFullYear() - 1980) * 12) + (@today.getMonth() - 5)
    @timediff = @today - @bday # diff in milliseconds
//  should execute age in years
    years: {value: @full_months // 12},
//  should execute age in months
    # what is returned will depend on whether the day in the current month has
    # made it to the 17th day of the month as declared in the birthday
    [@full_months, @full_months-1].indexOf(@months.exec(@ctx)).should.not.equal -1
//  should execute age in days
    days: {value: @timediff // 1000 // 60 // 60 // 24},
//  should execute age in hours
    # a little strange since the qicore data model specified birthDate as a date (no time)
    hours: {value: @timediff // 1000 // 60 // 60},
//  should execute age in minutes
    # a little strange since the qicore data model specified birthDate as a date (no time)
    minutes: {value: @timediff // 1000 // 60},
//  should execute age in seconds
    # a little strange since the qicore data model specified birthDate as a date (no time)
    seconds: {value: @timediff // 1000},

// CalculateAgeAt
  , [ p1 ]
//  should execute age at 2012 as 31
    ageAt2012: {value: 31},
//  should execute age at 19810216 as 0
    ageAt19810216: {value: 0},
//  should execute age at 1975 as -5
    ageAt19810216: {value: 0},

# TO Comparisons for Dates

*/

// Equal
//  should be false for 5 = 4
    Equal_AGtB_Int: {value: false},
//  should be true for 5 = 5
    Equal_AEqB_Int: {value: true},
//  should be false for 5 = 6
    Equal_ALtB_Int: {value: false},
//  should identify equal/unequal tuples
    Equal_EqTuples: {value: true},
    Equal_UneqTuples: {value: false},
//  should identify equal/unequal DateTimes in same timezone
    Equal_EqDateTimes: {value: true},
    Equal_UneqDateTimes: {value: false},
//  should identify equal/unequal DateTimes in different timezones
    Equal_EqDateTimesTZ: {value: true},

    Equal_UneqDateTimesTZ: {value: false},
//  should identify uncertain/unequal DateTimes when there is imprecision
    Equal_PossiblyEqualDateTimes: {value: null},
    Equal_ImpossiblyEqualDateTimes: {value: false},


// NotEqual
//  should be true for 5 <> 4
    NotEqual_AGtB_Int: {value: true},
//  should be false for 5 <> 5
    NotEqual_AEqB_Int: {value: false},
//  should be true for 5 <> 6
    NotEqual_ALtB_Int: {value: true},
//  should identify equal/unequal tuples
    NotEqual_EqTuples: {value: false},
    NotEqual_UneqTuples: {value: true},
//  should identify equal/unequal DateTimes in same timezone
    NotEqual_EqDateTimes: {value: false},
    NotEqual_UneqDateTimes: {value: true},
//  should identify equal/unequal DateTimes in different timezones
    NotEqual_EqDateTimesTZ: {value: false},
    NotEqual_UneqDateTimesTZ: {value: true},
//  should identify uncertain/unequal DateTimes when there is imprecision
    NotEqual_PossiblyEqualDateTimes: {value: null},
    NotEqual_ImpossiblyEqualDateTimes: {value: true},

// Less
//  should be false for 5 < 4
    Less_AGtB_Int: {value: false},
//  should be false for 5 < 5
    Less_AEqB_Int: {value: false},
//  should be true for 5 < 6
    Less_ALtB_Int: {value: true},

// LessOrEqual
//  should be false for 5 <= 4
    LessOrEqual_AGtB_Int: {value: false},
//  should be true for 5 <= 5
    LessOrEqual_AEqB_Int: {value: true},
//  should be true for 5 <= 6
    LessOrEqual_ALtB_Int: {value: true},

// Greater
//  should be true for 5 > 4
    Greater_AGtB_Int: {value: true},
//  should be false for 5 > 5
    Greater_AEqB_Int: {value: false},
//  should be false for 5 > 6
    Greater_ALtB_Int: {value: false},

// GreaterOrEqual
//  should be true for 5 >= 4
    GreaterOrEqual_AGtB_Int: {value: true},
//  should be true for 5 >= 5
    GreaterOrEqual_AEqB_Int: {value: true},
//  should be false for 5 >= 6
    GreaterOrEqual_ALtB_Int: {value: false},

    /* fixme todo
// If
//  should return the correct value when the expression is true
    @exp.exec(@ctx.withParameters { var: true }).should.equal "true return"
//  should return the correct value when the expression is false
    @exp.exec(@ctx.withParameters { var: false }).should.equal "false return"


// Case
//  should be able to execute a standard case statement
    vals =  [{"x" : 1, "y" : 2, "message" : "X < Y"},
      {"x" : 2, "y" : 1, "message" : "X > Y"},
      {"x" : 1, "y" : 1, "message" : "X == Y"}]
    for item in vals
      @ctx.withParameters { X : item.x, Y: item.y }
      standard: {value: item.message},
//  should be able to execute a selected case statement
    vals = [{"var" : 1, "message" : "one"},
      {"var" : 2, "message" : "two"},
      {"var" : 3, "message" : "?"}]
    for item in vals
      @ctx.withParameters { var : item.var }
      selected: {value: item.message},
{isNull} = require '../../../lib/util/util'

    */
// FromString
//  should convert 'str' to 'str'
    FromString_stringStr: {value: "str"},
//  should convert null to null
    FromString_stringNull: {value: null},
//  should convert 'true' to true
    FromString_boolTrue: {value: true},
//  should convert 'false' to false
    FromString_boolFalse: {value: false},
//  should convert '10.2' to Decimal
    FromString_decimalValid: {value: 10.2},
//  should convert 'abc' to Decimal NaN
    FromString_decimalInvalid: {throws: true},
//  should convert '10' to Integer
    FromString_integerValid: {value: 10},
//  should convert '10.2' to Integer 10
    FromString_integerDropDecimal: {value: 10},
//  should convert 'abc' to Integer NaN
    FromString_integerInvalid: {throws: true},
    }
} /*
//  should convert \"10 'A'\" to Quantity
    quantity = @quantityStr.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal "A"
//  should convert \"+10 'A'\" to Quantity
    quantity = @posQuantityStr.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal "A"
//  should convert \"-10 'A'\" to Quantity
    quantity = @negQuantityStr.exec(@ctx)
    quantity.value.should.equal -10
    quantity.unit.should.equal "A"
//  should convert \"10.0'mA'\" to Quantity
    quantity = @quantityStrDecimal.exec(@ctx)
    quantity.value.should.equal 10.0
    quantity.unit.should.equal "mA"
//  should convert '2015-01-02' to DateTime
    date = @dateStr.exec(@ctx)
    date.year.should.equal 2015
    date.month.should.equal 1
    date.day.should.equal 2

// FromInteger
//  should convert 10 to '10'
    string10: {value: "10"},
//  should convert 10 to 10.0
    decimal10: {value: 10.0},
//  should convert null to null
    intNull: {value: null},
//  should convert 10 to 10
    intInt: {value: 10},

// FromQuantity
//  should convert \"10 'A'\" to \"10 'A'\"
    quantityStr: {value: "10 'A'"},
//  should convert \"+10 'A'\" to \"10 'A'\"
    posQuantityStr: {value: "10 'A'"},
//  should convert \"-10 'A'\" to \"10 'A'\"
    negQuantityStr: {value: "-10 'A'"},
//  should convert \"10 'A'\" to \"10 'A'\"
    quantity = @quantityQuantity.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal 'A'

// FromBoolean
//  should convert true to 'true'
    booleanTrueStr: {value: "true"},
//  should convert false to 'false'
    booleanFalseStr: {value: "false"},
//  should convert true to true
    booleanTrueBool: {value: true},
//  should convert false to false
    booleanFalseBool: {value: false},

// FromDateTime
//  should convert @2015-01-02 to '2015-01-02'
    dateStr: {value: "2015-01-02"},
//  should convert @2015-01-02 to @2015-01-02
    date = @dateDate.exec(@ctx)
    date.year.should.equal 2015
    date.month.should.equal 1
    date.day.should.equal 2

// FromTime
  it.skip "should convert @T11:57 to '11:57'", ->
    timeStr: {value: "11:57"},

  it.skip "should convert @T11:57 to @11:57", ->
    time = @timeTime.exec(@ctx)
    time.hour.should.equal 11
    time.minute.should.equal 57

// FromCode
  it.skip "should convert hepB to a concept", ->
    concept = @codeConcept.exec(@ctx)

  it.skip "should convert hepB to a code", ->
    code = @codeCode.exec(@ctx)
{ Uncertainty } = require '../../../lib/datatypes/uncertainty'

// DateTime
    @defaultOffset = (new Date()).getTimezoneOffset() / 60 * -1
//  should execute year precision correctly
    d = @year.exec(@ctx)
    d.year.should.equal 2012
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'month', 'day', 'hour', 'minute', 'second', 'millisecond' ]
//  should execute month precision correctly
    d = @month.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'day', 'hour', 'minute', 'second', 'millisecond' ]
//  should execute day precision correctly
    d = @day.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'hour', 'minute', 'second', 'millisecond' ]
//  should execute hour precision correctly
    d = @hour.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'minute', 'second', 'millisecond' ]
//  should execute minute precision correctly
    d = @minute.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'second', 'millisecond' ]
//  should execute second precision correctly
    d = @second.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d.millisecond)
//  should execute millisecond precision correctly
    d = @millisecond.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.millisecond.should.equal 456
    d.timezoneOffset.should.equal @defaultOffset
//  should execute timezone offsets correctly
    d = @timezoneOffset.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.millisecond.should.equal 456
    d.timezoneOffset.should.equal -8

// Today
//  should return only day components and timezone of today
    jsDate = new Date()
    today = @todayVar.exec @ctx
    today.year.should.equal jsDate.getFullYear()
    today.month.should.equal jsDate.getMonth() + 1
    today.day.should.equal jsDate.getDate()
    today.timezoneOffset.should.equal jsDate.getTimezoneOffset() / 60 * -1
    should.not.exist(today[field]) for field in [ 'hour', 'minute', 'second', 'millisecond' ]

// Now
//  should return all date components representing now
    jsDate = new Date()
    now = @nowVar.exec @ctx
    now.year.should.equal jsDate.getFullYear()
    now.month.should.equal jsDate.getMonth() + 1
    now.day.should.equal jsDate.getDate()
    now.hour.should.equal jsDate.getHours()
    now.minute.should.exist
    now.second.should.exist
    now.millisecond.should.exist
    now.timezoneOffset.should.equal jsDate.getTimezoneOffset() / 60 * -1

// DateTimeComponentFrom
//  should return the year from the date
    year: {value: 2000},
//  should return the month from the date
    month: {value: 3},
//  should return the day from the date
    day: {value: 15},
//  should return the hour from the date
    hour: {value: 13},
//  should return the minute from the date
    minute: {value: 30},
//  should return the second from the date
    second: {value: 25},
//  should return the millisecond from the date
    millisecond: {value: 200},
//  should return null for imprecise components
    result = @impreciseComponentTuple.exec(@ctx)
    result.should.eql {
      Year: 2000,
      Month: 3,
      Day: 15,
      Hour: null,
      Minute: null,
      Second: null,
      Millisecond: null
    }
//  should return null for null date
    should(@nullDate.exec(@ctx)).be.null

// DateFrom
//  should return the date from a fully defined DateTime
    date = @date.exec(@ctx)
    date.year.should.equal 2000
    date.month.should.equal 3
    date.day.should.equal 15
    date.timezoneOffset.should.equal 1
    should.not.exist date.hour
    should.not.exist date.minute
    should.not.exist date.second
    should.not.exist date.millisecond
//  should return the defined date components from an imprecise date
    date = @impreciseDate.exec(@ctx)
    date.year.should.equal 2000
    should.not.exist date.month
    should.not.exist date.day
    should.not.exist date.hour
    should.not.exist date.minute
    should.not.exist date.second
    should.not.exist date.millisecond
//  should return null for null date
    should(@nullDate.exec(@ctx)).be.null

// TimeFrom
//  should return the time from a fully defined DateTime (and date should be lowest expressible date)
    time = @time.exec(@ctx)
    time.year.should.equal 1900
    time.month.should.equal 1
    time.day.should.equal 1
    time.hour.should.equal 13
    time.minute.should.equal 30
    time.second.should.equal 25
    time.millisecond.should.equal 200
    time.timezoneOffset.should.equal 1
//  should return the null time components from a date with no time
    noTime = @noTime.exec(@ctx)
    noTime.year.should.equal 1900
    noTime.month.should.equal 1
    noTime.day.should.equal 1
    should.not.exist noTime.hour
    should.not.exist noTime.minute
    should.not.exist noTime.second
    should.not.exist noTime.millisecond
//  should return null for null date
    should(@nullDate.exec(@ctx)).be.null

// TimezoneFrom
//  should return the timezone from a fully defined DateTime
    centralEuropean: {value: 1},
    easternStandard: {value: -5},
//  should return the default timezone when not specified
    defaultTimezone: {value: (new Date()).getTimezoneOffset() / 60 * -1},
//  should return null for null date
    should(@nullDate.exec(@ctx)).be.null

// SameAs
//  should properly determine when year is the same
    sameYear: {value: true},
    notSameYear: {value: false},
//  should properly determine when month is the same
    sameMonth: {value: true},
    notSameMonth: {value: false},
    sameMonthWrongYear: {value: false},
//  should properly determine when day is the same
    sameDay: {value: true},
    notSameDay: {value: false},
    sameDayWrongMonth: {value: false},
//  should properly determine when hour is the same
    sameHour: {value: true},
    notSameHour: {value: false},
    sameHourWrongDay: {value: false},
//  should properly determine when minute is the same
    sameMinute: {value: true},
    notSameMinute: {value: false},
    sameMinuteWrongHour: {value: false},
//  should properly determine when second is the same
    sameSecond: {value: true},
    notSameSecond: {value: false},
    sameSecondWrongMinute: {value: false},
//  should properly determine when millisecond is the same
    sameMillisecond: {value: true},
    notSameMillisecond: {value: false},
    sameMillisecondWrongSecond: {value: false},
//  should properly determine same as using milliseconds
    same: {value: true},
    notSame: {value: false},
//  should normalize timezones when determining sameness
    sameNormalized: {value: true},
    sameHourWrongTimezone: {value: false},
//  should handle imprecision
    should(@impreciseHour.exec(@ctx)).be.null
    impreciseHourWrongDay: {value: false},
//  should return null when either argument is null
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

// SameOrAfter
//  should properly determine when year is same or after
    sameYear: {value: true},
    yearAfter: {value: true},
    yearBefore: {value: false},
//  should properly determine when month is same or after
    sameMonth: {value: true},
    monthAfter: {value: true},
    monthBefore: {value: false},
//  should properly determine when day is same or after
    sameDay: {value: true},
    dayAfter: {value: true},
    dayBefore: {value: false},
//  should properly determine when hour is same or after
    sameHour: {value: true},
    hourAfter: {value: true},
    hourBefore: {value: false},
//  should properly determine when minute is same or after
    sameMinute: {value: true},
    minuteAfter: {value: true},
    minuteBefore: {value: false},
//  should properly determine when second is same or after
    sameSecond: {value: true},
    secondAfter: {value: true},
    secondBefore: {value: false},
//  should properly determine when millisecond is same or after
    sameMillisecond: {value: true},
    millisecondAfter: {value: true},
    millisecondBefore: {value: false},
//  should properly determine same or after using ms when no precision defined
    same: {value: true},
    after: {value: true},
    before: {value: false},
//  should consider precision units above the specified unit
    sameDayMonthBefore: {value: false},
    dayAfterMonthBefore: {value: false},
    dayBeforeMonthAfter: {value: true},
//  should handle imprecision
    should(@impreciseDay.exec(@ctx)).be.null
    impreciseDayMonthAfter: {value: true},
    impreciseDayMonthBefore: {value: false},
//  should normalize timezones
    sameHourNormalizeZones: {value: true},
    hourAfterNormalizeZones: {value: true},
    hourBeforeNormalizeZones: {value: false},
//  should return null when either argument is null
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

// SameOrBefore
//  should properly determine when year is same or after
    sameYear: {value: true},
    yearAfter: {value: false},
    yearBefore: {value: true},
//  should properly determine when month is same or after
    sameMonth: {value: true},
    monthAfter: {value: false},
    monthBefore: {value: true},
//  should properly determine when day is same or after
    sameDay: {value: true},
    dayAfter: {value: false},
    dayBefore: {value: true},
//  should properly determine when hour is same or after
    sameHour: {value: true},
    hourAfter: {value: false},
    hourBefore: {value: true},
//  should properly determine when minute is same or after
    sameMinute: {value: true},
    minuteAfter: {value: false},
    minuteBefore: {value: true},
//  should properly determine when second is same or after
    sameSecond: {value: true},
    secondAfter: {value: false},
    secondBefore: {value: true},
//  should properly determine when millisecond is same or after
    sameMillisecond: {value: true},
    millisecondAfter: {value: false},
    millisecondBefore: {value: true},
//  should properly determine same or after using ms when no precision defined
    same: {value: true},
    after: {value: false},
    before: {value: true},
//  should consider precision units above the specified unit
    sameDayMonthBefore: {value: true},
    dayAfterMonthBefore: {value: true},
    dayBeforeMonthAfter: {value: false},
//  should handle imprecision
    should(@impreciseDay.exec(@ctx)).be.null
    impreciseDayMonthAfter: {value: false},
    impreciseDayMonthBefore: {value: true},
//  should normalize timezones
    sameHourNormalizeZones: {value: true},
    hourAfterNormalizeZones: {value: false},
    hourBeforeNormalizeZones: {value: true},
//  should return null when either argument is null
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

// After
//  should properly determine when year is same or after
    sameYear: {value: false},
    yearAfter: {value: true},
    yearBefore: {value: false},
//  should properly determine when month is same or after
    sameMonth: {value: false},
    monthAfter: {value: true},
    monthBefore: {value: false},
//  should properly determine when day is same or after
    sameDay: {value: false},
    dayAfter: {value: true},
    dayBefore: {value: false},
//  should properly determine when hour is same or after
    sameHour: {value: false},
    hourAfter: {value: true},
    hourBefore: {value: false},
//  should properly determine when minute is same or after
    sameMinute: {value: false},
    minuteAfter: {value: true},
    minuteBefore: {value: false},
//  should properly determine when second is same or after
    sameSecond: {value: false},
    secondAfter: {value: true},
    secondBefore: {value: false},
//  should properly determine when millisecond is same or after
    sameMillisecond: {value: false},
    millisecondAfter: {value: true},
    millisecondBefore: {value: false},
//  should properly determine same or after using ms when no precision defined
    same: {value: false},
    after: {value: true},
    before: {value: false},
//  should handle imprecision
    should(@impreciseDay.exec(@ctx)).be.null
    impreciseDayMonthAfter: {value: true},
    impreciseDayMonthBefore: {value: false},
//  should normalize timezones
    sameHourNormalizeZones: {value: false},
    hourAfterNormalizeZones: {value: true},
    hourBeforeNormalizeZones: {value: false},
//  should return null when either argument is null
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

// Before
//  should properly determine when year is same or after
    sameYear: {value: false},
    yearAfter: {value: false},
    yearBefore: {value: true},
//  should properly determine when month is same or after
    sameMonth: {value: false},
    monthAfter: {value: false},
    monthBefore: {value: true},
//  should properly determine when day is same or after
    sameDay: {value: false},
    dayAfter: {value: false},
    dayBefore: {value: true},
//  should properly determine when hour is same or after
    sameHour: {value: false},
    hourAfter: {value: false},
    hourBefore: {value: true},
//  should properly determine when minute is same or after
    sameMinute: {value: false},
    minuteAfter: {value: false},
    minuteBefore: {value: true},
//  should properly determine when second is same or after
    sameSecond: {value: false},
    secondAfter: {value: false},
    secondBefore: {value: true},
//  should properly determine when millisecond is same or after
    sameMillisecond: {value: false},
    millisecondAfter: {value: false},
    millisecondBefore: {value: true},
//  should properly determine same or after using ms when no precision defined
    same: {value: false},
    after: {value: false},
    before: {value: true},
//  should handle imprecision
    should(@impreciseDay.exec(@ctx)).be.null
    impreciseDayMonthAfter: {value: false},
    impreciseDayMonthBefore: {value: true},
//  should normalize timezones
    sameHourNormalizeZones: {value: false},
    hourAfterNormalizeZones: {value: false},
    hourBeforeNormalizeZones: {value: true},
//  should return null when either argument is null
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

// DurationBetween
//  should properly execute years between
    yearsBetween: {value: 1},
//  should properly execute months between
    monthsBetween: {value: 12},
//  should properly execute days between
    daysBetween: {value: 365},
//  should properly execute hours between
    hoursBetween: {value: 24 * 365},
//  should properly execute minutes between
    minutesBetween: {value: 60 * 24 * 365},
//  should properly execute seconds between
    secondsBetween: {value: 60 * 60 * 24 * 365},
//  should properly execute milliseconds between
    millisecondsBetween: {value: 1000 * 60 * 60 * 24 * 365},
//  should properly execute milliseconds between when date 1 is after date 2
    millisecondsBetweenReversed: {value: -1 * 1000 * 60 * 60 * 24 * 365},
//  should properly execute years between with an uncertainty
    yearsBetweenUncertainty: {value: 0},
//  should properly execute months between with an uncertainty
    monthsBetweenUncertainty: {value: 0},
//  should properly execute days between with an uncertainty
    daysBetweenUncertainty: {value: new Uncertainty(0, 30)},
//  should properly execute hours between with an uncertainty
    hoursBetweenUncertainty: {value: new Uncertainty(0, 743)},
//  should properly execute minutes between with an uncertainty
    minutesBetweenUncertainty: {value: new Uncertainty(0, 44639)},
//  should properly execute seconds between with an uncertainty
    secondsBetweenUncertainty: {value: new Uncertainty(0, 2678399)},
//  should properly execute milliseconds between with an uncertainty
    millisecondsBetweenUncertainty: {value: new Uncertainty(0, 2678399999)},
//  should properly execute seconds between when date 1 is after date 2 with an uncertainty
    millisecondsBetweenReversedUncertainty: {value: new Uncertainty(-2678399999, 0)},

// DurationBetween Comparisons
//  should calculate days between > x
    greaterThan25DaysAfter: {value: true},
    should(@greaterThan40DaysAfter.exec(@ctx)).be.null
    greaterThan80DaysAfter: {value: false},
//  should calculate days between >= x
    greaterOrEqualTo25DaysAfter: {value: true},
    should(@greaterOrEqualTo40DaysAfter.exec(@ctx)).be.null
    greaterOrEqualTo80DaysAfter: {value: false},
//  should calculate days between = x
    equalTo25DaysAfter: {value: false},
    should(@equalTo40DaysAfter.exec(@ctx)).be.null
    equalTo80DaysAfter: {value: false},
//  should calculate days between <= x
    lessOrEqualTo25DaysAfter: {value: false},
    should(@lessOrEqualTo40DaysAfter.exec(@ctx)).be.null
    lessOrEqualTo80DaysAfter: {value: true},
//  should calculate days between < x
    lessThan25DaysAfter: {value: false},
    should(@lessThan40DaysAfter.exec(@ctx)).be.null
    lessThan80DaysAfter: {value: true},
//  should calculate other way too
    twentyFiveDaysLessThanDaysBetween: {value: true},
    should(@fortyDaysEqualToDaysBetween.exec(@ctx)).be.null
    twentyFiveDaysGreaterThanDaysBetween: {value: false},


{ p1, p2 } = require './patients'

// Age
  , [ p1, p2 ]
    @results = @executor.withLibrary(@lib).exec(@patientSource)
//  should have correct patient results
    @results.patientResults['1'].Age.should.equal 32
    @results.patientResults['2'].Age.should.equal 5
//  should have the correct population results
    @results.populationResults.AgeSum.should.equal 37
//  should be able to reference other population context expressions
    @results.populationResults.AgeSumRef.should.equal 37

vsets = require './valuesets'
{ p1 } = require './patients'

// Retrieve
  , [ p1 ], vsets
//  should find conditions
    c = @conditions.exec(@ctx)
    c.should.have.length(2)
    c[0].id().should.equal 'http://cqframework.org/3/2'
    c[1].id().should.equal 'http://cqframework.org/3/4'
//  should find encounter performances
    e = @encounters.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal 'http://cqframework.org/3/1'
    e[1].id().should.equal 'http://cqframework.org/3/3'
    e[2].id().should.equal 'http://cqframework.org/3/5'
//  should find observations with a value set
    p = @pharyngitisConditions.exec(@ctx)
    p.should.have.length(1)
    p[0].id().should.equal 'http://cqframework.org/3/2'
//  should find encounter performances with a value set
    a = @ambulatoryEncounters.exec(@ctx)
    a.should.have.length(3)
    a[0].id().should.equal 'http://cqframework.org/3/1'
    a[1].id().should.equal 'http://cqframework.org/3/3'
    a[2].id().should.equal 'http://cqframework.org/3/5'
//  should find encounter performances by service type
    e = @encountersByServiceType.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal 'http://cqframework.org/3/1'
    e[1].id().should.equal 'http://cqframework.org/3/3'
    e[2].id().should.equal 'http://cqframework.org/3/5'
//  should not find conditions with wrong valueset
    e = @wrongValueSet.exec(@ctx)
    e.should.be.empty
//  should not find encounter performances using wrong codeProperty
    e = @wrongCodeProperty.exec(@ctx)
    e.should.be.empty
{ DateTime } = require '../../../lib/datatypes/datetime'

// Instance
//  should create generic json objects with the correct key values
    @quantity.exec(@ctx).unit.should.eql 'a'
    @quantity.exec(@ctx).value.should.eql 12
    @med.exec(@ctx).isBrand.should.eql false
    @med.exec(@ctx).name.should.eql "Best Med Ever"
    val: {value: 12},


{ Interval } = require '../../../lib/datatypes/interval'
{ DateTime } = require '../../../lib/datatypes/datetime'

// Interval
//  should properly represent an open interval
    @open.lowClosed.should.be.false
    @open.highClosed.should.be.false
    @open.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @open.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)
//  should properly represent a left-open interval
    @leftOpen.lowClosed.should.be.false
    @leftOpen.highClosed.should.be.true
    @leftOpen.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @leftOpen.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)
//  should properly represent a right-open interval
    @rightOpen.lowClosed.should.be.true
    @rightOpen.highClosed.should.be.false
    @rightOpen.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @rightOpen.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)
//  should properly represent a closed interval
    @closed.lowClosed.should.be.true
    @closed.highClosed.should.be.true
    @closed.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @closed.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)
//  should exec to native Interval datatype
    ivl = @open.exec(@cql)
    ivl.should.be.instanceOf Interval
    ivl.lowClosed.should.equal @open.lowClosed
    ivl.highClosed.should.equal @open.highClosed
    ivl.low.should.eql new DateTime(2012, 1, 1)
    ivl.high.should.eql new DateTime(2013, 1, 1)

// Equal
//  should determine equal integer intervals
    equalClosed: {value: true},
    equalOpen: {value: true},
    equalOpenClosed: {value: true},
//  should determine unequal integer intervals
    unequalClosed: {value: false},
    unequalOpen: {value: false},
    unequalClosedOpen: {value: false},
//  should determine equal datetime intervals
    equalDates: {value: true},
    equalDatesOpenClosed: {value: true},
//  should operate correctly with imprecision
    should(@sameDays.exec(@ctx)).be.null
    differentDays: {value: false},

// NotEqual
//  should determine equal integer intervals
    equalClosed: {value: false},
    equalOpen: {value: false},
    equalOpenClosed: {value: false},
//  should determine unequal integer intervals
    unequalClosed: {value: true},
    unequalOpen: {value: true},
    unequalClosedOpen: {value: true},
//  should determine equal datetime intervals
    equalDates: {value: false},
    equalDatesOpenClosed: {value: false},
//  should operate correctly with imprecision
    should(@sameDays.exec(@ctx)).be.null
    differentDays: {value: true},

// Contains
//  should accept contained items
    containsInt: {value: true},
    containsReal: {value: true},
    containsDate: {value: true},
//  should reject uncontained items
    notContainsInt: {value: false},
    notContainsReal: {value: false},
    notContainsDate: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegContainsInt: {value: true},
    negInfBegNotContainsInt: {value: false},
    unknownBegContainsInt: {value: true},
    should(@unknownBegMayContainInt.exec(@ctx)).be.null
    unknownBegNotContainsInt: {value: false},
    posInfEndContainsInt: {value: true},
    posInfEndNotContainsInt: {value: false},
    unknownEndContainsInt: {value: true},
    should(@unknownEndMayContainInt.exec(@ctx)).be.null
    unknownEndNotContainsInt: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegContainsDate: {value: true},
    negInfBegNotContainsDate: {value: false},
    unknownBegContainsDate: {value: true},
    should(@unknownBegMayContainDate.exec(@ctx)).be.null
    unknownBegNotContainsDate: {value: false},
    posInfEndContainsDate: {value: true},
    posInfEndNotContainsDate: {value: false},
    unknownEndContainsDate: {value: true},
    should(@unknownEndMayContainDate.exec(@ctx)).be.null
    unknownEndNotContainsDate: {value: false},
//  should correctly handle imprecision
    containsImpreciseDate: {value: true},
    notContainsImpreciseDate: {value: false},
    should(@mayContainImpreciseDate.exec(@ctx)).be.null
    impreciseContainsDate: {value: true},
    impreciseNotContainsDate: {value: false},
    should(@impreciseMayContainDate.exec(@ctx)).be.null

// In
//  should accept contained items
    containsInt: {value: true},
    containsReal: {value: true},
    containsDate: {value: true},
//  should reject uncontained items
    notContainsInt: {value: false},
    notContainsReal: {value: false},
    notContainsDate: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegContainsInt: {value: true},
    negInfBegNotContainsInt: {value: false},
    unknownBegContainsInt: {value: true},
    should(@unknownBegMayContainInt.exec(@ctx)).be.null
    unknownBegNotContainsInt: {value: false},
    posInfEndContainsInt: {value: true},
    posInfEndNotContainsInt: {value: false},
    unknownEndContainsInt: {value: true},
    should(@unknownEndMayContainInt.exec(@ctx)).be.null
    unknownEndNotContainsInt: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegContainsDate: {value: true},
    negInfBegNotContainsDate: {value: false},
    unknownBegContainsDate: {value: true},
    should(@unknownBegMayContainDate.exec(@ctx)).be.null
    unknownBegNotContainsDate: {value: false},
    posInfEndContainsDate: {value: true},
    posInfEndNotContainsDate: {value: false},
    unknownEndContainsDate: {value: true},
    should(@unknownEndMayContainDate.exec(@ctx)).be.null
    unknownEndNotContainsDate: {value: false},
//  should correctly handle imprecision
    containsImpreciseDate: {value: true},
    notContainsImpreciseDate: {value: false},
    should(@mayContainImpreciseDate.exec(@ctx)).be.null
    impreciseContainsDate: {value: true},
    impreciseNotContainsDate: {value: false},
    should(@impreciseMayContainDate.exec(@ctx)).be.null

// Includes
//  should accept included items
    includesIntIvl: {value: true},
    includesRealIvl: {value: true},
    includesDateIvl: {value: true},
//  should reject unincluded items
    notIncludesIntIvl: {value: false},
    notIncludesRealIvl: {value: false},
    notIncludesDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegIncludesIntIvl: {value: true},
    negInfBegNotIncludesIntIvl: {value: false},
    unknownBegIncludesIntIvl: {value: true},
    should(@unknownBegMayIncludeIntIvl.exec(@ctx)).be.null
    unknownBegNotIncludesIntIvl: {value: false},
    posInfEndIncludesIntIvl: {value: true},
    posInfEndNotIncludesIntIvl: {value: false},
    unknownEndIncludesIntIvl: {value: true},
    should(@unknownEndMayIncludeIntIvl.exec(@ctx)).be.null
    unknownEndNotIncludesIntIvl: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegIncludesDateIvl: {value: true},
    negInfBegNotIncludesDateIvl: {value: false},
    unknownBegIncludesDateIvl: {value: true},
    should(@unknownBegMayIncludeDateIvl.exec(@ctx)).be.null
    unknownBegNotIncludesDateIvl: {value: false},
    posInfEndIncludesDateIvl: {value: true},
    posInfEndNotIncludesDateIvl: {value: false},
    unknownEndIncludesDateIvl: {value: true},
    should(@unknownEndMayIncludeDateIvl.exec(@ctx)).be.null
    unknownEndNotIncludesDateIvl: {value: false},
//  should correctly handle imprecision
    includesImpreciseDateIvl: {value: true},
    notIncludesImpreciseDateIvl: {value: false},
    should(@mayIncludeImpreciseDateIvl.exec(@ctx)).be.null
    impreciseIncludesDateIvl: {value: true},
    impreciseNotIncludesDateIvl: {value: false},
    should(@impreciseMayIncludeDateIvl.exec(@ctx)).be.null

// ProperlyIncludes
//  should accept properly included intervals
    properlyIncludesIntIvl: {value: true},
    properlyIncludesIntBeginsIvl: {value: true},
    properlyIncludesIntEndsIvl: {value: true},
    properlyIncludesRealIvl: {value: true},
    properlyIncludesDateIvl: {value: true},
//  should reject intervals not properly included
    notProperlyIncludesIntIvl: {value: false},
    notProperlyIncludesRealIvl: {value: false},
    notProperlyIncludesDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    posInfEndProperlyIncludesIntIvl: {value: true},
    posInfEndNotProperlyIncludesIntIvl: {value: false},
    should(@unknownEndMayProperlyIncludeIntIvl.exec(@ctx)).be.null

// IncludedIn
//  should accept included items
    includesIntIvl: {value: true},
    includesRealIvl: {value: true},
    includesDateIvl: {value: true},
//  should reject unincluded items
    notIncludesIntIvl: {value: false},
    notIncludesRealIvl: {value: false},
    notIncludesDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegIncludedInIntIvl: {value: true},
    negInfBegNotIncludedInIntIvl: {value: false},
    unknownBegIncludedInIntIvl: {value: true},
    should(@unknownBegMayBeIncludedInIntIvl.exec(@ctx)).be.null
    unknownBegNotIncludedInIntIvl: {value: false},
    posInfEndIncludedInIntIvl: {value: true},
    posInfEndNotIncludedInIntIvl: {value: false},
    unknownEndIncludedInIntIvl: {value: true},
    should(@unknownEndMayBeIncludedInIntIvl.exec(@ctx)).be.null
    unknownEndNotIncludedInIntIvl: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegIncludedInDateIvl: {value: true},
    negInfBegNotIncludedInDateIvl: {value: false},
    unknownBegIncludedInDateIvl: {value: true},
    should(@unknownBegMayBeIncludedInDateIvl.exec(@ctx)).be.null
    unknownBegNotIncludedInDateIvl: {value: false},
    posInfEndIncludedInDateIvl: {value: true},
    posInfEndNotIncludedInDateIvl: {value: false},
    unknownEndIncludedInDateIvl: {value: true},
    should(@unknownEndMayBeIncludedInDateIvl.exec(@ctx)).be.null
    unknownEndNotIncludedInDateIvl: {value: false},
//  should correctly handle imprecision
    includesImpreciseDateIvl: {value: true},
    notIncludesImpreciseDateIvl: {value: false},
    should(@mayIncludeImpreciseDateIvl.exec(@ctx)).be.null
    impreciseIncludesDateIvl: {value: true},
    impreciseNotIncludesDateIvl: {value: false},
    should(@impreciseMayIncludeDateIvl.exec(@ctx)).be.null

// ProperlyIncludedIn
//  should accept properly included intervals
    properlyIncludesIntIvl: {value: true},
    properlyIncludesIntBeginsIvl: {value: true},
    properlyIncludesIntEndsIvl: {value: true},
    properlyIncludesRealIvl: {value: true},
    properlyIncludesDateIvl: {value: true},
//  should reject intervals not properly included
    notProperlyIncludesIntIvl: {value: false},
    notProperlyIncludesRealIvl: {value: false},
    notProperlyIncludesDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    posInfEndProperlyIncludedInDateIvl: {value: true},
    posInfEndNotProperlyIncludedInDateIvl: {value: false},
    should(@unknownEndMayBeProperlyIncludedInDateIvl.exec(@ctx)).be.null

// After
//  should accept intervals before it
    afterIntIvl: {value: true},
    afterRealIvl: {value: true},
    afterDateIvl: {value: true},
//  should reject intervals on or after it
    notAfterIntIvl: {value: false},
    notAfterRealIvl: {value: false},
    notAfterDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegNotAfterIntIvl: {value: false},
    should(@unknownBegMayBeAfterIntIvl.exec(@ctx)).be.null
    unknownBegNotAfterIntIvl: {value: false},
    posInfEndAfterIntIvl: {value: true},
    posInfEndNotAfterIntIvl: {value: false},
    unknownEndAfterIntIvl: {value: true},
    unknownEndNotAfterIntIvl: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegNotAfterDateIvl: {value: false},
    should(@unknownBegMayBeAfterDateIvl.exec(@ctx)).be.null
    unknownBegNotAfterDateIvl: {value: false},
    posInfEndAfterDateIvl: {value: true},
    posInfEndNotAfterDateIvl: {value: false},
    unknownEndAfterDateIvl: {value: true},
    unknownEndNotAfterDateIvl: {value: false},
//  should correctly handle imprecision
    afterImpreciseDateIvl: {value: true},
    notAfterImpreciseDateIvl: {value: false},
    should(@mayBeAfterImpreciseDateIvl.exec(@ctx)).be.null
    impreciseAfterDateIvl: {value: true},
    impreciseNotAfterDateIvl: {value: false},
    should(@impreciseMayBeAfterDateIvl.exec(@ctx)).be.null

// Before
//  should accept intervals before it
    beforeIntIvl: {value: true},
    beforeRealIvl: {value: true},
    beforeDateIvl: {value: true},
//  should reject intervals on or after it
    notBeforeIntIvl: {value: false},
    notBeforeRealIvl: {value: false},
    notBeforeDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegBeforeIntIvl: {value: true},
    negInfBegNotBeforeIntIvl: {value: false},
    unknownBegBeforeIntIvl: {value: true},
    unknownBegNotBeforeIntIvl: {value: false},
    posInfEndNotBeforeIntIvl: {value: false},
    should(@unknownEndMayBeBeforeIntIvl.exec(@ctx)).be.null
    unknownEndNotBeforeIntIvl: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegBeforeDateIvl: {value: true},
    negInfBegNotBeforeDateIvl: {value: false},
    unknownBegBeforeDateIvl: {value: true},
    unknownBegNotBeforeDateIvl: {value: false},
    posInfEndNotBeforeDateIvl: {value: false},
    should(@unknownEndMayBeBeforeDateIvl.exec(@ctx)).be.null
    unknownEndNotBeforeDateIvl: {value: false},
//  should correctly handle imprecision
    beforeImpreciseDateIvl: {value: true},
    notBeforeImpreciseDateIvl: {value: false},
    should(@mayBeBeforeImpreciseDateIvl.exec(@ctx)).be.null
    impreciseBeforeDateIvl: {value: true},
    impreciseNotBeforeDateIvl: {value: false},
    should(@impreciseMayBeBeforeDateIvl.exec(@ctx)).be.null

// Meets
//  should accept intervals meeting after it
    meetsBeforeIntIvl: {value: true},
    meetsBeforeRealIvl: {value: true},
    meetsBeforeDateIvl: {value: true},
//  should accept intervals meeting before it
    meetsAfterIntIvl: {value: true},
    meetsAfterRealIvl: {value: true},
    meetsAfterDateIvl: {value: true},
//  should reject intervals not meeting it
    notMeetsIntIvl: {value: false},
    notMeetsRealIvl: {value: false},
    notMeetsDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegMeetsBeforeIntIvl: {value: true},
    negInfBegNotMeetsIntIvl: {value: false},
    intIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeIntIvl: {value: true},
    should(@unknownBegMayMeetAfterIntIvl.exec(@ctx)).be.null
    unknownBegNotMeetsIntIvl: {value: false},
    should(@intIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    posInfEndMeetsAfterIntIvl: {value: true},
    posInfEndNotMeetsIntIvl: {value: false},
    intIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterIntIvl: {value: true},
    should(@unknownEndMayMeetBeforeIntIvl.exec(@ctx)).be.null
    unknownEndNotMeetsIntIvl: {value: false},
    should(@intIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null
//  should correctly handle null endpoints (date)
    negInfBegMeetsBeforeDateIvl: {value: true},
    negInfBegNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeDateIvl: {value: true},
    should(@unknownBegMayMeetAfterDateIvl.exec(@ctx)).be.null
    unknownBegNotMeetsDateIvl: {value: false},
    should(@dateIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    posInfEndMeetsAfterDateIvl: {value: true},
    posInfEndNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterDateIvl: {value: true},
    should(@unknownEndMayMeetBeforeDateIvl.exec(@ctx)).be.null
    unknownEndNotMeetsDateIvl: {value: false},
    should(@dateIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null
//  should correctly handle imprecision
    should(@mayMeetAfterImpreciseDateIvl.exec(@ctx)).be.null
    should(@mayMeetBeforeImpreciseDateIvl.exec(@ctx)).be.null
    notMeetsImpreciseDateIvl: {value: false},
    should(@impreciseMayMeetAfterDateIvl.exec(@ctx)).be.null
    should(@impreciseMayMeetBeforeDateIvl.exec(@ctx)).be.null
    impreciseNotMeetsDateIvl: {value: false},

// MeetsAfter
//  should accept intervals meeting before it
    meetsAfterIntIvl: {value: true},
    meetsAfterRealIvl: {value: true},
    meetsAfterDateIvl: {value: true},
//  should reject intervals meeting after it
    meetsBeforeIntIvl: {value: false},
    meetsBeforeRealIvl: {value: false},
    meetsBeforeDateIvl: {value: false},
//  should reject intervals not meeting it
    notMeetsIntIvl: {value: false},
    notMeetsRealIvl: {value: false},
    notMeetsDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegMeetsBeforeIntIvl: {value: false},
    negInfBegNotMeetsIntIvl: {value: false},
    intIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeIntIvl: {value: false},
    should(@unknownBegMayMeetAfterIntIvl.exec(@ctx)).be.null
    unknownBegNotMeetsIntIvl: {value: false},
    intIvlMayMeetBeforeUnknownBeg: {value: false},
    posInfEndMeetsAfterIntIvl: {value: true},
    posInfEndNotMeetsIntIvl: {value: false},
    intIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterIntIvl: {value: true},
    unknownEndMayMeetBeforeIntIvl: {value: false},
    unknownEndNotMeetsIntIvl: {value: false},
    should(@intIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null
//  should correctly handle null endpoints (date)
    negInfBegMeetsBeforeDateIvl: {value: false},
    negInfBegNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeDateIvl: {value: false},
    should(@unknownBegMayMeetAfterDateIvl.exec(@ctx)).be.null
    unknownBegNotMeetsDateIvl: {value: false},
    dateIvlMayMeetBeforeUnknownBeg: {value: false},
    posInfEndMeetsAfterDateIvl: {value: true},
    posInfEndNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterDateIvl: {value: true},
    unknownEndMayMeetBeforeDateIvl: {value: false},
    unknownEndNotMeetsDateIvl: {value: false},
    should(@dateIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null
//  should correctly handle imprecision
    should(@mayMeetAfterImpreciseDateIvl.exec(@ctx)).be.null
    mayMeetBeforeImpreciseDateIvl: {value: false},
    notMeetsImpreciseDateIvl: {value: false},
    should(@impreciseMayMeetAfterDateIvl.exec(@ctx)).be.null
    impreciseMayMeetBeforeDateIvl: {value: false},
    impreciseNotMeetsDateIvl: {value: false},

// MeetsBefore
//  should accept intervals meeting after it
    meetsBeforeIntIvl: {value: true},
    meetsBeforeRealIvl: {value: true},
    meetsBeforeDateIvl: {value: true},
//  should reject intervals meeting before it
    meetsAfterIntIvl: {value: false},
    meetsAfterRealIvl: {value: false},
    meetsAfterDateIvl: {value: false},
//  should reject intervals not meeting it
    notMeetsIntIvl: {value: false},
    notMeetsRealIvl: {value: false},
    notMeetsDateIvl: {value: false},
//  should correctly handle null endpoints (int)
    negInfBegMeetsBeforeIntIvl: {value: true},
    negInfBegNotMeetsIntIvl: {value: false},
    intIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeIntIvl: {value: true},
    unknownBegMayMeetAfterIntIvl: {value: false},
    unknownBegNotMeetsIntIvl: {value: false},
    should(@intIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    posInfEndMeetsAfterIntIvl: {value: false},
    posInfEndNotMeetsIntIvl: {value: false},
    intIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterIntIvl: {value: false},
    should(@unknownEndMayMeetBeforeIntIvl.exec(@ctx)).be.null
    unknownEndNotMeetsIntIvl: {value: false},
    intIvlMayMeetAfterUnknownEnd: {value: false},
//  should correctly handle null endpoints (date)
    negInfBegMeetsBeforeDateIvl: {value: true},
    negInfBegNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsNegInfBeg: {value: false},
    unknownBegMeetsBeforeDateIvl: {value: true},
    unknownBegMayMeetAfterDateIvl: {value: false},
    unknownBegNotMeetsDateIvl: {value: false},
    should(@dateIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    posInfEndMeetsAfterDateIvl: {value: false},
    posInfEndNotMeetsDateIvl: {value: false},
    dateIvlNotMeetsPosInfEnd: {value: false},
    unknownEndMeetsAfterDateIvl: {value: false},
    should(@unknownEndMayMeetBeforeDateIvl.exec(@ctx)).be.null
    unknownEndNotMeetsDateIvl: {value: false},
    dateIvlMayMeetAfterUnknownEnd: {value: false},
//  should correctly handle imprecision
    mayMeetAfterImpreciseDateIvl: {value: false},
    should(@mayMeetBeforeImpreciseDateIvl.exec(@ctx)).be.null
    notMeetsImpreciseDateIvl: {value: false},
    impreciseMayMeetAfterDateIvl: {value: false},
    should(@impreciseMayMeetBeforeDateIvl.exec(@ctx)).be.null
    impreciseNotMeetsDateIvl: {value: false},

// Overlaps
//  should accept overlaps (integer)
    overlapsBeforeIntIvl: {value: true},
    overlapsAfterIntIvl: {value: true},
    overlapsBoundaryIntIvl: {value: true},
//  should accept overlaps (real)
    overlapsBeforeRealIvl: {value: true},
    overlapsAfterRealIvl: {value: true},
    overlapsBoundaryRealIvl: {value: true},
//  should reject non-overlaps (integer)
    noOverlapsIntIvl: {value: false},
//  should reject non-overlaps (real)
    noOverlapsRealIvl: {value: false},

// OverlapsDateTime
//  should accept overlaps
    overlapsBefore: {value: true},
    overlapsAfter: {value: true},
    overlapsContained: {value: true},
    overlapsContains: {value: true},
//  should accept imprecise overlaps
    impreciseOverlap: {value: true},
//  should reject non-overlaps
    noOverlap: {value: false},
//  should reject imprecise non-overlaps
    noImpreciseOverlap: {value: false},
//  should return null for imprecise overlaps that are unknown
    should(@unknownOverlap.exec(@ctx)).be.null

// OverlapsAfter
//  should accept overlaps that are after (integer)
    overlapsAfterIntIvl: {value: true},
    overlapsBoundaryIntIvl: {value: true},
//  should accept overlaps that are after (real)
    overlapsAfterRealIvl: {value: true},
    overlapsBoundaryRealIvl: {value: true},
//  should reject overlaps that are before (integer)
    overlapsBeforeIntIvl: {value: false},
//  should reject overlaps that are before (real)
    overlapsBeforeRealIvl: {value: false},
//  should reject non-overlaps (integer)
    noOverlapsIntIvl: {value: false},
//  should reject non-overlaps (real)
    noOverlapsRealIvl: {value: false},

// OverlapsAfterDateTime
//  should accept overlaps that are after
    overlapsAfter: {value: true},
    overlapsContains: {value: true},
//  should accept imprecise overlaps that are after
    impreciseOverlapAfter: {value: true},
//  should reject overlaps that are not before
    overlapsBefore: {value: false},
    overlapsContained: {value: false},
//  should reject imprecise overlaps that are not before
    impreciseOverlapBefore: {value: false},
//  should reject non-overlaps
    noOverlap: {value: false},
//  should reject imprecise non-overlaps
    noImpreciseOverlap: {value: false},
//  should return null for imprecise overlaps that are unknown
    should(@unknownOverlap.exec(@ctx)).be.null

// OverlapsBefore
//  should accept overlaps that are before (integer)
    overlapsBeforeIntIvl: {value: true},
    overlapsBoundaryIntIvl: {value: true},
//  should accept overlaps that are before (real)
    overlapsBeforeRealIvl: {value: true},
    overlapsBoundaryRealIvl: {value: true},
//  should reject overlaps that are after (integer)
    overlapsAfterIntIvl: {value: false},
//  should reject overlaps that are after (real)
    overlapsAfterRealIvl: {value: false},
//  should reject non-overlaps (integer)
    noOverlapsIntIvl: {value: false},
//  should reject non-overlaps (real)
    noOverlapsRealIvl: {value: false},

// OverlapsBeforeDateTime
//  should accept overlaps that are before
    overlapsBefore: {value: true},
    overlapsContains: {value: true},
//  should accept imprecise overlaps that are before
    impreciseOverlapBefore: {value: true},
//  should reject overlaps that are not before
    overlapsAfter: {value: false},
    overlapsContained: {value: false},
//  should reject imprecise overlaps that are not before
    impreciseOverlapAfter: {value: false},
//  should reject non-overlaps
    noOverlap: {value: false},
//  should reject imprecise non-overlaps
    noImpreciseOverlap: {value: false},
//  should return null for imprecise overlaps that are unknown
    should(@unknownOverlap.exec(@ctx)).be.null

// Width
//  should calculate the width of integer intervals
    intWidth: {value: 7},
    intOpenWidth: {value: 5},
//  should calculate the width of real intervals
    realWidth: {value: 3.33},
    realOpenWidth: {value: 3.32999998},
//  should calculate the width of infinite intervals
    intWidthThreeToMax: {value: Math.pow(2,31)-4},
    intWidthMinToThree: {value: Math.pow(2,31)+3},
//  should calculate the width of infinite intervals
    should(@intWidthThreeToUnknown.exec(@ctx)).be.null
    should(@intWidthUnknownToThree.exec(@ctx)).be.null

// Start
//  should execute as the start of the interval
    foo: {value: new DateTime(2012, 1, 1)},

// End
//  should execute as the end of the interval
    foo: {value: new DateTime(2013, 1, 1)},

// IntegerIntervalUnion
//  should properly calculate open and closed unions
    x = @intFullInterval.exec(@ctx)
    y = @intClosedUnionClosed.exec(@ctx)
    y.equals(x).should.be.true

    y = @intClosedUnionOpen.exec(@ctx)
    y.contains(0).should.be.true
    y.contains(10).should.be.false

    y = @intOpenUnionOpen.exec(@ctx)
    y.contains(0).should.be.false
    y.contains(10).should.be.false

    y = @intOpenUnionClosed.exec(@ctx)
    y.contains(0).should.be.false
    y.contains(10).should.be.true
//  should properly calculate sameAs unions
    x = @intFullInterval.exec(@ctx)
    y = @intSameAsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate before/after unions
    should(@intBeforeUnion.exec(@ctx)).be.null
//  should properly calculate meets unions
    x = @intFullInterval.exec(@ctx)
    y = @intMeetsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping unions
    x = @intFullInterval.exec(@ctx)
    y = @intOverlapsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by unions
    x = @intFullInterval.exec(@ctx)
    y = @intBeginsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate includes/included by unions
    x = @intFullInterval.exec(@ctx)
    y = @intDuringUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate ends/ended by unions
    x = @intFullInterval.exec(@ctx)
    y = @intEndsUnion.exec(@ctx)
    y.equals(x).should.be.true

# TODO
#//  should properly handle imprecision

// DateTimeIntervalUnion
//  should properly calculate open and closed unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeClosedUnionClosed.exec(@ctx)
    y.equals(x).should.be.true

    a = new DateTime(2012, 1, 1, 0, 0, 0, 0)
    b = new DateTime(2013, 1, 1, 0, 0, 0, 0)

    y = @dateTimeClosedUnionOpen.exec(@ctx)
    y.contains(a).should.be.true
    y.contains(b).should.be.false

    y = @dateTimeOpenUnionOpen.exec(@ctx)
    y.contains(a).should.be.false
    y.contains(b).should.be.false

    y = @dateTimeOpenUnionClosed.exec(@ctx)
    y.contains(a).should.be.false
    y.contains(b).should.be.true
//  should properly calculate sameAs unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeSameAsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate before/after unions
    should(@dateTimeBeforeUnion.exec(@ctx)).be.null
//  should properly calculate meets unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeMeetsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeOverlapsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeBeginsUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate includes/included by unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeDuringUnion.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate ends/ended by unions
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeEndsUnion.exec(@ctx)
    y.equals(x).should.be.true

# TODO
#//  should properly handle imprecision

// IntegerIntervalExcept
//  should properly calculate sameAs except
    should(@intSameAsExcept.exec(@ctx)).be.null
//  should properly calculate before/after except
    intBeforeExcept: {value: new Interval(0,4)},
//  should properly calculate meets except
    x = @intHalfInterval.exec(@ctx)
    y = @intMeetsExcept.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping except
    x = @intHalfInterval.exec(@ctx)
    y = @intOverlapsExcept.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by except
    should(@intBeginsExcept.exec(@ctx)).be.null
//  should properly calculate includes/included by except
    should(@intDuringExcept.exec(@ctx)).be.null
//  should properly calculate ends/ended by except
    should(@intEndsExcept.exec(@ctx)).be.null

# TODO
#//  should properly handle imprecision

// DateTimeIntervalExcept
//  should properly calculate sameAs except
    should(@dateTimeSameAsExcept.exec(@ctx)).be.null
//  should properly calculate before/after except
    dateTimeBeforeExcept: {value: new Interval(new DateTime(2012, 1, 1, 0, 0, 0, 0), new DateTime(2012, 4, 1, 0, 0, 0, 0))},
//  should properly calculate meets except
    x = @dateTimeHalfInterval.exec(@ctx)
    y = @dateTimeMeetsExcept.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping except
    x = @dateTimeHalfInterval.exec(@ctx)
    y = @dateTimeOverlapsExcept.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by except
    should(@dateTimeBeginsExcept.exec(@ctx)).be.null
//  should properly calculate includes/included by except
    should(@dateTimeDuringExcept.exec(@ctx)).be.null
//  should properly calculate ends/ended by except
    should(@dateTimeEndsExcept.exec(@ctx)).be.null

# TODO
#//  should properly handle imprecision

// IntegerIntervalIntersect
//  should properly calculate sameAs intersect
    x = @intSameAsIntersect.exec(@ctx)
    y = @intFullInterval.exec(@ctx)
    x.equals(y).should.be.true
//  should properly calculate before/after intersect
    should(@intBeforeIntersect.exec(@ctx)).be.null
//  should properly calculate meets intersect
    x = @intMeetsInterval.exec(@ctx)
    y = @intMeetsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping intersect
    x = @intOverlapsInterval.exec(@ctx)
    y = @intOverlapsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by intersect
    x = @intBeginsInterval.exec(@ctx)
    y = @intBeginsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate includes/included by intersect
    x = @intDuringInterval.exec(@ctx)
    y = @intDuringIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate ends/ended by intersect
    x = @intEndsInterval.exec(@ctx)
    y = @intEndsIntersect.exec(@ctx)
    y.equals(x).should.be.true

// DateTimeIntervalIntersect
//  should properly calculate sameAs intersect
    x = @dateTimeSameAsIntersect.exec(@ctx)
    y = @dateTimeFullInterval.exec(@ctx)
    x.equals(y).should.be.true
//  should properly calculate before/after intersect
    should(@dateTimeBeforeIntersect.exec(@ctx)).be.null
//  should properly calculate meets intersect
    x = @dateTimeMeetsInterval.exec(@ctx)
    y = @dateTimeMeetsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate left/right overlapping intersect
    x = @dateTimeOverlapsInterval.exec(@ctx)
    y = @dateTimeOverlapsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate begins/begun by intersect
    x = @dateTimeBeginsInterval.exec(@ctx)
    y = @dateTimeBeginsIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate includes/included by intersect
    x = @dateTimeDuringInterval.exec(@ctx)
    y = @dateTimeDuringIntersect.exec(@ctx)
    y.equals(x).should.be.true
//  should properly calculate ends/ended by intersect
    x = @dateTimeEndsInterval.exec(@ctx)
    y = @dateTimeEndsIntersect.exec(@ctx)
    y.equals(x).should.be.true
{Repository} = require './repository'

{ p1, p2 } = require './patients'

// In Age Demographic
  , [ p1, p2 ]
    @results = @executor.withLibrary(@lib).exec_patient_context(@patientSource)
//  should have correct patient results
    @results.patientResults['1'].InDemographic.should.equal false
    @results.patientResults['2'].InDemographic.should.equal true
//  should have empty population results
    @results.populationResults.should.be.empty


// Using CommonLib
//  , [ p1, p2 ], {}, {}, new Repository(data)
//  should have using models defined
    @lib.usings.should.not.be.empty
    @lib.usings.length.should.equal 1
    @lib.usings[0].name.should.equal "QUICK"
//  Should have included a library
    @lib.includes.should.not.be.empty
//  should be able to execute expression from included library
    @results = @executor.withLibrary(@lib).exec_patient_context(@patientSource)
    @results.patientResults['1'].ID.should.equal false
    @results.patientResults['2'].ID.should.equal true
    @results.patientResults['2'].FuncTest.should.equal 7

{ArrayIndexOutOfBoundsException} = require '../../../lib/elm/overloaded'

// List
//  should execute to an array (ints)
    intList: {value: [9, 7, 8]},
//  should execute to an array (strings)
    stringList: {value: ['a', 'bee', 'see']},
//  should execute to an array (mixed)
    mixedList: {value: [1, 'two', 3]},
//  should execute to an empty array
    emptyList: {value: []},

// Exists
//  should return false for empty list
    emptyList: {value: false},
//  should return true for full list
    fullList: {value: true},

// Equal
//  should identify equal lists of integers
    equalIntList: {value: true},
//  should identify unequal lists of integers
    unequalIntList: {value: false},
//  should identify re-ordered lists of integers as unequal
    reverseIntList: {value: false},
//  should identify equal lists of strings
    equalStringList: {value: true},
//  should identify unequal lists of strings
    unequalStringList: {value: false},
//  should identify equal lists of tuples
    equalTupleList: {value: true},
//  should identify unequal lists of tuples
    unequalTupleList: {value: false},

// NotEqual
//  should identify equal lists of integers
    equalIntList: {value: false},
//  should identify unequal lists of integers
    unequalIntList: {value: true},
//  should identify re-ordered lists of integers as unequal
    reverseIntList: {value: true},
//  should identify equal lists of strings
    equalStringList: {value: false},
//  should identify unequal lists of strings
    unequalStringList: {value: true},
//  should identify equal lists of tuples
    equalTupleList: {value: false},
//  should identify unequal lists of tuples
    unequalTupleList: {value: true},

// Union
//  should union two lists to a single list
    oneToTen: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]},
//  should maintain duplicate elements (according to CQL spec)
    oneToFiveOverlapped: {value: [1, 2, 3, 4, 3, 4, 5]},
//  should not fill in values in a disjoint union
    disjoint: {value: [1, 2, 4, 5]},
//  should return one list for multiple nested unions
    nestedToFifteen: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},
//  should return null if either arg is null
    should(@unionNull.exec(@ctx)).be.null
    should(@nullUnion.exec(@ctx)).be.null

// Except
//  should remove items in second list
    exceptThreeFour: {value: [1, 2, 5]},
//  should not be commutative
    threeFourExcept: {value: []},
//  should remove items in second list regardless of order
    exceptFiveThree: {value: [1, 2, 4]},
//  should be a no-op when lists have no common items
    exceptNoOp: {value: [1, 2, 3, 4, 5]},
//  should remove all items when lists are the same
    exceptEverything: {value: []},
//  should be a no-op when second list is empty
    somethingExceptNothing: {value: [1, 2, 3, 4, 5]},
//  should be a no-op when first list is already empty
    nothingExceptSomething: {value: []},
//  should except lists of tuples
    exceptTuples: {value: [{a: 1}, {a: 3}]},
//  should return null if either arg is null
    should(@exceptNull.exec(@ctx)).be.null
    should(@nullExcept.exec(@ctx)).be.null

// Intersect
//  should intersect two disjoint lists  to an empty list
    noIntersection: {value: []},
//  should intersect two lists with a single common element
    intersectOnFive: {value: [5]},
//  should intersect two lists with several common elements
    intersectOnEvens: {value: [2, 4, 6, 8, 10]},
//  should intersect two identical lists to the same list
    intersectOnAll: {value: [1, 2, 3, 4, 5]},
//  should intersect multiple lists to only those elements common across all
    nestedIntersects: {value: [4, 5]},
//  should intersect lists of tuples
    intersectTuples: {value: [{a:1, b:'c'}, {a:2, b:'c'}]},
//  should return null if either arg is null
    should(@intersectNull.exec(@ctx)).be.null
    should(@nullIntersect.exec(@ctx)).be.null

// IndexOf
//  should return the correct 1-based index when an item is in the list
    indexOfSecond: {value: 2},
//  should work with complex types like tuples
    indexOfThirdTuple: {value: 3},
//  should return the first index when there are multiple matches
    multipleMatches: {value: 4},
//  should return 0 when the item is not in the list
    itemNotFound: {value: 0},
//  should return null if either arg is null
    should(@nullList.exec(@ctx)).be.null
    should(@nullItem.exec(@ctx)).be.null

// Indexer
//  should return the correct item based on the 1-based index
    secondItem: {value: 'b'},
//  should throw ArrayIndexOutOfBoundsException when accessing index 0
      zeroIndex: {throws: true},

//  should throw ArrayIndexOutOfBoundsException when accessing out of bounds index
      outOfBounds: {throws: true},

//  should return null if either arg is null
    should(@nullList.exec(@ctx)).be.null
    should(@nullIndexer.exec(@ctx)).be.null

// In
//  should execute to true when item is in list
    isIn: {value: true},
//  should execute to false when item is not in list
    isNotIn: {value: false},
//  should execute to true when tuple is in list
    tupleIsIn: {value: true},
//  should execute to false when tuple is not in list
    tupleIsNotIn: {value: false},
//  should return null if either arg is null
    should(@nullIn.exec(@ctx)).be.null
    should(@inNull.exec(@ctx)).be.null

// Contains
//  should execute to true when item is in list
    isIn: {value: true},
//  should execute to false when item is not in list
    isNotIn: {value: false},
//  should execute to true when tuple is in list
    tupleIsIn: {value: true},
//  should execute to false when tuple is not in list
    tupleIsNotIn: {value: false},
//  should return null if either arg is null
    should(@nullIn.exec(@ctx)).be.null
    should(@inNull.exec(@ctx)).be.null

// Includes
//  should execute to true when sublist is in list
    isIncluded: {value: true},
//  should execute to true when sublist is in list in different order
    isIncludedReversed: {value: true},
//  should execute to true when lists are the same
    isSame: {value: true},
//  should execute to false when sublist is not in list
    isNotIncluded: {value: false},
//  should execute to true when tuple sublist is in list
    tuplesIncluded: {value: true},
//  should execute to false when tuple sublist is not in list
    tuplesNotIncluded: {value: false},
//  should return null if either arg is null
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

// IncludedIn
//  should execute to true when sublist is in list
    isIncluded: {value: true},
//  should execute to true when sublist is in list in different order
    isIncludedReversed: {value: true},
//  should execute to true when lists are the same
    isSame: {value: true},
//  should execute to false when sublist is not in list
    isNotIncluded: {value: false},
//  should execute to true when tuple sublist is in list
    tuplesIncluded: {value: true},
//  should execute to false when tuple sublist is not in list
    tuplesNotIncluded: {value: false},
//  should return null if either arg is null
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

// ProperIncludes
//  should execute to true when sublist is in list
    isIncluded: {value: true},
//  should execute to true when sublist is in list in different order
    isIncludedReversed: {value: true},
//  should execute to false when lists are the same
    isSame: {value: false},
//  should execute to false when sublist is not in list
    isNotIncluded: {value: false},
//  should execute to true when tuple sublist is in list
    tuplesIncluded: {value: true},
//  should execute to false when tuple sublist is not in list
    tuplesNotIncluded: {value: false},

  # TODO: Support for ProperContains
  it.skip 'should return null if either arg is null', ->
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

// ProperIncludedIn
//  should execute to true when sublist is in list
    isIncluded: {value: true},
//  should execute to true when sublist is in list in different order
    isIncludedReversed: {value: true},
//  should execute to false when lists are the same
    isSame: {value: false},
//  should execute to false when sublist is not in list
    isNotIncluded: {value: false},
//  should execute to true when tuple sublist is in list
    tuplesIncluded: {value: true},
//  should execute to false when tuple sublist is not in list
    tuplesNotIncluded: {value: false},
//  should return null if either arg is null
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

// Expand
//  should expand a list of lists
    listOfLists: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1]},
//  should return null for a null list
    should(@nullValue.exec(@ctx)).be.null

// Distinct
//  should remove duplicates
    lotsOfDups: {value: [1, 2, 3, 4, 5]},
//  should do nothing to an already distinct array
    noDups: {value: [2, 4, 6, 8, 10]},

// First
//  should get first of a list of numbers
    numbers: {value: 1},
//  should get first of a list of letters
    letters: {value: 'a'},
//  should get first of a list of lists
    lists: {value: ['a','b','c']},
//  should get first of a list of tuples
    tuples: {value: { a: 1, b: 2, c: 3 }},
//  should get first of a list of unordered numbers
    unordered: {value: 3},
//  should return null for an empty list
    should(@empty.exec(@ctx)).be.null
//  should return null for an empty list
    should(@nullValue.exec(@ctx)).be.null

// Last
//  should get last of a list of numbers
    numbers: {value: 4},
//  should get last of a list of letters
    letters: {value: 'c'},
//  should get last of a list of lists
    lists: {value: ['d','e','f']},
//  should get last of a list of tuples
    tuples: {value: { a: 24, b: 25, c: 26 }},
//  should get last of a list of unordered numbers
    unordered: {value: 2},
//  should return null for an empty list
    should(@empty.exec(@ctx)).be.null
//  should return null for an empty list
    should(@nullValue.exec(@ctx)).be.null

// Length
//  should get length of a list of numbers
    numbers: {value: 5},
//  should get length of a list of lists
    lists: {value: 4},
//  should get length of a list of tuples
    tuples: {value: 2},
//  should get length of an empty list
    empty: {value: 0},
//  should return null for an empty list
    should(@nullValue.exec(@ctx)).be.null

// Literal
//  should convert true to boolean true
    @boolTrue.value.should.be.true
//  should execute true as true
    boolTrue: {value: true},
//  should convert false to boolean false
    @boolFalse.value.should.be.false
//  should execute false as false
    boolFalse: {value: false},
//  should convert 1 to int 1
    @intOne.value.should.equal 1
//  should execute 1 as 1
    intOne: {value: 1},
//  should convert .1 to decimal .1
    @decimalTenth.value.should.equal 0.1
//  should execute .1 as .1
    decimalTenth: {value: 0.1},
//  should convert \'true\' to string \'true\'
    @stringTrue.value.should.equal 'true'
//  should execute \'true\' as \'true\'
    stringTrue: {value: 'true'},

// And
//  should execute true and...
    tT: {value: true},
    tF: {value: false},
    should(@tN.exec(@ctx)).be.null
//  should execute false and...
    fF: {value: false},
    fT: {value: false},
    fN: {value: false},
//  should execute null and...
    should(@nN.exec(@ctx)).be.null
    should(@nT.exec(@ctx)).be.null
    nF: {value: false},

// Or
//  should execute true or...
    tT: {value: true},
    tF: {value: true},
    tN: {value: true},
//  should execute false or...
    fF: {value: false},
    fT: {value: true},
    should(@fN.exec(@ctx)).be.null
//  should execute null or...
    should(@nN.exec(@ctx)).be.null
    nT: {value: true},
    should(@nF.exec(@ctx)).be.null

// Not
//  should execute not true as false
    notTrue: {value: false},
//  should execute not false as true
    notFalse: {value: true},
//  should execute not null as null
    should(@notNull.exec(@ctx)).be.null

// XOr
//  should execute true xor...
    tT: {value: false},
    tF: {value: true},
    should(@tN.exec(@ctx)).be.null
//  should execute false xor...
    fF: {value: false},
    fT: {value: true},
    should(@fN.exec(@ctx)).be.null
//  should execute null xor...
    should(@nN.exec(@ctx)).be.null
    should(@nT.exec(@ctx)).be.null
    should(@nF.exec(@ctx)).be.null

// Nil
//  should execute as null
    should(@nil.exec(@ctx)).be.null

// IsNull
//  should detect that null is null
    nullIsNull: {value: true},
//  should detect that null variable is null
    nullVarIsNull: {value: true},
//  should detect that string is not null
    stringIsNull: {value: false},
//  should detect that non-null variable is not null
    nonNullVarIsNull: {value: false},

// (skipped) Coalesce
//  should return first non-null when leading args are null
    nullNullHelloNullWorld: {value: 'Hello'},
//  should return first arg when it is non-null
    fooNullNullBar: {value: 'Foo'},
//  should return null when they are all null
    should(@allNull.exec(@ctx)).be.null

// ParameterDef
    @param = @lib.parameters.MeasureYear
//  should have a name
    @param.name.should.equal 'MeasureYear'
//  should execute to default value
    param: {value: 2012},
//  should execute to provided value
    @param.exec(@ctx.withParameters { MeasureYear: 2013 }).should.equal 2013
//  should work with typed int parameters
    intParam = @lib.parameters.IntParameter
    intParam.exec(@ctx.withParameters { IntParameter: 17 }).should.equal 17
//  should work with typed list parameters
    listParam = @lib.parameters.ListParameter
    listParam.exec(@ctx.withParameters { ListParameter: {'a', 'b', 'c'} }).should.eql {'a', 'b', 'c'}
//  should work with typed tuple parameters
    tupleParam = @lib.parameters.TupleParameter
    v = { a : 1, b : 'bee', c : true, d : [10, 9, 8], e : { f : 'eff', g : false}}
    tupleParam.exec(@ctx.withParameters { TupleParameter: v }).should.eql v

// ParameterRef
//  should have a name
    @foo.name.should.equal 'FooP'
//  should execute to default value
    foo: {value: 'Bar'},
//  should execute to provided value
    @foo.exec(@ctx.withParameters { FooP: 'Bah' }).should.equal 'Bah'
vsets = require './valuesets'
{ p1 } = require './patients'

// DateRangeOptimizedQuery
  , [ p1 ], vsets
//  should find encounters performed during the MP
    e = @encountersDuringMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'
//  should find ambulatory encounters performed during the MP
    e = @ambulatoryEncountersDuringMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'
//  should find ambulatory encounter performances included in the MP
    e = @ambulatoryEncountersIncludedInMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

// (skipped) IncludesQuery
  , [ p1 ], vsets
//  should find ambulatory encounter performances included in the MP
    e = @mPIncludedAmbulatoryEncounters.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

// MultiSourceQuery
  , [ p1 ], vsets
//  should find all Encounters performed and Conditions
    e = @msQuery.exec(@ctx)
    e.should.have.length(6)

  it.skip 'should find encounters performed during the MP and All conditions', ->
    e = @msQueryWhere.exec(@ctx)
    e.should.have.length(2)

  it.skip 'should be able to filter items in the where clause', ->
    e = @msQueryWhere2.exec(@ctx)
    e.should.have.length(1)

// (skipped) QueryRelationship
  , [ p1 ]
//  should be able to filter items with a with clause
    e = @withQuery.exec(@ctx)
    e.should.have.length(3)

//  with clause should filter out items not available
    e = @withQuery2.exec(@ctx)
    e.should.have.length(0)
//  should be able to filter items with a without clause
    e = @withOutQuery.exec(@ctx)
    e.should.have.length(3)

//  without clause should be able to filter items with a without clause
    e = @withOutQuery2.exec(@ctx)
    e.should.have.length(0)

// QueryDefine
  , [ p1 ]
//  should be able to define a variable in a query and use it
    e = @query.exec(@ctx)
    e.should.have.length(3)
    e[0]["a"].should.equal  e[0]["E"]
    e[1]["a"].should.equal  e[1]["E"]
    e[2]["a"].should.equal  e[2]["E"]

// Tuple
  , [ p1 ]
//  should be able to return tuple from a query
    e = @query.exec(@ctx)
    e.should.have.length(3)

// Sorting
  , [ p1 ]

  it 'should be able to sort by a tuple field asc' , ->
    e = @tupleAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[2].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[2].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnTupleAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].E.id().should.equal "http://cqframework.org/3/1"
    e[1].E.id().should.equal  "http://cqframework.org/3/3"
    e[2].E.id().should.equal  "http://cqframework.org/3/5"
//  should be able to sort by a tuple field desc
    e = @tupleDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[0].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[0].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnTupleDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].E.id().should.equal "http://cqframework.org/3/1"
    e[1].E.id().should.equal  "http://cqframework.org/3/3"
    e[0].E.id().should.equal  "http://cqframework.org/3/5"

  it 'should be able to sort by number asc' , ->
    e = @numberAsc.exec(@ctx)
    e.should.eql [0, 3, 5, 6, 7, 8, 9]

  it 'should be able to sort by number desc' , ->
    e = @numberDesc.exec(@ctx)
    e.should.eql [9, 8, 7, 6, 5, 3, 0]

  it 'should be able to sort by string asc' , ->
    stringAsc: {value: ['change', 'dont', 'jenny', 'number', 'your']},
    stringReturnAsc: {value: ['change', 'dont', 'jenny', 'number', 'your']},

  it 'should be able to sort by string desc' , ->
    stringDesc: {value: ['your', 'number', 'jenny', 'dont', 'change']},
    stringReturnDesc: {value: ['your', 'number', 'jenny', 'dont', 'change']},

// Distinct
  it 'should return distinct by default' , ->
    defaultNumbers: {value: [1, 2, 3, 4]},
    defaultStrings: {value: ['foo', 'bar', 'baz']},
    defaultTuples: {value: [{a: 1, b:2}, {a: 2, b:3}]},

  it 'should eliminate duplicates when returning distinct' , ->
    distinctNumbers: {value: [1, 2, 3, 4]},
    distinctStrings: {value: ['foo', 'bar', 'baz']},
    distinctTuples: {value: [{a: 1, b:2}, {a: 2, b:3}]},

  it 'should not eliminate duplicates when returning all' , ->
    allNumbers: {value: [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 2, 1]},
    allStrings: {value: ['foo', 'bar', 'baz', 'bar']},
    allTuples: {value: [{a: 1, b:2}, {a: 2, b:3}, {a: 1, b:2}]},

// ExpressionDef
    @def = @lib.expressions.Foo
//  should have a name
    @def.name.should.equal 'Foo'
//  should have the correct context
    @def.context.should.equal 'Patient'
//  should execute to its value
    def: {value: 'Bar'},

// ExpressionRef
//  should have a name
    @foo.name.should.equal 'Life'
//  should execute to expression value
    foo: {value: 42},

// FunctionDefinitions
  it 'should be able to define and use a simple function' , ->
    e = @testValue.exec(@ctx)
    e.should.equal 3
str = require '../../../lib/elm/string'
{ArrayIndexOutOfBoundsException} = require '../../../lib/elm/overloaded'

// Concat
  it.skip 'should be a Concat', ->
    @helloWorld.should.be.an.instanceOf(str.Concat)
    @helloWorldVariables.should.be.an.instanceOf(str.Concat)
//  should concat two strings
    helloWorld: {value: 'HelloWorld'},
//  should concat multiple strings
    sentence: {value: 'The quick brown fox jumps over the lazy dog.'},
//  should return null when an arg is null
    should(@concatNull.exec(@ctx)).be.null
//  should concat variables
    helloWorldVariables: {value: 'HelloWorld'},

// Combine
  it.skip 'should be a Combine', ->
    @separator.should.be.an.instanceOf(str.Combine)
//  should combine strings with no separator
    noSeparator: {value: 'abcdefghijkl'},
//  should combine strings with a separator
    separator: {value: 'abc;def;ghi;jkl'},
//  should return null when the list is null
    should(@combineNull.exec(@ctx)).be.null
//  should return null when an item in the list is null
    should(@combineNullItem.exec(@ctx)).be.null

// Split
  it.skip 'should be a Split', ->
    @commaSeparated.should.be.an.instanceOf(str.Split)
//  should split strings on comma
    commaSeparated: {value: ['a','b','c','','1','2','3']},
//  should return single-item array when separator is not used
    separatorNotUsed: {value: ['a,b,c,,1,2,3']},
//  should return null when separating null
    should(@separateNull.exec(@ctx)).be.null

  # TODO: Verify this assumption
//  should return null when the separator is null
    should(@separateUsingNull.exec(@ctx)).be.null

// Length
  it.skip 'should be a Length', ->
    @elevenLetters.should.be.an.instanceOf(str.Length)
//  should count letters in string
    elevenLetters: {value: 11},
//  should return null when string is null
    should(@nullString.exec(@ctx)).be.null

// Upper
  it.skip 'should be an Upper', ->
    @upperC.should.be.an.instanceOf(str.Upper)
//  should convert lower to upper
    lowerC: {value: 'ABCDEFG123'},
//  should leave upper as upper
    upperC: {value: 'ABCDEFG123'},
//  should convert camel to upper
    camelC: {value: 'ABCDEFG123'},
//  should return null when uppering null
    should(@nullString.exec(@ctx)).be.null

// Lower
  it.skip 'should be a Lower', ->
    @lowerC.should.be.an.instanceOf(str.Lower)
//  should leave lower as lower
    lowerC: {value: 'abcdefg123'},
//  should convert upper to lower
    upperC: {value: 'abcdefg123'},
//  should convert camel to lower
    camelC: {value: 'abcdefg123'},
//  should return null when lowering null
    should(@nullString.exec(@ctx)).be.null

# TODO: Verify behavior since its different than JS
// Indexer
//  should get letter at index
    helloWorldSix: {value: 'W'},
//  should error on index 0 (out of bounds)
      helloWorldZero: {throws: true},

//  should error on index 20 (out of bounds)
      helloWorldTwenty: {throws: true},

//  should return null when string is null
    should(@nullString.exec(@ctx)).be.null
//  should return null when index is null
    should(@nullIndex.exec(@ctx)).be.null

// PositionOf
  it.skip 'should be a PositionOf', ->
    @found.should.be.an.instanceOf(str.Pos)
//  should return 1-based position
    found: {value: 3},
//  should return 0 when not found
    notFound: {value: 0},
//  should return null when pattern is null
    should(@nullPattern.exec(@ctx)).be.null
//  should return null when string is null
    should(@nullString.exec(@ctx)).be.null

// Substring
  it.skip 'should be a Substring', ->
    @world.should.be.an.instanceOf(str.Substring)
//  should get substring to end
    world: {value: 'World'},
//  should get substring with length
    or: {value: 'or'},
//  should get substring with zero length
    zeroLength: {value: ''},
//  should error on index 0 (out of bounds)
      startTooLow: {throws: true},

//  should error on too much length (out of bounds)
      tooMuchLength: {throws: true},

//  should error on negative length
      negativeLength: {throws: true},

//  should return null when string is null
    should(@nullString.exec(@ctx)).be.null
//  should return null when start is null
    should(@nullStart.exec(@ctx)).be.null

// Tuple
//  should be able to define a tuple
    e = @tup.exec(@ctx)
    e["a"].should.equal 1
    e["b"].should.equal 2

    /StdDev/ */
