"use strict"
// this is a stub that reads, compiles and writes an array of files
// todo need to shift the beautification to core

/////////////////////////////////// compile
var compiler = require('../cql_compile')
, expect = require('chai').expect
, Promise = require('bluebird')
, moment = require('moment')
, runtime = require('../runtime')
, _ = require('lodash')
, path = require('path')
, writeFile = Promise.promisify(require("fs").writeFile)
//, readFile = Promise.promisify(require("fs").readFile)
, beautify = require('js-beautify').js_beautify
, path_to_test_data = '../test_data/FHIR'
, debug = require('debug')('test')
, debugr = require('debug')('recompile')
, debuge = require('debug')('exceptions')


/**
  * Quick and dirty CQL compiler that reads a file and writes a file
  * @params object params
  * - files - collection of files
  * - source_dir
  * @returns promis
  */
function compile(params) {
  return Promise.each(params.files, function (file) {
    var file_base = path.resolve(__dirname, params.source_dir, file)
    var source = file_base + ".json"
      , target = file_base + ".js"
    debugr('Recompiling ' + source)
    var elm = require(source)
    return compiler.compile({elm: elm})
    .then(function (ugly) {
      var result = beautify(ugly, { indent_size: 2 })
      return writeFile(target, result)
    })
    .then(function(result) {
      debugr('done', target)
    })
  })
  debugr('initiation complete')
}

/**
  *
  *@params
  */
function getValues(values) {
  if (_.isObject(values)) {
    // I think _.map will do this..
    var result = _.isArray(values) ? [] : {}
    _.forEach(values, function (item, key) {
      var value = getValues(item && item.value)
      result[key] = value
    })
    return result
  } else {
    return values
  }
}

// this file is a test harness for test1.js file
  var system = {
    Promise: Promise,
    _: _,
    moment: moment,
    runtime: runtime,
    now: moment()
  }

describe('Executing JS', function() {

  before( function(done) {
    debugr('starting compiling')
//    setTimeout(done, 5000)
    compile({
      source_dir: "../test_cds",
      files: ["raw1", "library-cbp", "test1"],
    }).catch(function (e) {
      done(e)
    }).finally(function () {
      debugr('done compiling')
      done()
    })
  })

  describe('test1', function () {

    // a suite is a set of tests that inculdes:
    // - set of equals assertions
    // - (todo) error cases
    // cql
    // test data (todo)

  var suite_path = '.'
  var suites = [
  'test_suite1',
  'test_raw'
  ]
  _.forEach(suites, function(suite_file) {
    var suite = require(suite_path + '/' + suite_file)
    var factory = require(suite.path).module
    var library = factory.constructor(system)

      // loop of all the tests
      _.forEach(suite.equals, function(expected, method) {
        describe(method, function() {
          var is_function = _.isFunction(library[method])
          , actual, result
          it('should be a function', function () {
            expect(is_function).to.be.true
          })
          if (is_function) {
            try {
              debug('method', method)
              result = library[method]() || {}
              actual = result.value
              result.throws = false
            } catch (e) {
              result = {throws: true}
              if (!expected.throws) {
              debuge('method, e, e.stack', method, e, e.stack)
              }
            }
            if (expected.throws) {
              it('should throw exception', function () {
                expect(result.throws).to.be.true
              })
            } else {
              it('should not throw exception', function () {
                expect(result.throws).to.be.false
              })
            }

            if ((_.has(expected, 'properties') || _.has(expected, 'value')) && !_.isNull(expected.value)) {
              it('should return a $type property', function () {
                expect(result.$type).to.be.ok
              })
              // handle expected values
              if (_.has(expected, 'value')) {
                it('should return ' + expected.value + ' ', function () {
                  if (method === 'XLogY') actual = Math.round(actual, 12) // horrible hack
                  var actual_values = getValues(actual)
                  expect(actual_values).to.deep.equal(expected.value)
                })
              }
              // handle expected property collections
              if (_.has(expected, 'properties')) {
                _.forEach(expected.properties, function (expected_value, key) {
                  it('should have a ' + key + ' property of ' + expected_value, function () {
                    expect(expected_value).to.deep.equal(actual[key])
                  })
                })
              }
            }

            // hand checks to ensure properties are *not* set
            if (_.has(expected, 'not')) {
              _.forEach(expected.not, function (property) {
                it('should not have a ' + property + ' property', function () {
                  expect(actual).to.not.include.keys(property)
                })
              })
            }

            if (expected.unit) {
              it('should have a unit of ' + expected.unit + ' ', function () {
                expect(expected.unit).to.deep.equal(result.unit)
              })
            }

          }
        })
      })
    })
  })

  describe('testdata', function () {
    var test_system = require(path_to_test_data)
    var test_system_config = {} // placeholder for DI

    describe('patients resource', function () {
      it('should return at least 2 patients if not given id', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Patient'
        })
        .then(function(patients) {
          expect(patients.length).to.be.at.least(2)
          done()
        })
      })
      it('should single patients if given id', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Patient',
          patient_id: '105739'
        })
        .then(function(patients) {
          expect(patients.length).to.equal(1)
          done()
        })
      })
    })
    describe('observations resource', function () {
      it('should return at least 30 observations across all', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Observation'
        })
        .then(function(obs) {
          expect(obs.length).to.be.at.least(30)
          done()
        })
      })
      it('should return 15 observations for Luca', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Observation',
          patient_id: '105739'
        })
        .then(function(obs) {
          expect(obs.length).to.equal(15)
          done()
        })
      })

      it('should return 3 temperatures for Luca', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Observation',
          patient_id: '105739',
          codes: [{system:'http://loinc.org', code:'8310-5'}],
          codeProperty: 'code',
        })
        .then(function(obs) {
          expect(obs.length).to.equal(3)
          done()
        })
      })

      it('should return 5 for Luca on 17 June', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Observation',
          patient_id: '105739',
          dateProperty: 'effectiveDateTime',
          dateRange: {
            low: "2016-06-17T00:00:00+10:00",
            high: "2016-06-17T24:00:00+10:00",
            lowClosed: true,
            highClosed: true
          }
        })
        .then(function(obs) {
          expect(obs.length).to.equal(5)
          done()
        })
      })

      it('should return 1 temperatures for Luca on 17 June', function(done) {
        test_system(test_system_config).retrieve({
          typeUri: 'http://hl7.org/fhir',
          type: 'Observation',
          patient_id: '105739',
          codeProperty: 'code',
          codes: [{system:'http://loinc.org', code:'8310-5'}],
          dateProperty: 'effectiveDateTime',
          dateRange: {
            low: "2016-06-17T00:00:00+10:00",
            high: "2016-06-17T24:00:00+10:00",
            lowClosed: true,
            highClosed: true
          }
        })
        .then(function(obs) {
          expect(obs.length).to.equal(1)
          done()
        })
      })

    })
  })
})

/*
*/
