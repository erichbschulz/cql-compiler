  var X = 4;
  var Y = 5;
module.exports = {
path: '../test_cds/test1',
equals: {
   X: {value: 4},
   XPlusY: {value: X + Y},
   XMinusY: {value: X - Y},
   XTimesY: {value: X * Y},
   XDivideY: {value: X / Y},
   Precedence_24: {value: X * Y + X},
   Precedence_24a: {value: X + X * Y},
   Precedence_45: {value: (Y * Y) + X * Y},// nb brackets only in js
   Precedence_45a: {value: (Y * Y) + X * Y},
   Precedence_36: {value: X * (Y + X)},
   XLogY: {value: 3},
   XModuloY: {value: X % Y},
   XPowerY: {value: X * X * X * X * X},
   And_IsTrue: {value: true},
   And_IsFalse: {value: false},
   And_IsFalse1: {value: false},
   And_IsAlsoFalse: {value: false},
   And_IsAlsoFalse1: {value: false},
   And_IsNull: {value: null},
   And_IsNull1: {value: null},
   NotIsTrue: {value: true},
   NotIsFalse: {value: false},
   NotIsNull: {value: null},
   OrIsTrue: {value: true},
   OrIsTrue1: {value: true},
   OrIsTrue2: {value: true},
   OrIsAlsoTrue: {value: true},
   OrIsAlsoTrue1: {value: true},
   OrIsFalse: {value: false},
   OrIsNull: {value: null},
   OrIsNull1: {value: null},
   // these test cases
   // from https://github.com/cqframework/clinical_quality_language/blob/master/Src/coffeescript/cql-execution/test/elm/list/test.coffee
   IntList: {value: [9, 7, 8]},
   StringList: {value: ['a', 'bee', 'see']},
   MixedList: {value: [1, 'two', 3]},
   EmptyList: {value: []},
   Exists_EmptyList_false: {value: false},
   Exists_FullList_true: {value: true},
   // --Equal
   // should identify equal lists of integers
   EqualIntList: {value: true},
   // should identify unequal lists of integers
   UnequalIntList: {value: false},
   // should identify re-ordered lists of integers as unequal
   ReverseIntList: {value: false},
   // should identify equal lists of strings
   EqualStringList: {value: true},
   // should identify unequal lists of strings
   UnequalStringList: {value: false},
   // should identify equal lists of tuples
   EqualTupleList: {value: true},
   // should identify unequal lists of tuples
   UnequalTupleList: {value: false},
   // --NotEqual
   // should identify equal lists of integers
   not_EqualIntList: {value: false},
   // should identify unequal lists of integers
   not_UnequalIntList: {value: true},
   // should identify re-ordered lists of integers as unequal
   not_ReverseIntList: {value: true},
   // should identify equal lists of strings
   not_EqualStringList: {value: false},
   // should identify unequal lists of strings
   not_UnequalStringList: {value: true},
   // should identify equal lists of tuples
   not_EqualTupleList: {value: false},
   // should identify unequal lists of tuples
   not_UnequalTupleList: {value: true},
   // --Union
   // should union two lists to a single list
   OneToTen: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]},
   // should maintain duplicate elements (according to CQL spec)
   OneToFiveOverlapped: {value: [1, 2, 3, 4, 3, 4, 5]},
   // should not fill in values in a disjoint union
   Disjoint: {value: [1, 2, 4, 5]},
   // should return one list for multiple nested unions
   NestedToFifteen: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]},
   // should return null if either arg is null
   UnionNull: {value: null},
   NullUnion: {value: null},
   // --Except
   // should remove items in second list
   ExceptThreeFour: {value: [1, 2, 5]},
   // should not be commutative
   ThreeFourExcept: {value: []},
   // should remove items in second list regardless of order
   ExceptFiveThree: {value: [1, 2, 4]},
   // should be a no-op when lists have no common items
   ExceptNoOp: {value: [1, 2, 3, 4, 5]},
   // should remove all items when lists are the same
   ExceptEverything: {value: []},
   // should be a no-op when second list is empty
   SomethingExceptNothing: {value: [1, 2, 3, 4, 5]},
   // should be a no-op when first list is already empty
   NothingExceptSomething: {value: []},
   // should except lists of tuples
   ExceptTuples: {value: [{a: 1}, {a: 3}]},
   // should return null if either arg is null
   ExceptNull: {value: null},
   NullExcept: {value: null},
   // --Intersect
   // should intersect two disjoint lists  to an empty list
   NoIntersection: {value: []},
   // should intersect two lists with a single common element
   IntersectOnFive: {value: [5]},
   // should intersect two lists with several common elements
   IntersectOnEvens: {value: [2, 4, 6, 8, 10]},
   // should intersect two identical lists to the same list
   IntersectOnAll: {value: [1, 2, 3, 4, 5]},
   // should intersect multiple lists to only those elements common across all
   NestedIntersects: {value: [4, 5]},
   // should intersect lists of tuples
   IntersectTuples: {value: [{a:1, b:'c'}, {a:2, b:'c'}]},
   // should return null if either arg is null
   IntersectNull: {value: null},
   NullIntersect: {value: null},
   // --IndexOf
   // should return the correct 1-based index when an item is in the list
   IndexOfSecond: {value: 2},
   // should work with complex types like tuples
   IndexOfThirdTuple: {value: 3},
   // should return the first index when there are multiple matches
   IndexOf_MultipleMatches: {value: 4},
   // should return 0 when the item is not in the list
   IndexOf_ItemNotFound: {value: 0},
   // should return null if either arg is null
   IndexOf_NullList: {value: null},
   IndexOf_NullItem: {value: null},
   // --Indexer
   // should return the correct item based on the 1-based index
   SecondItem: {value: 'b'},
   // should return null if either arg is null
   NullList: {value: null},
   NullIndexer: {value: null},
   // --In
   // In_should execute to true when item is in list
   In_IsIn: {value: true},
   // In_should execute to false when item is not in list
   In_IsNotIn: {value: false},
   // In_should execute to true when tuple is in list
   In_TupleIsIn: {value: true},
   // In_should execute to false when tuple is not in list
   In_TupleIsNotIn: {value: false},
   // In_should return null if either arg is null
   In_NullIn: {value: null},
   In_Null: {value: null},
   // --Contains
   // should execute to true when item is in list
   C_IsIn: {value: true},
   // C_should execute to false when item is not in list
   C_IsNotIn: {value: false},
   // C_should execute to true when tuple is in list
   C_TupleIsIn: {value: true},
   // C_should execute to false when tuple is not in list
   C_TupleIsNotIn: {value: false},
   // C_should return null if either arg is null
   C_NullIn: {value: null},
   C_InNull: {value: null},
   // --Includes
   // should execute to true when sublist is in list
   I_IsIncluded: {value: true},
   // I_should execute to true when sublist is in list in different order
   I_IsIncludedReversed: {value: true},
   // I_should execute to true when lists are the same
   I_IsSame: {value: true},
   // I_should execute to false when sublist is not in list
   I_IsNotIncluded: {value: false},
   // I_should execute to true when tuple sublist is in list
   I_TuplesIncluded: {value: true},
   // I_should execute to false when tuple sublist is not in list
   I_TuplesNotIncluded: {value: false},
   // I_should return null if either arg is null
   I_NullIncluded: {value: null},
   I_NullIncludes: {value: null},
   // --IncludedIn
   // II_should execute to true when sublist is in list
   II_IsIncluded: {value: true},
   // II_should execute to true when sublist is in list in different order
   II_IsIncludedReversed: {value: true},
   // II_should execute to true when lists are the same
   II_IsSame: {value: true},
   // II_should execute to false when sublist is not in list
   II_IsNotIncluded: {value: false},
   // II_should execute to true when tuple sublist is in list
   II_TuplesIncluded: {value: true},
   // II_should execute to false when tuple sublist is not in list
   II_TuplesNotIncluded: {value: false},
   // II_should return null if either arg is null
   II_NullIncluded: {value: null},
   II_NullIncludes: {value: null},
   // --ProperIncludes
   // should execute to true when sublist is in list
   PI_IsIncluded: {value: true},
   // PI_should execute to true when sublist is in list in different order
   PI_IsIncludedReversed: {value: true},
   // PI_should execute to false when lists are the same
   PI_IsSame: {value: false},
   // PI_should execute to false when sublist is not in list
   PI_IsNotIncluded: {value: false},
   // PI_should execute to true when tuple sublist is in list
   PI_TuplesIncluded: {value: true},
   // PI_should execute to false when tuple sublist is not in list
   PI_TuplesNotIncluded: {value: false},
   //  PI_TODO: Support for ProperContains
   // PI_It.skip 'should return null if either arg is null', ->,
   PI_NullIncluded: {value: null},
   PI_NullIncludes: {value: null},
   // --ProperIncludedIn
   // should execute to true when sublist is in list
   PII_IsIncluded: {value: true},
   // PII_should execute to true when sublist is in list in different order
   PII_IsIncludedReversed: {value: true},
   // PII_should execute to false when lists are the same
   PII_IsSame: {value: false},
   // PII_should execute to false when sublist is not in list
   PII_IsNotIncluded: {value: false},
   // PII_should execute to true when tuple sublist is in list
   PII_TuplesIncluded: {value: true},
   // PII_should execute to false when tuple sublist is not in list
   PII_TuplesNotIncluded: {value: false},
   // PII_should return null if either arg is null
   PII_NullIncluded: {value: null},
   PII_NullIncludes: {value: null},
   // --Expand

   // Expand_should expand a list of lists
   Expand_ListOfLists: {value: [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1]},
   // Expand_should return null for a null list
   Expand_NullValue: {value: null},
   // --Distinct


   // Distinct_should remove duplicates
   Distinct_LotsOfDups: {value: [1, 2, 3, 4, 5]},
   // Distinct_should do nothing to an already distinct array
   Distinct_NoDups: {value: [2, 4, 6, 8, 10]},

   // --First
   // should get first of a list of numbers
   First_Numbers: {value: 1},
   // First_should get first of a list of letters
   First_Letters: {value: 'a'},
   // First_should get first of a list of lists
   First_Lists: {value: ['a','b','c']},
   // First_should get first of a list of tuples
   First_Tuples: {value: { a: 1, b: 2, c: 3 }},
   // First_should get first of a list of unordered numbers
   First_Unordered: {value: 3},
   // First_should return null for an empty list
   First_Empty: {value: null},
   // First_should return null for an empty list
   First_NullValue: {value: null},
   // --Last
   // should get last of a list of numbers
   Last_Numbers: {value: 4},
   // Last_should get last of a list of letters
   Last_Letters: {value: 'c'},
   // Last_should get last of a list of lists
   Last_Lists: {value: ['d','e','f']},
   // Last_should get last of a list of tuples
   Last_Tuples: {value: { a: 24, b: 25, c: 26 }},
   // Last_should get last of a list of unordered numbers
   Last_Unordered: {value: 2},
   // Last_should return null for an empty list
   Last_Empty: {value: null},
   // Last_should return null for an empty list
   Last_NullValue: {value: null},
   // --Length
   // Length_should get length of a list of numbers
   Length_Numbers: {value: 5},
   // Length_should get length of a list of lists
   Length_Lists: {value: 4},
   // Length_should get length of a list of tuples
   Length_Tuples: {value: 2},
   // Length_should get length of an empty list
   Length_Empty: {value: 0},
   // Length_should return null for an empty list
   Length_NullValue: {value: null},

     //try // should throw ArrayIndexOutOfBoundsException when accessing index 0
     //zeroIndex.exec(@ctx)
     //  should.fail("Accessing index zero should throw ArrayIndexOutOfBoundsException")
     //catch e
     //  e.should.be.instanceof ArrayIndexOutOfBoundsException
     //try // should throw ArrayIndexOutOfBoundsException when accessing out of bounds index
     //outOfBounds.exec(@ctx)
     //  should.fail("Accessing out of bounds index should throw ArrayIndexOutOfBoundsException")
     //catch e
     //  e.should.be.instanceof ArrayIndexOutOfBoundsException


  }
}

