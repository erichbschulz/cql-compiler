#!/usr/bin/env bash

# This bash script simply generates a set of ELM file from CQL files.

DIR=test_cds

function main() {
#  cql_to_elm test test1
  cql_to_elm raw raw1
}

function cql_to_elm() {
  IN=$1
  OUT=$2
  echo "Translating $IN.cql to $OUT.xml and and $OUT.json in $DIR"
  cp $DIR/$IN.cql ../../../cql/cqf_cql/Examples/$OUT.cql
#  docker exec -it javao Src/java/cql-to-elm/build/install/cql-to-elm/bin/cql-to-elm --input Examples/$OUT.cql
  docker exec -it javao Src/java/cql-to-elm/build/install/cql-to-elm/bin/cql-to-elm --input Examples/$OUT.cql -f JSON
  cp ../../../cql/cqf_cql/Examples/$OUT.* $DIR/
}

main
