#!/usr/bin/env node
"use strict"
// stub using more advanced CLI options based on the `commander` module
// eventually the CLI script will be based on this

// todo need to shift the beautification to core
var program = require('commander')
, compiler = require('../cql_compile')
, Promise = require('bluebird')
, beautify = require('js-beautify').js_beautify
, file_val

// define options for cli
program
.version('0.0.1')
.arguments('<file>')
.option('-i, --in [type]', 'Add the specified type of cheese [marble]', 'marble')
.action(function(file) {
    file_val = file
})
.parse(process.argv)

// if (program.bbqSauce) console.log('  - bbq')

compiler.compile({
  elm: require(file_val)
}).then(function(result) {
  result = beautify(result, { indent_size: 2 })
  console.log(result)
}).catch(function(e) {
  console.error("Error reading file", e)
})
