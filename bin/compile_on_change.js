"use strict"
// this is a stub that reads, compiles and writes an array of files
// todo need to shift the beautification to core

var compiler = require('../cql_compile')
, Promise = require('bluebird')
, path = require('path')
, _ = require('lodash')
, writeFile = Promise.promisify(require("fs").writeFile)
//, readFile = Promise.promisify(require("fs").readFile)
, beautify = require('js-beautify').js_beautify

function compile(params) {
  _.forEach(params.files, function (file) {
    var file_base = path.resolve(__dirname, params.source_dir, file)
    var source = file_base + ".json"
      , target = file_base + ".js"
    console.log('Recompiling ' + source)
    var elm = require(source)
    compiler.compile({elm: elm})
    .then(function (ugly) {
      var result = beautify(ugly, { indent_size: 2 })
      return writeFile(target, result)
    })
    .then(function(result) {
      console.log('done', target)
    })

  })
  console.log('initiation complete')
}

compile({
  source_dir: "../test_cds",
  files: ["library-cbp", "test1"],
})
