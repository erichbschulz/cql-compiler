"use strict"
/**
  Utilities to parse FHIR data
  */

var Promise = require('bluebird')
var expect = require('chai').expect
var fs = require('fs')
var _ = require('lodash')
var path = require('path')
var moment = require('moment')
var writeFilePromise = Promise.promisify(fs.writeFile)
var readFilePromise = Promise.promisify(fs.readFile)
var statFilePromise = Promise.promisify(fs.stat)
var debug = require('debug')('testfhir')

module.exports = function(config) {

  // main export object
  var fhir = {}
  var all_test_patients = [105739, 105767]

//// Request looks like this:
//  system.interface.Retrieve({
//    type: "Observation",
//    typeUri: "http://hl7.org/fhir",
//    codeProperty: 'code',
//    codes: priv['Systolic blood pressure'](),
//    dateProperty: 'effectiveDateTime.value',
//    dateRange: ,
//    patient: context.patient.id
//  })
//
//// Data looks like:
//  "code":{"coding":[{"system":"http://loinc.org", "code":"8310-5"}]},
//  "subject":{"reference":"Patient/105738"},
//  "effectiveDateTime":"2016-06-24T20:44:01+10:00",

  // this code needs optimising - first cut to get it working :-)
  fhir.retrieve = function(params) {
    expect(params.typeUri).to.equal('http://hl7.org/fhir')
    var patients = params.patient_id
      ? [params.patient_id] // wrap provided individual in []
      : all_test_patients // no patient provided so grab all
    // get array of resources
    return multiPatientResourceByType(patients, params.type)
    // apply the filter
    .then(function (resources) {
      return _.filter(resources, function(r) {
        // check dates (if range given)
        var dates_match = (!params.dateRange) ||
          in_date_range(r[params.dateProperty], params.dateRange)
        var codes_match = (!params.codes) ||
          code_list_intersects(r[params.codeProperty].coding, params.codes)
        return dates_match && codes_match
      })
    })
  }

  /* test date in range
  // @param string date "2016-06-24T20:44:01+10:00""
  // @param object eg:
  //            low: "2016-06-16T20:44:01+10:00",
  //            high: "2016-06-17T20:44:01+10:00",
  //            lowClosed: true,
  //            highClosed: true
  // @returns boolean
  */
  function in_date_range(date, range) {
    // todo execute this in the outer loop
    var inclusivity = (range.lowClosed ? '[' : '(') +
      (range.highClosed ? ']' : ')')
    return moment(date).isBetween(range.low, range.high, inclusivity)
  }

  /* test if a set of codes intersecte
  // @param setA array of {system, codes}
  // @param setB array of {system, codes}
  // @returns boolean
  */
  function code_list_intersects(setA, setB) {
    return !!_.find(setA, function(codeA) {
      return _.find(setB, function(codeB) {
        return codeA.system === codeB.system && codeA.code === codeB.code
      })
    })
  }

  /* load test data for a set of patients
  // @param array of patient_id as string eg "12345"
  // @param type string FHIR resource type (eg Observation)
  // @returns promise of [] of the resource
  */
  function multiPatientResourceByType(patients, type) {
    // create and array of arrays
    return Promise.map(patients, function(patient_id) {
      return patientResourceByType(patient_id, type)
    })
    // make into single list
    .then(_.flatten)
  }

  /* load test data courtesy of ClinFHIR */
  // @param patient_id string eg "12345"
  // @param type string FHIR resource type (eg Observation)
  // @returns promise of [] of the resource
  function patientResourceByType(patient_id, type) {
    var patient_json = require(path.join(__dirname,
        'patient' + patient_id + '.json'))
    var all = patient_json.entry
    var result = _.reduce(all, function(result, value) {
      if (value.resource.resourceType === type) result.push(value.resource)
      return result
    }, [])
//    debug('result', result);
    return Promise.resolve(result)
  }

  return fhir

}
