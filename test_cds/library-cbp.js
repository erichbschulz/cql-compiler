'use strict';
(function(exports) {

  exports.module = {
    meta: {
      id: 'CBP',
      version: '1'
    },

    constructor: function(system) {
      var r = system.runtime,
        _ = system._,
        pub = {},
        priv = {},
        context = {}

      priv['Patient'] = function() {
        // type: undefined
        // deps: {"0":{"system":"{http://hl7.org/fhir}Patient"},"1":{"context":{"patient":"id"}}}
        return ({
          $type: '{http://hl7.org/fhir}Patient',
          value: r.SingletonFrom(system.interface.Retrieve({
            $type: "Patient",
            typeUri: "http://hl7.org/fhir",
            patient: context.patient.id
          }))
        })
      }

      pub['Lookback Interval One Year'] =
        priv['Lookback Interval One Year'] = function() {
          // type: undefined
          // deps: {}
          return ({
            lowClosed: true,
            highClosed: true,
            low: //deps: {"0":{"params":"MeasurementPeriod"}}
              r.Do("Subtract", [r.ExtractProperty(params['MeasurementPeriod'], "low"), {
              value: 1,
              unit: "years",
              $type: "Quantity"
            }]),
            high: r.ExtractProperty(params['MeasurementPeriod'], "high"),
            $type: "Interval"
          })
        }

      pub['Lookback Interval Three Years'] =
        priv['Lookback Interval Three Years'] = function() {
          // type: undefined
          // deps: {}
          return ({
            lowClosed: true,
            highClosed: true,
            low: //deps: {"0":{"params":"MeasurementPeriod"}}
              r.Do("Subtract", [r.ExtractProperty(params['MeasurementPeriod'], "low"), {
              value: 3,
              unit: "years",
              $type: "Quantity"
            }]),
            high: r.ExtractProperty(params['MeasurementPeriod'], "high"),
            $type: "Interval"
          })
        }

      pub['In Demographic'] =
        priv['In Demographic'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: true,
            $type: "Boolean"
          })
        }

      pub['Systolic blood pressure'] =
        priv['Systolic blood pressure'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            display: "Systolic blood pressure",
            code: "8480-6",
            system: {
              "name": "LOINC"
            },
            $type: Code
          })
        }

      pub['Diastolic blood pressure'] =
        priv['Diastolic blood pressure'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            display: "Diastolic blood pressure",
            code: "8462-4",
            system: {
              "name": "LOINC"
            },
            $type: Code
          })
        }

      pub['BP: Systolic'] =
        priv['BP: Systolic'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"system":"{http://hl7.org/fhir}Observation"},"1":{"expression":"Systolic blood pressure"},"2":{"expression":"Lookback Interval One Year"},"3":{"context":{"patient":"id"}}}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['O'].value, function(rec1, index1) {
                cursor['O'] = rec1
                var include = //deps: {"0":{},"1":{}}
                  r.Do("In", [cursor["O"]["status"], //deps: {}
                    {
                      $type: 'List',
                      value: [{
                        simple: true,
                        value: "final",
                        $type: "String"
                      }, {
                        simple: true,
                        value: "amended",
                        $type: "String"
                      }]
                    }
                  ])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              O: system.interface.Retrieve({
                $type: "Observation",
                typeUri: "http://hl7.org/fhir",
                codeProperty: 'code',
                codes: priv['Systolic blood pressure'](),
                dateProperty: 'effectiveDateTime.value',
                dateRange: priv['Lookback Interval One Year'](),
                patient: context.patient.id
              })
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Diastolic'] =
        priv['BP: Diastolic'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"system":"{http://hl7.org/fhir}Observation"},"1":{"expression":"Diastolic blood pressure"},"2":{"expression":"Lookback Interval One Year"},"3":{"context":{"patient":"id"}}}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['O'].value, function(rec1, index1) {
                cursor['O'] = rec1
                var include = //deps: {"0":{},"1":{}}
                  r.Do("In", [cursor["O"]["status"], //deps: {}
                    {
                      $type: 'List',
                      value: [{
                        simple: true,
                        value: "final",
                        $type: "String"
                      }, {
                        simple: true,
                        value: "amended",
                        $type: "String"
                      }]
                    }
                  ])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              O: system.interface.Retrieve({
                $type: "Observation",
                typeUri: "http://hl7.org/fhir",
                codeProperty: 'code',
                codes: priv['Diastolic blood pressure'](),
                dateProperty: 'effectiveDateTime.value',
                dateRange: priv['Lookback Interval One Year'](),
                patient: context.patient.id
              })
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Systolic Less Than 140'] =
        priv['BP: Systolic Less Than 140'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"BP: Systolic"}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['S'].value, function(rec1, index1) {
                cursor['S'] = rec1
                var include = //deps: {"0":{}}
                  r.Do("Less", [cursor["S"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                    simple: true,
                    value: 140,
                    $type: "Integer"
                  }])])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              S: priv['BP: Systolic']()
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Systolic Greater Than/Equal To 140'] =
        priv['BP: Systolic Greater Than/Equal To 140'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"BP: Systolic"}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['S'].value, function(rec1, index1) {
                cursor['S'] = rec1
                var include = //deps: {"0":{}}
                  r.Do("GreaterOrEqual", [cursor["S"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                    simple: true,
                    value: 140,
                    $type: "Integer"
                  }])])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              S: priv['BP: Systolic']()
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Diastolic Less Than 80'] =
        priv['BP: Diastolic Less Than 80'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"BP: Diastolic"}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['D'].value, function(rec1, index1) {
                cursor['D'] = rec1
                var include = //deps: {"0":{}}
                  r.Do("Less", [cursor["D"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                    simple: true,
                    value: 80,
                    $type: "Integer"
                  }])])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              D: priv['BP: Diastolic']()
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Diastolic 80-89'] =
        priv['BP: Diastolic 80-89'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"BP: Diastolic"}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['D'].value, function(rec1, index1) {
                cursor['D'] = rec1
                var include = //deps: {"0":{"0":{}},"1":{"0":{}}}
                  r.Do("And", [ //deps: {"0":{}}
                    r.Do("GreaterOrEqual", [cursor["D"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                      simple: true,
                      value: 80,
                      $type: "Integer"
                    }])]), //deps: {"0":{}}
                    r.Do("LessOrEqual", [cursor["D"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                      simple: true,
                      value: 89,
                      $type: "Integer"
                    }])])
                  ])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              D: priv['BP: Diastolic']()
            })),
            $type: 'I do not know'
          })
        }

      pub['BP: Diastolic Greater Than/Equal To 90'] =
        priv['BP: Diastolic Greater Than/Equal To 90'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"BP: Diastolic"}}
          return ({
            value: (function(sources) {
              var result = []
              var cursor = {}
              _.forEach(sources['D'].value, function(rec1, index1) {
                cursor['D'] = rec1
                var include = //deps: {"0":{}}
                  r.Do("GreaterOrEqual", [cursor["D"]["valueQuantity"], r.Do("ToDecimal", ["Decimal", {
                    simple: true,
                    value: 90,
                    $type: "Integer"
                  }])])
                if (include) {
                  console.log('Included', cursor)
                  result.push(_.clone(cursor))
                } else {
                  console.log('excluded')
                }
              })
              return result
            }({
              D: priv['BP: Diastolic']()
            })),
            $type: 'I do not know'
          })
        }

      return pub

    }
  }
})(typeof exports === 'undefined' ? this['CBP'] = {} : exports)