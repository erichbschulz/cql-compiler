validateQuantity = (object,expectedValue,expectedUnit) ->
  object.constructor.name.should.equal "Quantity"
  object.value.should.equal expectedValue
  object.unit.should.equal expectedUnit

describe 'Count', ->
  it 'should be able to count lists without nulls', ->
    @not_null.exec(@ctx).should.equal 5
  it 'should be able to count lists with nulls', ->
    @has_null.exec(@ctx).should.equal 2
  it 'should be able to count empty list', ->
    @empty.exec(@ctx).should.equal 0

describe 'Sum', ->
  it 'should be able to sum lists without nulls', ->
    @not_null.exec(@ctx).should.equal 15
  it 'should be able to sum lists with nulls', ->
    @has_null.exec(@ctx).should.equal 3
  it 'should be able to sum empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to sum quantity lists without nulls', ->
    validateQuantity @not_null_q.exec(@ctx), 15, 'ml'
  it 'should be able to sum  quantity lists with nulls', ->
    validateQuantity @has_null_q.exec(@ctx), 3, 'ml'
  it 'should return null for unmatched units in a list of quantiies', ->
    @unmatched_units_q.exec(@ctx) == null

describe 'Min', ->
  it 'should be able to find min in lists without nulls', ->
    @not_null.exec(@ctx).should.equal 0
  it 'should be able to find min in lists with nulls', ->
    @has_null.exec(@ctx).should.equal -1
  it 'should be return null for empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to find min in lists of quantiies without nulls', ->
    validateQuantity @not_null_q.exec(@ctx), 0, 'ml'
  it 'should be able to find min in lists of quantiies with nulls', ->
    validateQuantity @has_null_q.exec(@ctx), -1 , 'ml'

describe 'Max', ->
  it 'should be able to find max in lists without nulls', ->
    @not_null.exec(@ctx).should.equal 10
  it 'should be able to find max in lists with nulls', ->
    @has_null.exec(@ctx).should.equal 2
  it 'should be return null for empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to find max in lists of quantiies without nulls', ->
    validateQuantity @not_null_q.exec(@ctx),  10, 'ml'
  it 'should be able to find max in lists of quantiies with nulls', ->
    validateQuantity @has_null_q.exec(@ctx), 2, 'ml'

describe 'Avg', ->
  it 'should be able to find average for lists without nulls', ->
    @not_null.exec(@ctx).should.equal 3
  it 'should be able to find average for lists with nulls', ->
    @has_null.exec(@ctx).should.equal 1.5
  it 'should be return null for empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to find average for lists of quantiies without nulls', ->
    validateQuantity @not_null_q.exec(@ctx), 3, 'ml'
  it 'should be able to find average for lists of quantiies with nulls', ->
    validateQuantity @has_null_q.exec(@ctx), 1.5 , 'ml'

describe 'Median', ->
  it 'should be able to find median of odd numbered list', ->
    @odd.exec(@ctx).should.equal 3
  it 'should be able to find median of even numbered list', ->
    @even.exec(@ctx).should.equal 3.5
  it 'should be able to find median of odd numbered list that contains duplicates', ->
    @dup_vals_odd.exec(@ctx).should.equal 3
  it 'should be able to find median of even numbered list that contians duplicates', ->
    @dup_vals_even.exec(@ctx).should.equal 2.5
  it 'should be return null for empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to find median of odd numbered list', ->
    validateQuantity @odd_q.exec(@ctx),  3 , 'ml'
  it 'should be able to find median of even numbered list', ->
    validateQuantity @even_q.exec(@ctx), 3.5, 'ml'
  it 'should be able to find median of odd numbered list that contains duplicates', ->
    validateQuantity @dup_vals_odd_q.exec(@ctx),3, 'ml'
  it 'should be able to find median of even numbered list that contians duplicates', ->
    validateQuantity @dup_vals_even_q.exec(@ctx), 2.5, 'ml'

describe 'Mode', ->
  it 'should be able to find mode of lists without nulls', ->
    @not_null.exec(@ctx).should.equal 2
  it 'should be able to find Mode lists with nulls', ->
    @has_null.exec(@ctx).should.equal 2
  it 'should be return null for empty list', ->
    @empty.exec(@ctx) == null
  it 'should be able to find bimodal', ->
    @bi_modal.exec(@ctx).should.eql [2,3]

describe 'PopulationVariance', ->
  it 'should be able to find PopulationVariance of a list ', ->
    @v.exec(@ctx).should.equal 2.5
  it 'should be able to find PopulationVariance of a list ', ->
    validateQuantity @v_q.exec(@ctx), 2.5, 'ml'

describe 'Variance', ->
  it 'should be able to find Variance of a list ', ->
    @v.exec(@ctx).should.equal 2
  it 'should be able to find Variance of a list ', ->
    validateQuantity @v_q.exec(@ctx), 2, 'ml'

describe 'StdDev', ->
  it 'should be able to find Standard Dev of a list ', ->
    @std.exec(@ctx).should.equal 1.4142135623730951
  it 'should be able to find Standard Dev of a list ', ->
    validateQuantity @std_q.exec(@ctx), 1.4142135623730951, 'ml'

describe 'PopulationStdDev', ->
  it 'should be able to find Population Standard Dev of a list ', ->
    @dev.exec(@ctx).should.equal 1.5811388300841898
  it 'should be able to find Population Standard Dev of a list ', ->
    validateQuantity @dev_q.exec(@ctx), 1.5811388300841898, 'ml'

describe 'AllTrue', ->
  it 'should be able to calculate all true', ->
    @at.exec(@ctx).should.equal true
    @atwn.exec(@ctx).should.equal false
    @atf.exec(@ctx).should.equal false
    @atfwn.exec(@ctx).should.equal false

describe 'AnyTrue', ->
  it 'should be able to calculate any true', ->
    @at.exec(@ctx).should.equal true
    @atwn.exec(@ctx).should.equal true
    @atf.exec(@ctx).should.equal false
    @atfwn.exec(@ctx).should.equal false

describe 'Add', ->
  it 'should add two numbers', ->
    @onePlusTwo.exec(@ctx).should.equal 3

  it 'should add multiple numbers', ->
    @addMultiple.exec(@ctx).should.equal 55

  it 'should add variables', ->
    @addVariables.exec(@ctx).should.equal 21

describe 'Subtract', ->
  it 'should subtract two numbers', ->
    @fiveMinusTwo.exec(@ctx).should.equal 3

  it 'should subtract multiple numbers', ->
    @subtractMultiple.exec(@ctx).should.equal 15

  it 'should subtract variables', ->
    @subtractVariables.exec(@ctx).should.equal 1

describe 'Multiply', ->
  it 'should multiply two numbers', ->
    @fiveTimesTwo.exec(@ctx).should.equal 10

  it 'should multiply multiple numbers', ->
    @multiplyMultiple.exec(@ctx).should.equal 120

  it 'should multiply variables', ->
    @multiplyVariables.exec(@ctx).should.equal 110

describe 'Divide', ->
  it 'should divide two numbers', ->
    @tenDividedByTwo.exec(@ctx).should.equal 5

  it 'should divide two numbers that don\'t evenly divide', ->
    @tenDividedByFour.exec(@ctx).should.equal 2.5

  it 'should divide multiple numbers', ->
    @divideMultiple.exec(@ctx).should.equal 5

  it 'should divide variables', ->
    @divideVariables.exec(@ctx).should.equal 25

describe 'Negate', ->
  it 'should negate a number', ->
    @negativeOne.exec(@ctx).should.equal -1

describe 'MathPrecedence', ->
  it 'should follow order of operations', ->
    @mixed.exec(@ctx).should.equal 46

  it 'should allow parentheses to override order of operations', ->
    @parenthetical.exec(@ctx).should.equal -10

describe  'Power', ->
  it "should be able to calculate the power of a number" , ->
    @pow.exec(@ctx).should.equal 81

describe 'TruncatedDivide', ->
  it "should be able to return just the integer portion of a division", ->
    @trunc.exec(@ctx).should.equal 3
    @even.exec(@ctx).should.equal 3

describe  'Truncate', ->
  it "should be able to return the integer portion of a number", ->
    @trunc.exec(@ctx).should.equal 10
    @even.exec(@ctx).should.equal 10

describe  'Floor', ->
  it "should be able to round down to the closest integer", ->
    @flr.exec(@ctx).should.equal 10
    @even.exec(@ctx).should.equal 10

describe 'Ceiling', ->
    it "should be able to round up to the closest integer", ->
      @ceil.exec(@ctx).should.equal 11
      @even.exec(@ctx).should.equal 10

describe 'Ln', ->
  it "should be able to return the natural log of a number", ->
    @ln.exec(@ctx).should.equal Math.log(4)

describe 'Log', ->
    it "should be able to return the log of a number based on an arbitary base value", ->
      @log.exec(@ctx).should.equal 0.25

describe 'Modulo', ->
    it "should be able to return the remainder of a division", ->
      @mod.exec(@ctx).should.equal 1

describe 'Abs', ->
  it "should be able to return the absolute value of a positive number", ->
    @pos.exec(@ctx).should.equal 10
  it "should be able to return the absolute value of a negative number", ->
    @neg.exec(@ctx).should.equal 10
  it "should be able to return the absolute value of 0", ->
    @zero.exec(@ctx).should.equal 0

describe 'Round', ->
  it "should be able to round a number up or down to the closest integer value", ->
    @up.exec(@ctx).should.equal 5
    @down.exec(@ctx).should.equal 4
  it "should be able to round a number up or down to the closest decimal place ", ->
    @up_percent.exec(@ctx).should.equal 4.6
    @down_percent.exec(@ctx).should.equal 4.4

describe 'Successor', ->
  it "should be able to get Integer Successor", ->
    @is.exec(@ctx).should.equal 3
  it "should be able to get Real Successor", ->
    @rs.exec(@ctx).should.equal ( 2.2  + Math.pow(10,-8) )

  it "should cause runtime error for Successor greater than Integer Max value" , ->
    a = false
    try
      @ofr.exec(@ctx)
    catch e
      e.constructor.name.should.equal "OverFlowException"
      a = true

    a.should.equal true

  it "should be able to get Date Successor for year", ->
    dp = @y_date.exec(@ctx)
    dp.year.should.equal 2016
    should.not.exist dp.month
    should.not.exist dp.day
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month", ->
    dp = @ym_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 2
    should.not.exist dp.day
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month,day", ->
    dp = @ymd_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 1
    dp.day.should.equal 2
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month,day,hour", ->
    dp = @ymdh_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 1
    dp.day.should.equal 1
    dp.hour.should.equal 1
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month,day,hour,minute", ->
    dp = @ymdhm_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 1
    dp.day.should.equal 1
    dp.hour.should.equal 0
    dp.minute.should.equal 1
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month,day,hour,minute,seconds", ->
    dp = @ymdhms_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 1
    dp.day.should.equal 1
    dp.hour.should.equal 0
    dp.minute.should.equal 0
    dp.second.should.equal 1
    should.not.exist  dp.millisecond

  it "should be able to get Date Successor for year,month,day,hour,minute,seconds,milliseconds", ->
    dp = @ymdhmsm_date.exec(@ctx)
    dp.year.should.equal 2015
    dp.month.should.equal 1
    dp.day.should.equal 1
    dp.hour.should.equal 0
    dp.minute.should.equal 0
    dp.second.should.equal 0
    dp.millisecond.should.equal 1

  it "should throw an exception when attempting to get the Successor of the maximum allowed date", ->
    a = false
    try
      @max_date.exec(@ctx)
    catch e
      e.constructor.name.should.equal "OverFlowException"
      a = true
     a.should.equal true


describe 'Predecessor', ->
  it "should be able to get Integer Predecessor", ->
    @is.exec(@ctx).should.equal 1
  it "should be able to get Real Predecessor", ->
    @rs.exec(@ctx).should.equal ( 2.2  - Math.pow(10,-8))
  it "should cause runtime error for Predecessor greater than Integer Max value" , ->
    a = false
    try
      @ufr.exec(@ctx)
    catch e
      e.constructor.name.should.equal "OverFlowException"
      a = true

    a.should.equal true

  it "should be able to get Date Predecessor for year", ->
    dp = @y_date.exec(@ctx)
    dp.year.should.equal 2014
    should.not.exist dp.month
    should.not.exist dp.day
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Predecessor for year,month", ->
    dp = @ym_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    should.not.exist dp.day
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Predecessor for year,month,day", ->
    dp = @ymd_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    dp.day.should.equal 31
    should.not.exist dp.hour
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond
  it "should be able to get Date Predecessor for year,month,day,hour", ->
    dp = @ymdh_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    dp.day.should.equal 31
    dp.hour.should.equal 23
    should.not.exist dp.minute
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Predecessor for year,month,day,hour,minute", ->
    dp = @ymdhm_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    dp.day.should.equal 31
    dp.hour.should.equal 23
    dp.minute.should.equal 59
    should.not.exist dp.second
    should.not.exist  dp.millisecond

  it "should be able to get Date Predecessor for year,month,day,hour,minute,seconds", ->
    dp = @ymdhms_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    dp.day.should.equal 31
    dp.hour.should.equal 23
    dp.minute.should.equal 59
    dp.second.should.equal 59
    should.not.exist  dp.millisecond

  it "should be able to get Date Predecessor for year,month,day,hour,minute,seconds,milliseconds", ->
    dp = @ymdhmsm_date.exec(@ctx)
    dp.year.should.equal 2014
    dp.month.should.equal 12
    dp.day.should.equal 31
    dp.hour.should.equal 23
    dp.minute.should.equal 59
    dp.millisecond.should.equal 999

  it "should throw an exception when attempting to get the Predecessor of the minimum allowed date", ->
    a = false
    try
      @min_date.exec(@ctx)
    catch e
      e.constructor.name.should.equal "OverFlowException"
      a = true
     a.should.equal true

describe 'Quantity', ->
  it "should be able to perform Quantity Addition", ->
    aqq = @add_q_q.exec(@ctx)
    aqq.value.should.equal 20
    aqq.unit.should.equal 'days'
    adq = @add_d_q.exec(@ctx)
    adq.constructor.name.should.equal "DateTime"
    adq.year.should.equal 2000
    adq.month.should.equal 1
    adq.day.should.equal 11


  it "should be able to perform Quantity Subtraction", ->
    sqq = @sub_q_q.exec(@ctx)
    sqq.value.should.equal 0
    sqq.unit.should.equal 'days'
    sdq = @sub_d_q.exec(@ctx)
    sdq.constructor.name.should.equal "DateTime"
    sdq.year.should.equal 1999
    sdq.month.should.equal 12
    sdq.day.should.equal 22

  it "should be able to perform Quantity Division", ->
    dqd = @div_q_d.exec(@ctx)
    dqd.constructor.name.should.equal "Quantity"
    dqd.unit.should.equal "days"
    dqd.value.should.equal 5
    dqq = @div_q_q.exec(@ctx)
    dqq.should.equal 1

  it "should be able to perform Quantity Multiplication", ->
    mdq = @mul_d_q.exec(@ctx)
    mdq.constructor.name.should.equal "Quantity"
    mdq.unit.should.equal "days"
    mdq.value.should.equal 20
    mqd = @mul_q_d.exec(@ctx)
    mqd.constructor.name.should.equal "Quantity"
    mqd.unit.should.equal "days"
    mqd.value.should.equal 20

  it "should be able to perform Quantity Absolution", ->
    q = @abs.exec(@ctx)
    q.value.should.equal 10
    q.unit.should.equal 'days'

  it "should be able to perform Quantity Negation", ->
    q = @neg.exec(@ctx)
    q.value.should.equal -10
    q.unit.should.equal 'days'
vsets = require './valuesets'
{ p1, p2 } = require './patients'
{ PatientSource} = require '../../../lib/cql-patient'


describe 'ValueSetDef', ->
  , [], vsets

  it 'should return a resolved value set when the codeService knows about it', ->
    vs = @known.exec(@ctx)
    vs.oid.should.equal '2.16.840.1.113883.3.464.1003.101.12.1061'
    vs.version.should.equal '20140501'
    vs.codes.length.should.equal 3

  it 'should execute one-arg to ValueSet with ID', ->
    vs = @['unknown One Arg'].exec(@ctx)
    vs.oid.should.equal '1.2.3.4.5.6.7.8.9'
    should.not.exist vs.version

  it 'should execute two-arg to ValueSet with ID and version', ->
    vs = @['unknown Two Arg'].exec(@ctx)
    vs.oid.should.equal '1.2.3.4.5.6.7.8.9'
    vs.version.should.equal '1'

describe 'ValueSetRef', ->
  it 'should have a name', ->
    @foo.name.should.equal 'Acute Pharyngitis'

  it 'should execute to the defined value set', ->
    @foo.exec(@ctx).oid.should.equal '2.16.840.1.113883.3.464.1003.101.12.1001'

describe 'InValueSet', ->
  , [], vsets

  it 'should find string code in value set', ->
    @string.exec(@ctx).should.be.true

  it 'should find string code in versioned value set', ->
    @stringInVersionedValueSet.exec(@ctx).should.be.true

  it 'should find short code in value set', ->
    @shortCode.exec(@ctx).should.be.true

  it 'should find medium code in value set', ->
    @mediumCode.exec(@ctx).should.be.true

  it 'should find long code in value set', ->
    @longCode.exec(@ctx).should.be.true

  it 'should not find string code in value set', ->
    @wrongString.exec(@ctx).should.be.false

  it 'should not find string code in versioned value set', ->
    @wrongStringInVersionedValueSet.exec(@ctx).should.be.false

  it 'should not find short code in value set', ->
    @wrongShortCode.exec(@ctx).should.be.false

  it 'should not find medium code in value set', ->
    @wrongMediumCode.exec(@ctx).should.be.false

  it 'should not find long code in value set', ->
    @wrongLongCode.exec(@ctx).should.be.false

describe 'Patient Property In ValueSet', ->
  , [], vsets

  it 'should find that John is not female', ->
    @ctx.patient =  new PatientSource([ p1 ]).currentPatient()
    @isFemale.exec(@ctx).should.be.false

  it 'should find that Sally is female', ->
    @ctx.patient =  new PatientSource([ p2 ]).currentPatient()
    @isFemale.exec(@ctx).should.be.true

describe 'CalculateAge', ->
  , [ p1 ]
    # Note, tests are inexact (otherwise test needs to repeat exact logic we're testing)
    # p1 birth date is 1980-06-17
    @bday = new Date(1980, 5, 17)
    @today = new Date()
    # according to spec, dates without timezones are in *current* time offset, so need to adjust
    offsetDiff = @today.getTimezoneOffset() - @bday.getTimezoneOffset()
    @bday.setMinutes(@bday.getMinutes() + offsetDiff)

    # this is getting the possible number of months in years with the addtion of an offset
    # to get the correct number of months
    @full_months = ((@today.getFullYear() - 1980) * 12) + (@today.getMonth() - 5)
    @timediff = @today - @bday # diff in milliseconds

  it 'should execute age in years', ->
    @years.exec(@ctx).should.equal @full_months // 12

  it 'should execute age in months', ->
    # what is returned will depend on whether the day in the current month has
    # made it to the 17th day of the month as declared in the birthday
    [@full_months, @full_months-1].indexOf(@months.exec(@ctx)).should.not.equal -1

  it 'should execute age in days', ->
    @days.exec(@ctx).should.equal @timediff // 1000 // 60 // 60 // 24

  it 'should execute age in hours', ->
    # a little strange since the qicore data model specified birthDate as a date (no time)
    @hours.exec(@ctx).should.equal @timediff // 1000 // 60 // 60

  it 'should execute age in minutes', ->
    # a little strange since the qicore data model specified birthDate as a date (no time)
    @minutes.exec(@ctx).should.equal @timediff // 1000 // 60

  it 'should execute age in seconds', ->
    # a little strange since the qicore data model specified birthDate as a date (no time)
    @seconds.exec(@ctx).should.equal @timediff // 1000

describe 'CalculateAgeAt', ->
  , [ p1 ]

  it 'should execute age at 2012 as 31', ->
    @ageAt2012.exec(@ctx).should.equal 31

  it 'should execute age at 19810216 as 0', ->
    @ageAt19810216.exec(@ctx).should.equal 0

  it 'should execute age at 1975 as -5', ->
    @ageAt19810216.exec(@ctx).should.equal 0

# TO Comparisons for Dates

describe 'Equal', ->
  it 'should be false for 5 = 4', ->
    @aGtB_Int.exec(@ctx).should.be.false

  it 'should be true for 5 = 5', ->
    @aEqB_Int.exec(@ctx).should.be.true

  it 'should be false for 5 = 6', ->
    @aLtB_Int.exec(@ctx).should.be.false

  it 'should identify equal/unequal tuples', ->
    @eqTuples.exec(@ctx).should.be.true
    @uneqTuples.exec(@ctx).should.be.false

  it 'should identify equal/unequal DateTimes in same timezone', ->
    @eqDateTimes.exec(@ctx).should.be.true
    @uneqDateTimes.exec(@ctx).should.be.false

  it 'should identify equal/unequal DateTimes in different timezones', ->
    @eqDateTimesTZ.exec(@ctx).should.be.true
    @uneqDateTimesTZ.exec(@ctx).should.be.false

  it 'should identify uncertain/unequal DateTimes when there is imprecision', ->
    should(@possiblyEqualDateTimes.exec(@ctx)).be.null
    @impossiblyEqualDateTimes.exec(@ctx).should.be.false

describe 'NotEqual', ->
  it 'should be true for 5 <> 4', ->
    @aGtB_Int.exec(@ctx).should.be.true

  it 'should be false for 5 <> 5', ->
    @aEqB_Int.exec(@ctx).should.be.false

  it 'should be true for 5 <> 6', ->
    @aLtB_Int.exec(@ctx).should.be.true

  it 'should identify equal/unequal tuples', ->
    @eqTuples.exec(@ctx).should.be.false
    @uneqTuples.exec(@ctx).should.be.true

  it 'should identify equal/unequal DateTimes in same timezone', ->
    @eqDateTimes.exec(@ctx).should.be.false
    @uneqDateTimes.exec(@ctx).should.be.true

  it 'should identify equal/unequal DateTimes in different timezones', ->
    @eqDateTimesTZ.exec(@ctx).should.be.false
    @uneqDateTimesTZ.exec(@ctx).should.be.true

  it 'should identify uncertain/unequal DateTimes when there is imprecision', ->
    should(@possiblyEqualDateTimes.exec(@ctx)).be.null
    @impossiblyEqualDateTimes.exec(@ctx).should.be.true

describe 'Less', ->
  it 'should be false for 5 < 4', ->
    @aGtB_Int.exec(@ctx).should.be.false

  it 'should be false for 5 < 5', ->
    @aEqB_Int.exec(@ctx).should.be.false

  it 'should be true for 5 < 6', ->
    @aLtB_Int.exec(@ctx).should.be.true

describe 'LessOrEqual', ->
  it 'should be false for 5 <= 4', ->
    @aGtB_Int.exec(@ctx).should.be.false

  it 'should be true for 5 <= 5', ->
    @aEqB_Int.exec(@ctx).should.be.true

  it 'should be true for 5 <= 6', ->
    @aLtB_Int.exec(@ctx).should.be.true

describe 'Greater', ->
  it 'should be true for 5 > 4', ->
    @aGtB_Int.exec(@ctx).should.be.true

  it 'should be false for 5 > 5', ->
    @aEqB_Int.exec(@ctx).should.be.false

  it 'should be false for 5 > 6', ->
    @aLtB_Int.exec(@ctx).should.be.false

describe 'GreaterOrEqual', ->
  it 'should be true for 5 >= 4', ->
    @aGtB_Int.exec(@ctx).should.be.true

  it 'should be true for 5 >= 5', ->
    @aEqB_Int.exec(@ctx).should.be.true

  it 'should be false for 5 >= 6', ->
    @aLtB_Int.exec(@ctx).should.be.false

describe 'If', ->
  it "should return the correct value when the expression is true", ->
    @exp.exec(@ctx.withParameters { var: true }).should.equal "true return"

  it "should return the correct value when the expression is false", ->
    @exp.exec(@ctx.withParameters { var: false }).should.equal "false return"


describe  'Case', ->
  it "should be able to execute a standard case statement", ->
    vals =  [{"x" : 1, "y" : 2, "message" : "X < Y"},
      {"x" : 2, "y" : 1, "message" : "X > Y"},
      {"x" : 1, "y" : 1, "message" : "X == Y"}]
    for item in vals
      @ctx.withParameters { X : item.x, Y: item.y }
      @standard.exec(@ctx).should.equal item.message

  it "should be able to execute a selected case statement", ->
    vals = [{"var" : 1, "message" : "one"},
      {"var" : 2, "message" : "two"},
      {"var" : 3, "message" : "?"}]
    for item in vals
      @ctx.withParameters { var : item.var }
      @selected.exec(@ctx).should.equal item.message
{isNull} = require '../../../lib/util/util'

describe 'FromString', ->
  it "should convert 'str' to 'str'", ->
    @stringStr.exec(@ctx).should.equal "str"

  it "should convert null to null", ->
    isNull(@stringNull.exec(@ctx)).should.equal true

  it "should convert 'true' to true", ->
    @boolTrue.exec(@ctx).should.equal true

  it "should convert 'false' to false", ->
    @boolFalse.exec(@ctx).should.equal false

  it "should convert '10.2' to Decimal", ->
    @decimalValid.exec(@ctx).should.equal 10.2

  it "should convert 'abc' to Decimal NaN", ->
    isNaN(@decimalInvalid.exec(@ctx)).should.equal true

  it "should convert '10' to Integer", ->
    @integerValid.exec(@ctx).should.equal 10

  it "should convert '10.2' to Integer 10", ->
    @integerDropDecimal.exec(@ctx).should.equal 10

  it "should convert 'abc' to Integer NaN", ->
    isNaN(@integerInvalid.exec(@ctx)).should.equal true

  it "should convert \"10 'A'\" to Quantity", ->
    quantity = @quantityStr.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal "A"

  it "should convert \"+10 'A'\" to Quantity", ->
    quantity = @posQuantityStr.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal "A"

  it "should convert \"-10 'A'\" to Quantity", ->
    quantity = @negQuantityStr.exec(@ctx)
    quantity.value.should.equal -10
    quantity.unit.should.equal "A"

  it "should convert \"10.0'mA'\" to Quantity", ->
    quantity = @quantityStrDecimal.exec(@ctx)
    quantity.value.should.equal 10.0
    quantity.unit.should.equal "mA"

  it "should convert '2015-01-02' to DateTime", ->
    date = @dateStr.exec(@ctx)
    date.year.should.equal 2015
    date.month.should.equal 1
    date.day.should.equal 2

describe 'FromInteger', ->
  it "should convert 10 to '10'", ->
    @string10.exec(@ctx).should.equal "10"

  it "should convert 10 to 10.0", ->
    @decimal10.exec(@ctx).should.equal 10.0

  it "should convert null to null", ->
    isNull(@intNull.exec(@ctx)).should.equal true

  it "should convert 10 to 10", ->
    @intInt.exec(@ctx).should.equal 10

describe 'FromQuantity', ->
  it "should convert \"10 'A'\" to \"10 'A'\"", ->
    @quantityStr.exec(@ctx).should.equal "10 'A'"

  it "should convert \"+10 'A'\" to \"10 'A'\"", ->
    @posQuantityStr.exec(@ctx).should.equal "10 'A'"

  it "should convert \"-10 'A'\" to \"10 'A'\"", ->
    @negQuantityStr.exec(@ctx).should.equal "-10 'A'"

  it "should convert \"10 'A'\" to \"10 'A'\"", ->
    quantity = @quantityQuantity.exec(@ctx)
    quantity.value.should.equal 10
    quantity.unit.should.equal 'A'

describe 'FromBoolean', ->
  it "should convert true to 'true'", ->
    @booleanTrueStr.exec(@ctx).should.equal "true"

  it "should convert false to 'false'", ->
    @booleanFalseStr.exec(@ctx).should.equal "false"

  it "should convert true to true", ->
    @booleanTrueBool.exec(@ctx).should.equal true

  it "should convert false to false", ->
    @booleanFalseBool.exec(@ctx).should.equal false

describe 'FromDateTime', ->
  it "should convert @2015-01-02 to '2015-01-02'", ->
    @dateStr.exec(@ctx).should.equal "2015-01-02"

  it "should convert @2015-01-02 to @2015-01-02", ->
    date = @dateDate.exec(@ctx)
    date.year.should.equal 2015
    date.month.should.equal 1
    date.day.should.equal 2

describe 'FromTime', ->
  it.skip "should convert @T11:57 to '11:57'", ->
    @timeStr.exec(@ctx).should.equal "11:57"

  it.skip "should convert @T11:57 to @11:57", ->
    time = @timeTime.exec(@ctx)
    time.hour.should.equal 11
    time.minute.should.equal 57

describe 'FromCode', ->
  it.skip "should convert hepB to a concept", ->
    concept = @codeConcept.exec(@ctx)

  it.skip "should convert hepB to a code", ->
    code = @codeCode.exec(@ctx)
{ Uncertainty } = require '../../../lib/datatypes/uncertainty'

describe 'DateTime', ->
    @defaultOffset = (new Date()).getTimezoneOffset() / 60 * -1

  it 'should execute year precision correctly', ->
    d = @year.exec(@ctx)
    d.year.should.equal 2012
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'month', 'day', 'hour', 'minute', 'second', 'millisecond' ]

  it 'should execute month precision correctly', ->
    d = @month.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'day', 'hour', 'minute', 'second', 'millisecond' ]

  it 'should execute day precision correctly', ->
    d = @day.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'hour', 'minute', 'second', 'millisecond' ]

  it 'should execute hour precision correctly', ->
    d = @hour.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'minute', 'second', 'millisecond' ]

  it 'should execute minute precision correctly', ->
    d = @minute.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d[field]) for field in [ 'second', 'millisecond' ]

  it 'should execute second precision correctly', ->
    d = @second.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.timezoneOffset.should.equal @defaultOffset
    should.not.exist(d.millisecond)

  it 'should execute millisecond precision correctly', ->
    d = @millisecond.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.millisecond.should.equal 456
    d.timezoneOffset.should.equal @defaultOffset

  it 'should execute timezone offsets correctly', ->
    d = @timezoneOffset.exec(@ctx)
    d.year.should.equal 2012
    d.month.should.equal 2
    d.day.should.equal 15
    d.hour.should.equal 12
    d.minute.should.equal 10
    d.second.should.equal 59
    d.millisecond.should.equal 456
    d.timezoneOffset.should.equal -8

describe 'Today', ->
  it 'should return only day components and timezone of today', ->
    jsDate = new Date()
    today = @todayVar.exec @ctx
    today.year.should.equal jsDate.getFullYear()
    today.month.should.equal jsDate.getMonth() + 1
    today.day.should.equal jsDate.getDate()
    today.timezoneOffset.should.equal jsDate.getTimezoneOffset() / 60 * -1
    should.not.exist(today[field]) for field in [ 'hour', 'minute', 'second', 'millisecond' ]

describe 'Now', ->
  it 'should return all date components representing now', ->
    jsDate = new Date()
    now = @nowVar.exec @ctx
    now.year.should.equal jsDate.getFullYear()
    now.month.should.equal jsDate.getMonth() + 1
    now.day.should.equal jsDate.getDate()
    now.hour.should.equal jsDate.getHours()
    now.minute.should.exist
    now.second.should.exist
    now.millisecond.should.exist
    now.timezoneOffset.should.equal jsDate.getTimezoneOffset() / 60 * -1

describe 'DateTimeComponentFrom', ->
  it 'should return the year from the date', ->
    @year.exec(@ctx).should.equal 2000

  it 'should return the month from the date', ->
    @month.exec(@ctx).should.equal 3

  it 'should return the day from the date', ->
    @day.exec(@ctx).should.equal 15

  it 'should return the hour from the date', ->
    @hour.exec(@ctx).should.equal 13

  it 'should return the minute from the date', ->
    @minute.exec(@ctx).should.equal 30

  it 'should return the second from the date', ->
    @second.exec(@ctx).should.equal 25

  it 'should return the millisecond from the date', ->
    @millisecond.exec(@ctx).should.equal 200

  it 'should return null for imprecise components', ->
    result = @impreciseComponentTuple.exec(@ctx)
    result.should.eql {
      Year: 2000,
      Month: 3,
      Day: 15,
      Hour: null,
      Minute: null,
      Second: null,
      Millisecond: null
    }

  it 'should return null for null date', ->
    should(@nullDate.exec(@ctx)).be.null

describe 'DateFrom', ->
  it 'should return the date from a fully defined DateTime', ->
    date = @date.exec(@ctx)
    date.year.should.equal 2000
    date.month.should.equal 3
    date.day.should.equal 15
    date.timezoneOffset.should.equal 1
    should.not.exist date.hour
    should.not.exist date.minute
    should.not.exist date.second
    should.not.exist date.millisecond

  it 'should return the defined date components from an imprecise date', ->
    date = @impreciseDate.exec(@ctx)
    date.year.should.equal 2000
    should.not.exist date.month
    should.not.exist date.day
    should.not.exist date.hour
    should.not.exist date.minute
    should.not.exist date.second
    should.not.exist date.millisecond

  it 'should return null for null date', ->
    should(@nullDate.exec(@ctx)).be.null

describe 'TimeFrom', ->
  it 'should return the time from a fully defined DateTime (and date should be lowest expressible date)', ->
    time = @time.exec(@ctx)
    time.year.should.equal 1900
    time.month.should.equal 1
    time.day.should.equal 1
    time.hour.should.equal 13
    time.minute.should.equal 30
    time.second.should.equal 25
    time.millisecond.should.equal 200
    time.timezoneOffset.should.equal 1

  it 'should return the null time components from a date with no time', ->
    noTime = @noTime.exec(@ctx)
    noTime.year.should.equal 1900
    noTime.month.should.equal 1
    noTime.day.should.equal 1
    should.not.exist noTime.hour
    should.not.exist noTime.minute
    should.not.exist noTime.second
    should.not.exist noTime.millisecond

  it 'should return null for null date', ->
    should(@nullDate.exec(@ctx)).be.null

describe 'TimezoneFrom', ->
  it 'should return the timezone from a fully defined DateTime', ->
    @centralEuropean.exec(@ctx).should.equal 1
    @easternStandard.exec(@ctx).should.equal -5

  it 'should return the default timezone when not specified', ->
    @defaultTimezone.exec(@ctx).should.equal (new Date()).getTimezoneOffset() / 60 * -1

  it 'should return null for null date', ->
    should(@nullDate.exec(@ctx)).be.null

describe 'SameAs', ->
  it 'should properly determine when year is the same', ->
    @sameYear.exec(@ctx).should.be.true
    @notSameYear.exec(@ctx).should.be.false

  it 'should properly determine when month is the same', ->
    @sameMonth.exec(@ctx).should.be.true
    @notSameMonth.exec(@ctx).should.be.false
    @sameMonthWrongYear.exec(@ctx).should.be.false

  it 'should properly determine when day is the same', ->
    @sameDay.exec(@ctx).should.be.true
    @notSameDay.exec(@ctx).should.be.false
    @sameDayWrongMonth.exec(@ctx).should.be.false

  it 'should properly determine when hour is the same', ->
    @sameHour.exec(@ctx).should.be.true
    @notSameHour.exec(@ctx).should.be.false
    @sameHourWrongDay.exec(@ctx).should.be.false

  it 'should properly determine when minute is the same', ->
    @sameMinute.exec(@ctx).should.be.true
    @notSameMinute.exec(@ctx).should.be.false
    @sameMinuteWrongHour.exec(@ctx).should.be.false

  it 'should properly determine when second is the same', ->
    @sameSecond.exec(@ctx).should.be.true
    @notSameSecond.exec(@ctx).should.be.false
    @sameSecondWrongMinute.exec(@ctx).should.be.false

  it 'should properly determine when millisecond is the same', ->
    @sameMillisecond.exec(@ctx).should.be.true
    @notSameMillisecond.exec(@ctx).should.be.false
    @sameMillisecondWrongSecond.exec(@ctx).should.be.false

  it 'should properly determine same as using milliseconds', ->
    @same.exec(@ctx).should.be.true
    @notSame.exec(@ctx).should.be.false

  it 'should normalize timezones when determining sameness', ->
    @sameNormalized.exec(@ctx).should.be.true
    @sameHourWrongTimezone.exec(@ctx).should.be.false

  it 'should handle imprecision', ->
    should(@impreciseHour.exec(@ctx)).be.null
    @impreciseHourWrongDay.exec(@ctx).should.be.false

  it 'should return null when either argument is null', ->
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

describe 'SameOrAfter', ->
  it 'should properly determine when year is same or after', ->
    @sameYear.exec(@ctx).should.be.true
    @yearAfter.exec(@ctx).should.be.true
    @yearBefore.exec(@ctx).should.be.false

  it 'should properly determine when month is same or after', ->
    @sameMonth.exec(@ctx).should.be.true
    @monthAfter.exec(@ctx).should.be.true
    @monthBefore.exec(@ctx).should.be.false

  it 'should properly determine when day is same or after', ->
    @sameDay.exec(@ctx).should.be.true
    @dayAfter.exec(@ctx).should.be.true
    @dayBefore.exec(@ctx).should.be.false

  it 'should properly determine when hour is same or after', ->
    @sameHour.exec(@ctx).should.be.true
    @hourAfter.exec(@ctx).should.be.true
    @hourBefore.exec(@ctx).should.be.false

  it 'should properly determine when minute is same or after', ->
    @sameMinute.exec(@ctx).should.be.true
    @minuteAfter.exec(@ctx).should.be.true
    @minuteBefore.exec(@ctx).should.be.false

  it 'should properly determine when second is same or after', ->
    @sameSecond.exec(@ctx).should.be.true
    @secondAfter.exec(@ctx).should.be.true
    @secondBefore.exec(@ctx).should.be.false

  it 'should properly determine when millisecond is same or after', ->
    @sameMillisecond.exec(@ctx).should.be.true
    @millisecondAfter.exec(@ctx).should.be.true
    @millisecondBefore.exec(@ctx).should.be.false

  it 'should properly determine same or after using ms when no precision defined', ->
    @same.exec(@ctx).should.be.true
    @after.exec(@ctx).should.be.true
    @before.exec(@ctx).should.be.false

  it 'should consider precision units above the specified unit', ->
    @sameDayMonthBefore.exec(@ctx).should.be.false
    @dayAfterMonthBefore.exec(@ctx).should.be.false
    @dayBeforeMonthAfter.exec(@ctx).should.be.true

  it 'should handle imprecision', ->
    should(@impreciseDay.exec(@ctx)).be.null
    @impreciseDayMonthAfter.exec(@ctx).should.be.true
    @impreciseDayMonthBefore.exec(@ctx).should.be.false

  it 'should normalize timezones', ->
    @sameHourNormalizeZones.exec(@ctx).should.be.true
    @hourAfterNormalizeZones.exec(@ctx).should.be.true
    @hourBeforeNormalizeZones.exec(@ctx).should.be.false

  it 'should return null when either argument is null', ->
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

describe 'SameOrBefore', ->
  it 'should properly determine when year is same or after', ->
    @sameYear.exec(@ctx).should.be.true
    @yearAfter.exec(@ctx).should.be.false
    @yearBefore.exec(@ctx).should.be.true

  it 'should properly determine when month is same or after', ->
    @sameMonth.exec(@ctx).should.be.true
    @monthAfter.exec(@ctx).should.be.false
    @monthBefore.exec(@ctx).should.be.true

  it 'should properly determine when day is same or after', ->
    @sameDay.exec(@ctx).should.be.true
    @dayAfter.exec(@ctx).should.be.false
    @dayBefore.exec(@ctx).should.be.true

  it 'should properly determine when hour is same or after', ->
    @sameHour.exec(@ctx).should.be.true
    @hourAfter.exec(@ctx).should.be.false
    @hourBefore.exec(@ctx).should.be.true

  it 'should properly determine when minute is same or after', ->
    @sameMinute.exec(@ctx).should.be.true
    @minuteAfter.exec(@ctx).should.be.false
    @minuteBefore.exec(@ctx).should.be.true

  it 'should properly determine when second is same or after', ->
    @sameSecond.exec(@ctx).should.be.true
    @secondAfter.exec(@ctx).should.be.false
    @secondBefore.exec(@ctx).should.be.true

  it 'should properly determine when millisecond is same or after', ->
    @sameMillisecond.exec(@ctx).should.be.true
    @millisecondAfter.exec(@ctx).should.be.false
    @millisecondBefore.exec(@ctx).should.be.true

  it 'should properly determine same or after using ms when no precision defined', ->
    @same.exec(@ctx).should.be.true
    @after.exec(@ctx).should.be.false
    @before.exec(@ctx).should.be.true

  it 'should consider precision units above the specified unit', ->
    @sameDayMonthBefore.exec(@ctx).should.be.true
    @dayAfterMonthBefore.exec(@ctx).should.be.true
    @dayBeforeMonthAfter.exec(@ctx).should.be.false

  it 'should handle imprecision', ->
    should(@impreciseDay.exec(@ctx)).be.null
    @impreciseDayMonthAfter.exec(@ctx).should.be.false
    @impreciseDayMonthBefore.exec(@ctx).should.be.true

  it 'should normalize timezones', ->
    @sameHourNormalizeZones.exec(@ctx).should.be.true
    @hourAfterNormalizeZones.exec(@ctx).should.be.false
    @hourBeforeNormalizeZones.exec(@ctx).should.be.true

  it 'should return null when either argument is null', ->
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

describe 'After', ->
  it 'should properly determine when year is same or after', ->
    @sameYear.exec(@ctx).should.be.false
    @yearAfter.exec(@ctx).should.be.true
    @yearBefore.exec(@ctx).should.be.false

  it 'should properly determine when month is same or after', ->
    @sameMonth.exec(@ctx).should.be.false
    @monthAfter.exec(@ctx).should.be.true
    @monthBefore.exec(@ctx).should.be.false

  it 'should properly determine when day is same or after', ->
    @sameDay.exec(@ctx).should.be.false
    @dayAfter.exec(@ctx).should.be.true
    @dayBefore.exec(@ctx).should.be.false

  it 'should properly determine when hour is same or after', ->
    @sameHour.exec(@ctx).should.be.false
    @hourAfter.exec(@ctx).should.be.true
    @hourBefore.exec(@ctx).should.be.false

  it 'should properly determine when minute is same or after', ->
    @sameMinute.exec(@ctx).should.be.false
    @minuteAfter.exec(@ctx).should.be.true
    @minuteBefore.exec(@ctx).should.be.false

  it 'should properly determine when second is same or after', ->
    @sameSecond.exec(@ctx).should.be.false
    @secondAfter.exec(@ctx).should.be.true
    @secondBefore.exec(@ctx).should.be.false

  it 'should properly determine when millisecond is same or after', ->
    @sameMillisecond.exec(@ctx).should.be.false
    @millisecondAfter.exec(@ctx).should.be.true
    @millisecondBefore.exec(@ctx).should.be.false

  it 'should properly determine same or after using ms when no precision defined', ->
    @same.exec(@ctx).should.be.false
    @after.exec(@ctx).should.be.true
    @before.exec(@ctx).should.be.false

  it 'should handle imprecision', ->
    should(@impreciseDay.exec(@ctx)).be.null
    @impreciseDayMonthAfter.exec(@ctx).should.be.true
    @impreciseDayMonthBefore.exec(@ctx).should.be.false

  it 'should normalize timezones', ->
    @sameHourNormalizeZones.exec(@ctx).should.be.false
    @hourAfterNormalizeZones.exec(@ctx).should.be.true
    @hourBeforeNormalizeZones.exec(@ctx).should.be.false

  it 'should return null when either argument is null', ->
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

describe 'Before', ->
  it 'should properly determine when year is same or after', ->
    @sameYear.exec(@ctx).should.be.false
    @yearAfter.exec(@ctx).should.be.false
    @yearBefore.exec(@ctx).should.be.true

  it 'should properly determine when month is same or after', ->
    @sameMonth.exec(@ctx).should.be.false
    @monthAfter.exec(@ctx).should.be.false
    @monthBefore.exec(@ctx).should.be.true

  it 'should properly determine when day is same or after', ->
    @sameDay.exec(@ctx).should.be.false
    @dayAfter.exec(@ctx).should.be.false
    @dayBefore.exec(@ctx).should.be.true

  it 'should properly determine when hour is same or after', ->
    @sameHour.exec(@ctx).should.be.false
    @hourAfter.exec(@ctx).should.be.false
    @hourBefore.exec(@ctx).should.be.true

  it 'should properly determine when minute is same or after', ->
    @sameMinute.exec(@ctx).should.be.false
    @minuteAfter.exec(@ctx).should.be.false
    @minuteBefore.exec(@ctx).should.be.true

  it 'should properly determine when second is same or after', ->
    @sameSecond.exec(@ctx).should.be.false
    @secondAfter.exec(@ctx).should.be.false
    @secondBefore.exec(@ctx).should.be.true

  it 'should properly determine when millisecond is same or after', ->
    @sameMillisecond.exec(@ctx).should.be.false
    @millisecondAfter.exec(@ctx).should.be.false
    @millisecondBefore.exec(@ctx).should.be.true

  it 'should properly determine same or after using ms when no precision defined', ->
    @same.exec(@ctx).should.be.false
    @after.exec(@ctx).should.be.false
    @before.exec(@ctx).should.be.true

  it 'should handle imprecision', ->
    should(@impreciseDay.exec(@ctx)).be.null
    @impreciseDayMonthAfter.exec(@ctx).should.be.false
    @impreciseDayMonthBefore.exec(@ctx).should.be.true

  it 'should normalize timezones', ->
    @sameHourNormalizeZones.exec(@ctx).should.be.false
    @hourAfterNormalizeZones.exec(@ctx).should.be.false
    @hourBeforeNormalizeZones.exec(@ctx).should.be.true

  it 'should return null when either argument is null', ->
    should(@nullLeft.exec(@ctx)).be.null
    should(@nullRight.exec(@ctx)).be.null
    should(@nullBoth.exec(@ctx)).be.null

describe 'DurationBetween', ->
  it 'should properly execute years between', ->
    @yearsBetween.exec(@ctx).should.equal 1

  it 'should properly execute months between', ->
    @monthsBetween.exec(@ctx).should.equal 12

  it 'should properly execute days between', ->
    @daysBetween.exec(@ctx).should.equal 365

  it 'should properly execute hours between', ->
    @hoursBetween.exec(@ctx).should.equal 24 * 365

  it 'should properly execute minutes between', ->
    @minutesBetween.exec(@ctx).should.equal 60 * 24 * 365

  it 'should properly execute seconds between', ->
    @secondsBetween.exec(@ctx).should.equal 60 * 60 * 24 * 365

  it 'should properly execute milliseconds between', ->
    @millisecondsBetween.exec(@ctx).should.equal 1000 * 60 * 60 * 24 * 365

  it 'should properly execute milliseconds between when date 1 is after date 2', ->
    @millisecondsBetweenReversed.exec(@ctx).should.equal -1 * 1000 * 60 * 60 * 24 * 365

  it 'should properly execute years between with an uncertainty', ->
    @yearsBetweenUncertainty.exec(@ctx).should.equal 0

  it 'should properly execute months between with an uncertainty', ->
    @monthsBetweenUncertainty.exec(@ctx).should.equal 0

  it 'should properly execute days between with an uncertainty', ->
    @daysBetweenUncertainty.exec(@ctx).should.eql new Uncertainty(0, 30)

  it 'should properly execute hours between with an uncertainty', ->
    @hoursBetweenUncertainty.exec(@ctx).should.eql new Uncertainty(0, 743)

  it 'should properly execute minutes between with an uncertainty', ->
    @minutesBetweenUncertainty.exec(@ctx).should.eql new Uncertainty(0, 44639)

  it 'should properly execute seconds between with an uncertainty', ->
    @secondsBetweenUncertainty.exec(@ctx).should.eql new Uncertainty(0, 2678399)

  it 'should properly execute milliseconds between with an uncertainty', ->
    @millisecondsBetweenUncertainty.exec(@ctx).should.eql new Uncertainty(0, 2678399999)

  it 'should properly execute seconds between when date 1 is after date 2 with an uncertainty', ->
    @millisecondsBetweenReversedUncertainty.exec(@ctx).should.eql new Uncertainty(-2678399999, 0)

describe 'DurationBetween Comparisons', ->
  it 'should calculate days between > x', ->
    @greaterThan25DaysAfter.exec(@ctx).should.be.true
    should(@greaterThan40DaysAfter.exec(@ctx)).be.null
    @greaterThan80DaysAfter.exec(@ctx).should.be.false

  it 'should calculate days between >= x', ->
    @greaterOrEqualTo25DaysAfter.exec(@ctx).should.be.true
    should(@greaterOrEqualTo40DaysAfter.exec(@ctx)).be.null
    @greaterOrEqualTo80DaysAfter.exec(@ctx).should.be.false

  it 'should calculate days between = x', ->
    @equalTo25DaysAfter.exec(@ctx).should.be.false
    should(@equalTo40DaysAfter.exec(@ctx)).be.null
    @equalTo80DaysAfter.exec(@ctx).should.be.false

  it 'should calculate days between <= x', ->
    @lessOrEqualTo25DaysAfter.exec(@ctx).should.be.false
    should(@lessOrEqualTo40DaysAfter.exec(@ctx)).be.null
    @lessOrEqualTo80DaysAfter.exec(@ctx).should.be.true

  it 'should calculate days between < x', ->
    @lessThan25DaysAfter.exec(@ctx).should.be.false
    should(@lessThan40DaysAfter.exec(@ctx)).be.null
    @lessThan80DaysAfter.exec(@ctx).should.be.true

  it 'should calculate other way too', ->
    @twentyFiveDaysLessThanDaysBetween.exec(@ctx).should.be.true
    should(@fortyDaysEqualToDaysBetween.exec(@ctx)).be.null
    @twentyFiveDaysGreaterThanDaysBetween.exec(@ctx).should.be.false


{ p1, p2 } = require './patients'

describe 'Age', ->
  , [ p1, p2 ]
    @results = @executor.withLibrary(@lib).exec(@patientSource)

  it 'should have correct patient results', ->
    @results.patientResults['1'].Age.should.equal 32
    @results.patientResults['2'].Age.should.equal 5

  it 'should have the correct population results', ->
    @results.populationResults.AgeSum.should.equal 37

  it 'should be able to reference other population context expressions', ->
    @results.populationResults.AgeSumRef.should.equal 37

vsets = require './valuesets'
{ p1 } = require './patients'

describe 'Retrieve', ->
  , [ p1 ], vsets

  it 'should find conditions', ->
    c = @conditions.exec(@ctx)
    c.should.have.length(2)
    c[0].id().should.equal 'http://cqframework.org/3/2'
    c[1].id().should.equal 'http://cqframework.org/3/4'

  it 'should find encounter performances', ->
    e = @encounters.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal 'http://cqframework.org/3/1'
    e[1].id().should.equal 'http://cqframework.org/3/3'
    e[2].id().should.equal 'http://cqframework.org/3/5'

  it 'should find observations with a value set', ->
    p = @pharyngitisConditions.exec(@ctx)
    p.should.have.length(1)
    p[0].id().should.equal 'http://cqframework.org/3/2'

  it 'should find encounter performances with a value set', ->
    a = @ambulatoryEncounters.exec(@ctx)
    a.should.have.length(3)
    a[0].id().should.equal 'http://cqframework.org/3/1'
    a[1].id().should.equal 'http://cqframework.org/3/3'
    a[2].id().should.equal 'http://cqframework.org/3/5'

  it 'should find encounter performances by service type', ->
    e = @encountersByServiceType.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal 'http://cqframework.org/3/1'
    e[1].id().should.equal 'http://cqframework.org/3/3'
    e[2].id().should.equal 'http://cqframework.org/3/5'

  it 'should not find conditions with wrong valueset', ->
    e = @wrongValueSet.exec(@ctx)
    e.should.be.empty

  it 'should not find encounter performances using wrong codeProperty', ->
    e = @wrongCodeProperty.exec(@ctx)
    e.should.be.empty
{ DateTime } = require '../../../lib/datatypes/datetime'

describe 'Instance', ->
  it 'should create generic json objects with the correct key values', ->
    @quantity.exec(@ctx).unit.should.eql 'a'
    @quantity.exec(@ctx).value.should.eql 12
    @med.exec(@ctx).isBrand.should.eql false
    @med.exec(@ctx).name.should.eql "Best Med Ever"
    @val.exec(@ctx).should.eql 12


{ Interval } = require '../../../lib/datatypes/interval'
{ DateTime } = require '../../../lib/datatypes/datetime'

describe 'Interval', ->
  it 'should properly represent an open interval', ->
    @open.lowClosed.should.be.false
    @open.highClosed.should.be.false
    @open.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @open.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)

  it 'should properly represent a left-open interval', ->
    @leftOpen.lowClosed.should.be.false
    @leftOpen.highClosed.should.be.true
    @leftOpen.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @leftOpen.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)

  it 'should properly represent a right-open interval', ->
    @rightOpen.lowClosed.should.be.true
    @rightOpen.highClosed.should.be.false
    @rightOpen.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @rightOpen.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)

  it 'should properly represent a closed interval', ->
    @closed.lowClosed.should.be.true
    @closed.highClosed.should.be.true
    @closed.low.exec(@ctx).should.eql new DateTime(2012, 1, 1)
    @closed.high.exec(@ctx).should.eql new DateTime(2013, 1, 1)

  it 'should exec to native Interval datatype', ->
    ivl = @open.exec(@cql)
    ivl.should.be.instanceOf Interval
    ivl.lowClosed.should.equal @open.lowClosed
    ivl.highClosed.should.equal @open.highClosed
    ivl.low.should.eql new DateTime(2012, 1, 1)
    ivl.high.should.eql new DateTime(2013, 1, 1)

describe 'Equal', ->
  it 'should determine equal integer intervals', ->
    @equalClosed.exec(@ctx).should.be.true
    @equalOpen.exec(@ctx).should.be.true
    @equalOpenClosed.exec(@ctx).should.be.true

  it 'should determine unequal integer intervals', ->
    @unequalClosed.exec(@ctx).should.be.false
    @unequalOpen.exec(@ctx).should.be.false
    @unequalClosedOpen.exec(@ctx).should.be.false

  it 'should determine equal datetime intervals', ->
    @equalDates.exec(@ctx).should.be.true
    @equalDatesOpenClosed.exec(@ctx).should.be.true

  it 'should operate correctly with imprecision', ->
    should(@sameDays.exec(@ctx)).be.null
    @differentDays.exec(@ctx).should.be.false

describe 'NotEqual', ->
  it 'should determine equal integer intervals', ->
    @equalClosed.exec(@ctx).should.be.false
    @equalOpen.exec(@ctx).should.be.false
    @equalOpenClosed.exec(@ctx).should.be.false

  it 'should determine unequal integer intervals', ->
    @unequalClosed.exec(@ctx).should.be.true
    @unequalOpen.exec(@ctx).should.be.true
    @unequalClosedOpen.exec(@ctx).should.be.true

  it 'should determine equal datetime intervals', ->
    @equalDates.exec(@ctx).should.be.false
    @equalDatesOpenClosed.exec(@ctx).should.be.false

  it 'should operate correctly with imprecision', ->
    should(@sameDays.exec(@ctx)).be.null
    @differentDays.exec(@ctx).should.be.true

describe 'Contains', ->
  it 'should accept contained items', ->
    @containsInt.exec(@ctx).should.be.true
    @containsReal.exec(@ctx).should.be.true
    @containsDate.exec(@ctx).should.be.true

  it 'should reject uncontained items', ->
    @notContainsInt.exec(@ctx).should.be.false
    @notContainsReal.exec(@ctx).should.be.false
    @notContainsDate.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegContainsInt.exec(@ctx).should.be.true
    @negInfBegNotContainsInt.exec(@ctx).should.be.false
    @unknownBegContainsInt.exec(@ctx).should.be.true
    should(@unknownBegMayContainInt.exec(@ctx)).be.null
    @unknownBegNotContainsInt.exec(@ctx).should.be.false
    @posInfEndContainsInt.exec(@ctx).should.be.true
    @posInfEndNotContainsInt.exec(@ctx).should.be.false
    @unknownEndContainsInt.exec(@ctx).should.be.true
    should(@unknownEndMayContainInt.exec(@ctx)).be.null
    @unknownEndNotContainsInt.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegContainsDate.exec(@ctx).should.be.true
    @negInfBegNotContainsDate.exec(@ctx).should.be.false
    @unknownBegContainsDate.exec(@ctx).should.be.true
    should(@unknownBegMayContainDate.exec(@ctx)).be.null
    @unknownBegNotContainsDate.exec(@ctx).should.be.false
    @posInfEndContainsDate.exec(@ctx).should.be.true
    @posInfEndNotContainsDate.exec(@ctx).should.be.false
    @unknownEndContainsDate.exec(@ctx).should.be.true
    should(@unknownEndMayContainDate.exec(@ctx)).be.null
    @unknownEndNotContainsDate.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @containsImpreciseDate.exec(@ctx).should.be.true
    @notContainsImpreciseDate.exec(@ctx).should.be.false
    should(@mayContainImpreciseDate.exec(@ctx)).be.null
    @impreciseContainsDate.exec(@ctx).should.be.true
    @impreciseNotContainsDate.exec(@ctx).should.be.false
    should(@impreciseMayContainDate.exec(@ctx)).be.null

describe 'In', ->
  it 'should accept contained items', ->
    @containsInt.exec(@ctx).should.be.true
    @containsReal.exec(@ctx).should.be.true
    @containsDate.exec(@ctx).should.be.true

  it 'should reject uncontained items', ->
    @notContainsInt.exec(@ctx).should.be.false
    @notContainsReal.exec(@ctx).should.be.false
    @notContainsDate.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegContainsInt.exec(@ctx).should.be.true
    @negInfBegNotContainsInt.exec(@ctx).should.be.false
    @unknownBegContainsInt.exec(@ctx).should.be.true
    should(@unknownBegMayContainInt.exec(@ctx)).be.null
    @unknownBegNotContainsInt.exec(@ctx).should.be.false
    @posInfEndContainsInt.exec(@ctx).should.be.true
    @posInfEndNotContainsInt.exec(@ctx).should.be.false
    @unknownEndContainsInt.exec(@ctx).should.be.true
    should(@unknownEndMayContainInt.exec(@ctx)).be.null
    @unknownEndNotContainsInt.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegContainsDate.exec(@ctx).should.be.true
    @negInfBegNotContainsDate.exec(@ctx).should.be.false
    @unknownBegContainsDate.exec(@ctx).should.be.true
    should(@unknownBegMayContainDate.exec(@ctx)).be.null
    @unknownBegNotContainsDate.exec(@ctx).should.be.false
    @posInfEndContainsDate.exec(@ctx).should.be.true
    @posInfEndNotContainsDate.exec(@ctx).should.be.false
    @unknownEndContainsDate.exec(@ctx).should.be.true
    should(@unknownEndMayContainDate.exec(@ctx)).be.null
    @unknownEndNotContainsDate.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @containsImpreciseDate.exec(@ctx).should.be.true
    @notContainsImpreciseDate.exec(@ctx).should.be.false
    should(@mayContainImpreciseDate.exec(@ctx)).be.null
    @impreciseContainsDate.exec(@ctx).should.be.true
    @impreciseNotContainsDate.exec(@ctx).should.be.false
    should(@impreciseMayContainDate.exec(@ctx)).be.null

describe 'Includes', ->
  it 'should accept included items', ->
    @includesIntIvl.exec(@ctx).should.be.true
    @includesRealIvl.exec(@ctx).should.be.true
    @includesDateIvl.exec(@ctx).should.be.true

  it 'should reject unincluded items', ->
    @notIncludesIntIvl.exec(@ctx).should.be.false
    @notIncludesRealIvl.exec(@ctx).should.be.false
    @notIncludesDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegIncludesIntIvl.exec(@ctx).should.be.true
    @negInfBegNotIncludesIntIvl.exec(@ctx).should.be.false
    @unknownBegIncludesIntIvl.exec(@ctx).should.be.true
    should(@unknownBegMayIncludeIntIvl.exec(@ctx)).be.null
    @unknownBegNotIncludesIntIvl.exec(@ctx).should.be.false
    @posInfEndIncludesIntIvl.exec(@ctx).should.be.true
    @posInfEndNotIncludesIntIvl.exec(@ctx).should.be.false
    @unknownEndIncludesIntIvl.exec(@ctx).should.be.true
    should(@unknownEndMayIncludeIntIvl.exec(@ctx)).be.null
    @unknownEndNotIncludesIntIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegIncludesDateIvl.exec(@ctx).should.be.true
    @negInfBegNotIncludesDateIvl.exec(@ctx).should.be.false
    @unknownBegIncludesDateIvl.exec(@ctx).should.be.true
    should(@unknownBegMayIncludeDateIvl.exec(@ctx)).be.null
    @unknownBegNotIncludesDateIvl.exec(@ctx).should.be.false
    @posInfEndIncludesDateIvl.exec(@ctx).should.be.true
    @posInfEndNotIncludesDateIvl.exec(@ctx).should.be.false
    @unknownEndIncludesDateIvl.exec(@ctx).should.be.true
    should(@unknownEndMayIncludeDateIvl.exec(@ctx)).be.null
    @unknownEndNotIncludesDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @includesImpreciseDateIvl.exec(@ctx).should.be.true
    @notIncludesImpreciseDateIvl.exec(@ctx).should.be.false
    should(@mayIncludeImpreciseDateIvl.exec(@ctx)).be.null
    @impreciseIncludesDateIvl.exec(@ctx).should.be.true
    @impreciseNotIncludesDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayIncludeDateIvl.exec(@ctx)).be.null

describe 'ProperlyIncludes', ->
  it 'should accept properly included intervals', ->
    @properlyIncludesIntIvl.exec(@ctx).should.be.true
    @properlyIncludesIntBeginsIvl.exec(@ctx).should.be.true
    @properlyIncludesIntEndsIvl.exec(@ctx).should.be.true
    @properlyIncludesRealIvl.exec(@ctx).should.be.true
    @properlyIncludesDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals not properly included', ->
    @notProperlyIncludesIntIvl.exec(@ctx).should.be.false
    @notProperlyIncludesRealIvl.exec(@ctx).should.be.false
    @notProperlyIncludesDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @posInfEndProperlyIncludesIntIvl.exec(@ctx).should.be.true
    @posInfEndNotProperlyIncludesIntIvl.exec(@ctx).should.be.false
    should(@unknownEndMayProperlyIncludeIntIvl.exec(@ctx)).be.null

describe 'IncludedIn', ->
  it 'should accept included items', ->
    @includesIntIvl.exec(@ctx).should.be.true
    @includesRealIvl.exec(@ctx).should.be.true
    @includesDateIvl.exec(@ctx).should.be.true

  it 'should reject unincluded items', ->
    @notIncludesIntIvl.exec(@ctx).should.be.false
    @notIncludesRealIvl.exec(@ctx).should.be.false
    @notIncludesDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegIncludedInIntIvl.exec(@ctx).should.be.true
    @negInfBegNotIncludedInIntIvl.exec(@ctx).should.be.false
    @unknownBegIncludedInIntIvl.exec(@ctx).should.be.true
    should(@unknownBegMayBeIncludedInIntIvl.exec(@ctx)).be.null
    @unknownBegNotIncludedInIntIvl.exec(@ctx).should.be.false
    @posInfEndIncludedInIntIvl.exec(@ctx).should.be.true
    @posInfEndNotIncludedInIntIvl.exec(@ctx).should.be.false
    @unknownEndIncludedInIntIvl.exec(@ctx).should.be.true
    should(@unknownEndMayBeIncludedInIntIvl.exec(@ctx)).be.null
    @unknownEndNotIncludedInIntIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegIncludedInDateIvl.exec(@ctx).should.be.true
    @negInfBegNotIncludedInDateIvl.exec(@ctx).should.be.false
    @unknownBegIncludedInDateIvl.exec(@ctx).should.be.true
    should(@unknownBegMayBeIncludedInDateIvl.exec(@ctx)).be.null
    @unknownBegNotIncludedInDateIvl.exec(@ctx).should.be.false
    @posInfEndIncludedInDateIvl.exec(@ctx).should.be.true
    @posInfEndNotIncludedInDateIvl.exec(@ctx).should.be.false
    @unknownEndIncludedInDateIvl.exec(@ctx).should.be.true
    should(@unknownEndMayBeIncludedInDateIvl.exec(@ctx)).be.null
    @unknownEndNotIncludedInDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @includesImpreciseDateIvl.exec(@ctx).should.be.true
    @notIncludesImpreciseDateIvl.exec(@ctx).should.be.false
    should(@mayIncludeImpreciseDateIvl.exec(@ctx)).be.null
    @impreciseIncludesDateIvl.exec(@ctx).should.be.true
    @impreciseNotIncludesDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayIncludeDateIvl.exec(@ctx)).be.null

describe 'ProperlyIncludedIn', ->
  it 'should accept properly included intervals', ->
    @properlyIncludesIntIvl.exec(@ctx).should.be.true
    @properlyIncludesIntBeginsIvl.exec(@ctx).should.be.true
    @properlyIncludesIntEndsIvl.exec(@ctx).should.be.true
    @properlyIncludesRealIvl.exec(@ctx).should.be.true
    @properlyIncludesDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals not properly included', ->
    @notProperlyIncludesIntIvl.exec(@ctx).should.be.false
    @notProperlyIncludesRealIvl.exec(@ctx).should.be.false
    @notProperlyIncludesDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @posInfEndProperlyIncludedInDateIvl.exec(@ctx).should.be.true
    @posInfEndNotProperlyIncludedInDateIvl.exec(@ctx).should.be.false
    should(@unknownEndMayBeProperlyIncludedInDateIvl.exec(@ctx)).be.null

describe 'After', ->
  it 'should accept intervals before it', ->
    @afterIntIvl.exec(@ctx).should.be.true
    @afterRealIvl.exec(@ctx).should.be.true
    @afterDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals on or after it', ->
    @notAfterIntIvl.exec(@ctx).should.be.false
    @notAfterRealIvl.exec(@ctx).should.be.false
    @notAfterDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegNotAfterIntIvl.exec(@ctx).should.be.false
    should(@unknownBegMayBeAfterIntIvl.exec(@ctx)).be.null
    @unknownBegNotAfterIntIvl.exec(@ctx).should.be.false
    @posInfEndAfterIntIvl.exec(@ctx).should.be.true
    @posInfEndNotAfterIntIvl.exec(@ctx).should.be.false
    @unknownEndAfterIntIvl.exec(@ctx).should.be.true
    @unknownEndNotAfterIntIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegNotAfterDateIvl.exec(@ctx).should.be.false
    should(@unknownBegMayBeAfterDateIvl.exec(@ctx)).be.null
    @unknownBegNotAfterDateIvl.exec(@ctx).should.be.false
    @posInfEndAfterDateIvl.exec(@ctx).should.be.true
    @posInfEndNotAfterDateIvl.exec(@ctx).should.be.false
    @unknownEndAfterDateIvl.exec(@ctx).should.be.true
    @unknownEndNotAfterDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @afterImpreciseDateIvl.exec(@ctx).should.be.true
    @notAfterImpreciseDateIvl.exec(@ctx).should.be.false
    should(@mayBeAfterImpreciseDateIvl.exec(@ctx)).be.null
    @impreciseAfterDateIvl.exec(@ctx).should.be.true
    @impreciseNotAfterDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayBeAfterDateIvl.exec(@ctx)).be.null

describe 'Before', ->
  it 'should accept intervals before it', ->
    @beforeIntIvl.exec(@ctx).should.be.true
    @beforeRealIvl.exec(@ctx).should.be.true
    @beforeDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals on or after it', ->
    @notBeforeIntIvl.exec(@ctx).should.be.false
    @notBeforeRealIvl.exec(@ctx).should.be.false
    @notBeforeDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegBeforeIntIvl.exec(@ctx).should.be.true
    @negInfBegNotBeforeIntIvl.exec(@ctx).should.be.false
    @unknownBegBeforeIntIvl.exec(@ctx).should.be.true
    @unknownBegNotBeforeIntIvl.exec(@ctx).should.be.false
    @posInfEndNotBeforeIntIvl.exec(@ctx).should.be.false
    should(@unknownEndMayBeBeforeIntIvl.exec(@ctx)).be.null
    @unknownEndNotBeforeIntIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegBeforeDateIvl.exec(@ctx).should.be.true
    @negInfBegNotBeforeDateIvl.exec(@ctx).should.be.false
    @unknownBegBeforeDateIvl.exec(@ctx).should.be.true
    @unknownBegNotBeforeDateIvl.exec(@ctx).should.be.false
    @posInfEndNotBeforeDateIvl.exec(@ctx).should.be.false
    should(@unknownEndMayBeBeforeDateIvl.exec(@ctx)).be.null
    @unknownEndNotBeforeDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @beforeImpreciseDateIvl.exec(@ctx).should.be.true
    @notBeforeImpreciseDateIvl.exec(@ctx).should.be.false
    should(@mayBeBeforeImpreciseDateIvl.exec(@ctx)).be.null
    @impreciseBeforeDateIvl.exec(@ctx).should.be.true
    @impreciseNotBeforeDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayBeBeforeDateIvl.exec(@ctx)).be.null

describe 'Meets', ->
  it 'should accept intervals meeting after it', ->
    @meetsBeforeIntIvl.exec(@ctx).should.be.true
    @meetsBeforeRealIvl.exec(@ctx).should.be.true
    @meetsBeforeDateIvl.exec(@ctx).should.be.true

  it 'should accept intervals meeting before it', ->
    @meetsAfterIntIvl.exec(@ctx).should.be.true
    @meetsAfterRealIvl.exec(@ctx).should.be.true
    @meetsAfterDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals not meeting it', ->
    @notMeetsIntIvl.exec(@ctx).should.be.false
    @notMeetsRealIvl.exec(@ctx).should.be.false
    @notMeetsDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegMeetsBeforeIntIvl.exec(@ctx).should.be.true
    @negInfBegNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeIntIvl.exec(@ctx).should.be.true
    should(@unknownBegMayMeetAfterIntIvl.exec(@ctx)).be.null
    @unknownBegNotMeetsIntIvl.exec(@ctx).should.be.false
    should(@intIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    @posInfEndMeetsAfterIntIvl.exec(@ctx).should.be.true
    @posInfEndNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterIntIvl.exec(@ctx).should.be.true
    should(@unknownEndMayMeetBeforeIntIvl.exec(@ctx)).be.null
    @unknownEndNotMeetsIntIvl.exec(@ctx).should.be.false
    should(@intIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegMeetsBeforeDateIvl.exec(@ctx).should.be.true
    @negInfBegNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeDateIvl.exec(@ctx).should.be.true
    should(@unknownBegMayMeetAfterDateIvl.exec(@ctx)).be.null
    @unknownBegNotMeetsDateIvl.exec(@ctx).should.be.false
    should(@dateIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    @posInfEndMeetsAfterDateIvl.exec(@ctx).should.be.true
    @posInfEndNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterDateIvl.exec(@ctx).should.be.true
    should(@unknownEndMayMeetBeforeDateIvl.exec(@ctx)).be.null
    @unknownEndNotMeetsDateIvl.exec(@ctx).should.be.false
    should(@dateIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null

  it 'should correctly handle imprecision', ->
    should(@mayMeetAfterImpreciseDateIvl.exec(@ctx)).be.null
    should(@mayMeetBeforeImpreciseDateIvl.exec(@ctx)).be.null
    @notMeetsImpreciseDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayMeetAfterDateIvl.exec(@ctx)).be.null
    should(@impreciseMayMeetBeforeDateIvl.exec(@ctx)).be.null
    @impreciseNotMeetsDateIvl.exec(@ctx).should.be.false

describe 'MeetsAfter', ->
  it 'should accept intervals meeting before it', ->
    @meetsAfterIntIvl.exec(@ctx).should.be.true
    @meetsAfterRealIvl.exec(@ctx).should.be.true
    @meetsAfterDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals meeting after it', ->
    @meetsBeforeIntIvl.exec(@ctx).should.be.false
    @meetsBeforeRealIvl.exec(@ctx).should.be.false
    @meetsBeforeDateIvl.exec(@ctx).should.be.false

  it 'should reject intervals not meeting it', ->
    @notMeetsIntIvl.exec(@ctx).should.be.false
    @notMeetsRealIvl.exec(@ctx).should.be.false
    @notMeetsDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegMeetsBeforeIntIvl.exec(@ctx).should.be.false
    @negInfBegNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeIntIvl.exec(@ctx).should.be.false
    should(@unknownBegMayMeetAfterIntIvl.exec(@ctx)).be.null
    @unknownBegNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlMayMeetBeforeUnknownBeg.exec(@ctx).should.be.false
    @posInfEndMeetsAfterIntIvl.exec(@ctx).should.be.true
    @posInfEndNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterIntIvl.exec(@ctx).should.be.true
    @unknownEndMayMeetBeforeIntIvl.exec(@ctx).should.be.false
    @unknownEndNotMeetsIntIvl.exec(@ctx).should.be.false
    should(@intIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegMeetsBeforeDateIvl.exec(@ctx).should.be.false
    @negInfBegNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeDateIvl.exec(@ctx).should.be.false
    should(@unknownBegMayMeetAfterDateIvl.exec(@ctx)).be.null
    @unknownBegNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlMayMeetBeforeUnknownBeg.exec(@ctx).should.be.false
    @posInfEndMeetsAfterDateIvl.exec(@ctx).should.be.true
    @posInfEndNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterDateIvl.exec(@ctx).should.be.true
    @unknownEndMayMeetBeforeDateIvl.exec(@ctx).should.be.false
    @unknownEndNotMeetsDateIvl.exec(@ctx).should.be.false
    should(@dateIvlMayMeetAfterUnknownEnd.exec(@ctx)).be.null

  it 'should correctly handle imprecision', ->
    should(@mayMeetAfterImpreciseDateIvl.exec(@ctx)).be.null
    @mayMeetBeforeImpreciseDateIvl.exec(@ctx).should.be.false
    @notMeetsImpreciseDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayMeetAfterDateIvl.exec(@ctx)).be.null
    @impreciseMayMeetBeforeDateIvl.exec(@ctx).should.be.false
    @impreciseNotMeetsDateIvl.exec(@ctx).should.be.false

describe 'MeetsBefore', ->
  it 'should accept intervals meeting after it', ->
    @meetsBeforeIntIvl.exec(@ctx).should.be.true
    @meetsBeforeRealIvl.exec(@ctx).should.be.true
    @meetsBeforeDateIvl.exec(@ctx).should.be.true

  it 'should reject intervals meeting before it', ->
    @meetsAfterIntIvl.exec(@ctx).should.be.false
    @meetsAfterRealIvl.exec(@ctx).should.be.false
    @meetsAfterDateIvl.exec(@ctx).should.be.false

  it 'should reject intervals not meeting it', ->
    @notMeetsIntIvl.exec(@ctx).should.be.false
    @notMeetsRealIvl.exec(@ctx).should.be.false
    @notMeetsDateIvl.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (int)', ->
    @negInfBegMeetsBeforeIntIvl.exec(@ctx).should.be.true
    @negInfBegNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeIntIvl.exec(@ctx).should.be.true
    @unknownBegMayMeetAfterIntIvl.exec(@ctx).should.be.false
    @unknownBegNotMeetsIntIvl.exec(@ctx).should.be.false
    should(@intIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    @posInfEndMeetsAfterIntIvl.exec(@ctx).should.be.false
    @posInfEndNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterIntIvl.exec(@ctx).should.be.false
    should(@unknownEndMayMeetBeforeIntIvl.exec(@ctx)).be.null
    @unknownEndNotMeetsIntIvl.exec(@ctx).should.be.false
    @intIvlMayMeetAfterUnknownEnd.exec(@ctx).should.be.false

  it 'should correctly handle null endpoints (date)', ->
    @negInfBegMeetsBeforeDateIvl.exec(@ctx).should.be.true
    @negInfBegNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsNegInfBeg.exec(@ctx).should.be.false
    @unknownBegMeetsBeforeDateIvl.exec(@ctx).should.be.true
    @unknownBegMayMeetAfterDateIvl.exec(@ctx).should.be.false
    @unknownBegNotMeetsDateIvl.exec(@ctx).should.be.false
    should(@dateIvlMayMeetBeforeUnknownBeg.exec(@ctx)).be.null
    @posInfEndMeetsAfterDateIvl.exec(@ctx).should.be.false
    @posInfEndNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlNotMeetsPosInfEnd.exec(@ctx).should.be.false
    @unknownEndMeetsAfterDateIvl.exec(@ctx).should.be.false
    should(@unknownEndMayMeetBeforeDateIvl.exec(@ctx)).be.null
    @unknownEndNotMeetsDateIvl.exec(@ctx).should.be.false
    @dateIvlMayMeetAfterUnknownEnd.exec(@ctx).should.be.false

  it 'should correctly handle imprecision', ->
    @mayMeetAfterImpreciseDateIvl.exec(@ctx).should.be.false
    should(@mayMeetBeforeImpreciseDateIvl.exec(@ctx)).be.null
    @notMeetsImpreciseDateIvl.exec(@ctx).should.be.false
    @impreciseMayMeetAfterDateIvl.exec(@ctx).should.be.false
    should(@impreciseMayMeetBeforeDateIvl.exec(@ctx)).be.null
    @impreciseNotMeetsDateIvl.exec(@ctx).should.be.false

describe 'Overlaps', ->
  it 'should accept overlaps (integer)', ->
    @overlapsBeforeIntIvl.exec(@ctx).should.be.true
    @overlapsAfterIntIvl.exec(@ctx).should.be.true
    @overlapsBoundaryIntIvl.exec(@ctx).should.be.true

  it 'should accept overlaps (real)', ->
    @overlapsBeforeRealIvl.exec(@ctx).should.be.true
    @overlapsAfterRealIvl.exec(@ctx).should.be.true
    @overlapsBoundaryRealIvl.exec(@ctx).should.be.true

  it 'should reject non-overlaps (integer)', ->
    @noOverlapsIntIvl.exec(@ctx).should.be.false

  it 'should reject non-overlaps (real)', ->
    @noOverlapsRealIvl.exec(@ctx).should.be.false

describe 'OverlapsDateTime', ->
  it 'should accept overlaps', ->
    @overlapsBefore.exec(@ctx).should.be.true
    @overlapsAfter.exec(@ctx).should.be.true
    @overlapsContained.exec(@ctx).should.be.true
    @overlapsContains.exec(@ctx).should.be.true

  it 'should accept imprecise overlaps', ->
    @impreciseOverlap.exec(@ctx).should.be.true

  it 'should reject non-overlaps', ->
    @noOverlap.exec(@ctx).should.be.false

  it 'should reject imprecise non-overlaps', ->
    @noImpreciseOverlap.exec(@ctx).should.be.false

  it 'should return null for imprecise overlaps that are unknown', ->
    should(@unknownOverlap.exec(@ctx)).be.null

describe 'OverlapsAfter', ->
  it 'should accept overlaps that are after (integer)', ->
    @overlapsAfterIntIvl.exec(@ctx).should.be.true
    @overlapsBoundaryIntIvl.exec(@ctx).should.be.true

  it 'should accept overlaps that are after (real)', ->
    @overlapsAfterRealIvl.exec(@ctx).should.be.true
    @overlapsBoundaryRealIvl.exec(@ctx).should.be.true

  it 'should reject overlaps that are before (integer)', ->
    @overlapsBeforeIntIvl.exec(@ctx).should.be.false

  it 'should reject overlaps that are before (real)', ->
    @overlapsBeforeRealIvl.exec(@ctx).should.be.false

  it 'should reject non-overlaps (integer)', ->
    @noOverlapsIntIvl.exec(@ctx).should.be.false

  it 'should reject non-overlaps (real)', ->
    @noOverlapsRealIvl.exec(@ctx).should.be.false

describe 'OverlapsAfterDateTime', ->
  it 'should accept overlaps that are after', ->
    @overlapsAfter.exec(@ctx).should.be.true
    @overlapsContains.exec(@ctx).should.be.true

  it 'should accept imprecise overlaps that are after', ->
    @impreciseOverlapAfter.exec(@ctx).should.be.true

  it 'should reject overlaps that are not before', ->
    @overlapsBefore.exec(@ctx).should.be.false
    @overlapsContained.exec(@ctx).should.be.false

  it 'should reject imprecise overlaps that are not before', ->
    @impreciseOverlapBefore.exec(@ctx).should.be.false

  it 'should reject non-overlaps', ->
    @noOverlap.exec(@ctx).should.be.false

  it 'should reject imprecise non-overlaps', ->
    @noImpreciseOverlap.exec(@ctx).should.be.false

  it 'should return null for imprecise overlaps that are unknown', ->
    should(@unknownOverlap.exec(@ctx)).be.null

describe 'OverlapsBefore', ->
  it 'should accept overlaps that are before (integer)', ->
    @overlapsBeforeIntIvl.exec(@ctx).should.be.true
    @overlapsBoundaryIntIvl.exec(@ctx).should.be.true

  it 'should accept overlaps that are before (real)', ->
    @overlapsBeforeRealIvl.exec(@ctx).should.be.true
    @overlapsBoundaryRealIvl.exec(@ctx).should.be.true

  it 'should reject overlaps that are after (integer)', ->
    @overlapsAfterIntIvl.exec(@ctx).should.be.false

  it 'should reject overlaps that are after (real)', ->
    @overlapsAfterRealIvl.exec(@ctx).should.be.false

  it 'should reject non-overlaps (integer)', ->
    @noOverlapsIntIvl.exec(@ctx).should.be.false

  it 'should reject non-overlaps (real)', ->
    @noOverlapsRealIvl.exec(@ctx).should.be.false

describe 'OverlapsBeforeDateTime', ->
  it 'should accept overlaps that are before', ->
    @overlapsBefore.exec(@ctx).should.be.true
    @overlapsContains.exec(@ctx).should.be.true

  it 'should accept imprecise overlaps that are before', ->
    @impreciseOverlapBefore.exec(@ctx).should.be.true

  it 'should reject overlaps that are not before', ->
    @overlapsAfter.exec(@ctx).should.be.false
    @overlapsContained.exec(@ctx).should.be.false

  it 'should reject imprecise overlaps that are not before', ->
    @impreciseOverlapAfter.exec(@ctx).should.be.false

  it 'should reject non-overlaps', ->
    @noOverlap.exec(@ctx).should.be.false

  it 'should reject imprecise non-overlaps', ->
    @noImpreciseOverlap.exec(@ctx).should.be.false

  it 'should return null for imprecise overlaps that are unknown', ->
    should(@unknownOverlap.exec(@ctx)).be.null

describe 'Width', ->
  it 'should calculate the width of integer intervals', ->
    @intWidth.exec(@ctx).should.equal 7
    @intOpenWidth.exec(@ctx).should.equal 5

  it 'should calculate the width of real intervals', ->
    @realWidth.exec(@ctx).should.equal 3.33
    @realOpenWidth.exec(@ctx).should.equal 3.32999998

  it 'should calculate the width of infinite intervals', ->
    @intWidthThreeToMax.exec(@ctx).should.equal Math.pow(2,31)-4
    @intWidthMinToThree.exec(@ctx).should.equal Math.pow(2,31)+3

  it 'should calculate the width of infinite intervals', ->
    should(@intWidthThreeToUnknown.exec(@ctx)).be.null
    should(@intWidthUnknownToThree.exec(@ctx)).be.null

describe 'Start', ->
  it 'should execute as the start of the interval', ->
    @foo.exec(@ctx).should.eql new DateTime(2012, 1, 1)

describe 'End', ->
  it 'should execute as the end of the interval', ->
    @foo.exec(@ctx).should.eql new DateTime(2013, 1, 1)

describe 'IntegerIntervalUnion', ->
  it 'should properly calculate open and closed unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intClosedUnionClosed.exec(@ctx)
    y.equals(x).should.be.true

    y = @intClosedUnionOpen.exec(@ctx)
    y.contains(0).should.be.true
    y.contains(10).should.be.false

    y = @intOpenUnionOpen.exec(@ctx)
    y.contains(0).should.be.false
    y.contains(10).should.be.false

    y = @intOpenUnionClosed.exec(@ctx)
    y.contains(0).should.be.false
    y.contains(10).should.be.true

  it 'should properly calculate sameAs unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intSameAsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate before/after unions', ->
    should(@intBeforeUnion.exec(@ctx)).be.null

  it 'should properly calculate meets unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intMeetsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intOverlapsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intBeginsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate includes/included by unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intDuringUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate ends/ended by unions', ->
    x = @intFullInterval.exec(@ctx)
    y = @intEndsUnion.exec(@ctx)
    y.equals(x).should.be.true

# TODO
# it 'should properly handle imprecision', ->

describe 'DateTimeIntervalUnion', ->
  it 'should properly calculate open and closed unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeClosedUnionClosed.exec(@ctx)
    y.equals(x).should.be.true

    a = new DateTime(2012, 1, 1, 0, 0, 0, 0)
    b = new DateTime(2013, 1, 1, 0, 0, 0, 0)

    y = @dateTimeClosedUnionOpen.exec(@ctx)
    y.contains(a).should.be.true
    y.contains(b).should.be.false

    y = @dateTimeOpenUnionOpen.exec(@ctx)
    y.contains(a).should.be.false
    y.contains(b).should.be.false

    y = @dateTimeOpenUnionClosed.exec(@ctx)
    y.contains(a).should.be.false
    y.contains(b).should.be.true

  it 'should properly calculate sameAs unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeSameAsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate before/after unions', ->
    should(@dateTimeBeforeUnion.exec(@ctx)).be.null

  it 'should properly calculate meets unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeMeetsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeOverlapsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeBeginsUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate includes/included by unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeDuringUnion.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate ends/ended by unions', ->
    x = @dateTimeFullInterval.exec(@ctx)
    y = @dateTimeEndsUnion.exec(@ctx)
    y.equals(x).should.be.true

# TODO
# it 'should properly handle imprecision', ->

describe 'IntegerIntervalExcept', ->
   it 'should properly calculate sameAs except', ->
    should(@intSameAsExcept.exec(@ctx)).be.null

  it 'should properly calculate before/after except', ->
    @intBeforeExcept.exec(@ctx).should.eql new Interval(0,4)

  it 'should properly calculate meets except', ->
    x = @intHalfInterval.exec(@ctx)
    y = @intMeetsExcept.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping except', ->
    x = @intHalfInterval.exec(@ctx)
    y = @intOverlapsExcept.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by except', ->
    should(@intBeginsExcept.exec(@ctx)).be.null

  it 'should properly calculate includes/included by except', ->
    should(@intDuringExcept.exec(@ctx)).be.null

  it 'should properly calculate ends/ended by except', ->
    should(@intEndsExcept.exec(@ctx)).be.null

# TODO
# it 'should properly handle imprecision', ->

describe 'DateTimeIntervalExcept', ->
  it 'should properly calculate sameAs except', ->
    should(@dateTimeSameAsExcept.exec(@ctx)).be.null

  it 'should properly calculate before/after except', ->
    @dateTimeBeforeExcept.exec(@ctx).should.eql new Interval(new DateTime(2012, 1, 1, 0, 0, 0, 0), new DateTime(2012, 4, 1, 0, 0, 0, 0))

  it 'should properly calculate meets except', ->
    x = @dateTimeHalfInterval.exec(@ctx)
    y = @dateTimeMeetsExcept.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping except', ->
    x = @dateTimeHalfInterval.exec(@ctx)
    y = @dateTimeOverlapsExcept.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by except', ->
    should(@dateTimeBeginsExcept.exec(@ctx)).be.null

  it 'should properly calculate includes/included by except', ->
    should(@dateTimeDuringExcept.exec(@ctx)).be.null

  it 'should properly calculate ends/ended by except', ->
    should(@dateTimeEndsExcept.exec(@ctx)).be.null

# TODO
# it 'should properly handle imprecision', ->

describe 'IntegerIntervalIntersect', ->
   it 'should properly calculate sameAs intersect', ->
    x = @intSameAsIntersect.exec(@ctx)
    y = @intFullInterval.exec(@ctx)
    x.equals(y).should.be.true

  it 'should properly calculate before/after intersect', ->
    should(@intBeforeIntersect.exec(@ctx)).be.null

  it 'should properly calculate meets intersect', ->
    x = @intMeetsInterval.exec(@ctx)
    y = @intMeetsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping intersect', ->
    x = @intOverlapsInterval.exec(@ctx)
    y = @intOverlapsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by intersect', ->
    x = @intBeginsInterval.exec(@ctx)
    y = @intBeginsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate includes/included by intersect', ->
    x = @intDuringInterval.exec(@ctx)
    y = @intDuringIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate ends/ended by intersect', ->
    x = @intEndsInterval.exec(@ctx)
    y = @intEndsIntersect.exec(@ctx)
    y.equals(x).should.be.true

describe 'DateTimeIntervalIntersect', ->
   it 'should properly calculate sameAs intersect', ->
    x = @dateTimeSameAsIntersect.exec(@ctx)
    y = @dateTimeFullInterval.exec(@ctx)
    x.equals(y).should.be.true

  it 'should properly calculate before/after intersect', ->
    should(@dateTimeBeforeIntersect.exec(@ctx)).be.null

  it 'should properly calculate meets intersect', ->
    x = @dateTimeMeetsInterval.exec(@ctx)
    y = @dateTimeMeetsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate left/right overlapping intersect', ->
    x = @dateTimeOverlapsInterval.exec(@ctx)
    y = @dateTimeOverlapsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate begins/begun by intersect', ->
    x = @dateTimeBeginsInterval.exec(@ctx)
    y = @dateTimeBeginsIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate includes/included by intersect', ->
    x = @dateTimeDuringInterval.exec(@ctx)
    y = @dateTimeDuringIntersect.exec(@ctx)
    y.equals(x).should.be.true

  it 'should properly calculate ends/ended by intersect', ->
    x = @dateTimeEndsInterval.exec(@ctx)
    y = @dateTimeEndsIntersect.exec(@ctx)
    y.equals(x).should.be.true
{Repository} = require './repository'

{ p1, p2 } = require './patients'

describe 'In Age Demographic', ->
  , [ p1, p2 ]
    @results = @executor.withLibrary(@lib).exec_patient_context(@patientSource)

  it 'should have correct patient results', ->
    @results.patientResults['1'].InDemographic.should.equal false
    @results.patientResults['2'].InDemographic.should.equal true

  it 'should have empty population results', ->
    @results.populationResults.should.be.empty


describe 'Using CommonLib', ->
  , [ p1, p2 ], {}, {}, new Repository(data)

  it "should have using models defined", ->
    @lib.usings.should.not.be.empty
    @lib.usings.length.should.equal 1
    @lib.usings[0].name.should.equal "QUICK"

  it 'Should have included a library', ->
    @lib.includes.should.not.be.empty

  it "should be able to execute expression from included library", ->
    @results = @executor.withLibrary(@lib).exec_patient_context(@patientSource)
    @results.patientResults['1'].ID.should.equal false
    @results.patientResults['2'].ID.should.equal true
    @results.patientResults['2'].FuncTest.should.equal 7

{ArrayIndexOutOfBoundsException} = require '../../../lib/elm/overloaded'

describe 'List', ->
  it 'should execute to an array (ints)', ->
    @intList.exec(@ctx).should.eql [9, 7, 8]

  it 'should execute to an array (strings)', ->
    @stringList.exec(@ctx).should.eql ['a', 'bee', 'see']

  it 'should execute to an array (mixed)', ->
    @mixedList.exec(@ctx).should.eql [1, 'two', 3]

  it 'should execute to an empty array', ->
    @emptyList.exec(@ctx).should.eql []

describe 'Exists', ->
  it 'should return false for empty list', ->
    @emptyList.exec(@ctx).should.be.false

  it 'should return true for full list', ->
    @fullList.exec(@ctx).should.be.true

describe 'Equal', ->
  it 'should identify equal lists of integers', ->
    @equalIntList.exec(@ctx).should.be.true

  it 'should identify unequal lists of integers', ->
    @unequalIntList.exec(@ctx).should.be.false

  it 'should identify re-ordered lists of integers as unequal', ->
    @reverseIntList.exec(@ctx).should.be.false

  it 'should identify equal lists of strings', ->
    @equalStringList.exec(@ctx).should.be.true

  it 'should identify unequal lists of strings', ->
    @unequalStringList.exec(@ctx).should.be.false

  it 'should identify equal lists of tuples', ->
    @equalTupleList.exec(@ctx).should.be.true

  it 'should identify unequal lists of tuples', ->
    @unequalTupleList.exec(@ctx).should.be.false

describe 'NotEqual', ->
  it 'should identify equal lists of integers', ->
    @equalIntList.exec(@ctx).should.be.false

  it 'should identify unequal lists of integers', ->
    @unequalIntList.exec(@ctx).should.be.true

  it 'should identify re-ordered lists of integers as unequal', ->
    @reverseIntList.exec(@ctx).should.be.true

  it 'should identify equal lists of strings', ->
    @equalStringList.exec(@ctx).should.be.false

  it 'should identify unequal lists of strings', ->
    @unequalStringList.exec(@ctx).should.be.true

  it 'should identify equal lists of tuples', ->
    @equalTupleList.exec(@ctx).should.be.false

  it 'should identify unequal lists of tuples', ->
    @unequalTupleList.exec(@ctx).should.be.true

describe 'Union', ->
  it 'should union two lists to a single list', ->
    @oneToTen.exec(@ctx).should.eql [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

  it 'should maintain duplicate elements (according to CQL spec)', ->
    @oneToFiveOverlapped.exec(@ctx).should.eql [1, 2, 3, 4, 3, 4, 5]

  it 'should not fill in values in a disjoint union', ->
    @disjoint.exec(@ctx).should.eql [1, 2, 4, 5]

  it 'should return one list for multiple nested unions', ->
    @nestedToFifteen.exec(@ctx).should.eql [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

  it 'should return null if either arg is null', ->
    should(@unionNull.exec(@ctx)).be.null
    should(@nullUnion.exec(@ctx)).be.null

describe 'Except', ->
  it 'should remove items in second list', ->
    @exceptThreeFour.exec(@ctx).should.eql [1, 2, 5]

  it 'should not be commutative', ->
    @threeFourExcept.exec(@ctx).should.eql []

  it 'should remove items in second list regardless of order', ->
    @exceptFiveThree.exec(@ctx).should.eql [1, 2, 4]

  it 'should be a no-op when lists have no common items', ->
    @exceptNoOp.exec(@ctx).should.eql [1, 2, 3, 4, 5]

  it 'should remove all items when lists are the same', ->
    @exceptEverything.exec(@ctx).should.eql []

  it 'should be a no-op when second list is empty', ->
    @somethingExceptNothing.exec(@ctx).should.eql [1, 2, 3, 4, 5]

  it 'should be a no-op when first list is already empty', ->
    @nothingExceptSomething.exec(@ctx).should.eql []

  it 'should except lists of tuples', ->
    @exceptTuples.exec(@ctx).should.eql [{a: 1}, {a: 3}]

  it 'should return null if either arg is null', ->
    should(@exceptNull.exec(@ctx)).be.null
    should(@nullExcept.exec(@ctx)).be.null

describe 'Intersect', ->
  it 'should intersect two disjoint lists  to an empty list', ->
    @noIntersection.exec(@ctx).should.eql []

  it 'should intersect two lists with a single common element', ->
    @intersectOnFive.exec(@ctx).should.eql [5]

  it 'should intersect two lists with several common elements', ->
    @intersectOnEvens.exec(@ctx).should.eql [2, 4, 6, 8, 10]

  it 'should intersect two identical lists to the same list', ->
    @intersectOnAll.exec(@ctx).should.eql [1, 2, 3, 4, 5]

  it 'should intersect multiple lists to only those elements common across all', ->
    @nestedIntersects.exec(@ctx).should.eql [4, 5]

  it 'should intersect lists of tuples', ->
    @intersectTuples.exec(@ctx).should.eql [{a:1, b:'c'}, {a:2, b:'c'}]

  it 'should return null if either arg is null', ->
    should(@intersectNull.exec(@ctx)).be.null
    should(@nullIntersect.exec(@ctx)).be.null

describe 'IndexOf', ->
  it 'should return the correct 1-based index when an item is in the list', ->
    @indexOfSecond.exec(@ctx).should.equal 2

  it 'should work with complex types like tuples', ->
    @indexOfThirdTuple.exec(@ctx).should.equal 3

  it 'should return the first index when there are multiple matches', ->
    @multipleMatches.exec(@ctx).should.equal 4

  it 'should return 0 when the item is not in the list', ->
    @itemNotFound.exec(@ctx).should.equal 0

  it 'should return null if either arg is null', ->
    should(@nullList.exec(@ctx)).be.null
    should(@nullItem.exec(@ctx)).be.null

describe 'Indexer', ->
  it 'should return the correct item based on the 1-based index', ->
    @secondItem.exec(@ctx).should.equal 'b'

  it 'should throw ArrayIndexOutOfBoundsException when accessing index 0', ->
    try
      @zeroIndex.exec(@ctx)
      should.fail("Accessing index zero should throw ArrayIndexOutOfBoundsException")
    catch e
      e.should.be.instanceof ArrayIndexOutOfBoundsException

  it 'should throw ArrayIndexOutOfBoundsException when accessing out of bounds index', ->
    try
      @outOfBounds.exec(@ctx)
      should.fail("Accessing out of bounds index should throw ArrayIndexOutOfBoundsException")
    catch e
      e.should.be.instanceof ArrayIndexOutOfBoundsException

  it 'should return null if either arg is null', ->
    should(@nullList.exec(@ctx)).be.null
    should(@nullIndexer.exec(@ctx)).be.null

describe 'In', ->
  it 'should execute to true when item is in list', ->
    @isIn.exec(@ctx).should.be.true

  it 'should execute to false when item is not in list', ->
    @isNotIn.exec(@ctx).should.be.false

  it 'should execute to true when tuple is in list', ->
    @tupleIsIn.exec(@ctx).should.be.true

  it 'should execute to false when tuple is not in list', ->
    @tupleIsNotIn.exec(@ctx).should.be.false

  it 'should return null if either arg is null', ->
    should(@nullIn.exec(@ctx)).be.null
    should(@inNull.exec(@ctx)).be.null

describe 'Contains', ->
  it 'should execute to true when item is in list', ->
    @isIn.exec(@ctx).should.be.true

  it 'should execute to false when item is not in list', ->
    @isNotIn.exec(@ctx).should.be.false

  it 'should execute to true when tuple is in list', ->
    @tupleIsIn.exec(@ctx).should.be.true

  it 'should execute to false when tuple is not in list', ->
    @tupleIsNotIn.exec(@ctx).should.be.false

  it 'should return null if either arg is null', ->
    should(@nullIn.exec(@ctx)).be.null
    should(@inNull.exec(@ctx)).be.null

describe 'Includes', ->
  it 'should execute to true when sublist is in list', ->
    @isIncluded.exec(@ctx).should.be.true

  it 'should execute to true when sublist is in list in different order', ->
    @isIncludedReversed.exec(@ctx).should.be.true

  it 'should execute to true when lists are the same', ->
    @isSame.exec(@ctx).should.be.true

  it 'should execute to false when sublist is not in list', ->
    @isNotIncluded.exec(@ctx).should.be.false

  it 'should execute to true when tuple sublist is in list', ->
    @tuplesIncluded.exec(@ctx).should.be.true

  it 'should execute to false when tuple sublist is not in list', ->
    @tuplesNotIncluded.exec(@ctx).should.be.false

  it 'should return null if either arg is null', ->
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

describe 'IncludedIn', ->
  it 'should execute to true when sublist is in list', ->
    @isIncluded.exec(@ctx).should.be.true

  it 'should execute to true when sublist is in list in different order', ->
    @isIncludedReversed.exec(@ctx).should.be.true

  it 'should execute to true when lists are the same', ->
    @isSame.exec(@ctx).should.be.true

  it 'should execute to false when sublist is not in list', ->
    @isNotIncluded.exec(@ctx).should.be.false

  it 'should execute to true when tuple sublist is in list', ->
    @tuplesIncluded.exec(@ctx).should.be.true

  it 'should execute to false when tuple sublist is not in list', ->
    @tuplesNotIncluded.exec(@ctx).should.be.false

  it 'should return null if either arg is null', ->
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

describe 'ProperIncludes', ->
  it 'should execute to true when sublist is in list', ->
    @isIncluded.exec(@ctx).should.be.true

  it 'should execute to true when sublist is in list in different order', ->
    @isIncludedReversed.exec(@ctx).should.be.true

  it 'should execute to false when lists are the same', ->
    @isSame.exec(@ctx).should.be.false

  it 'should execute to false when sublist is not in list', ->
    @isNotIncluded.exec(@ctx).should.be.false

  it 'should execute to true when tuple sublist is in list', ->
    @tuplesIncluded.exec(@ctx).should.be.true

  it 'should execute to false when tuple sublist is not in list', ->
    @tuplesNotIncluded.exec(@ctx).should.be.false

  # TODO: Support for ProperContains
  it.skip 'should return null if either arg is null', ->
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

describe 'ProperIncludedIn', ->
  it 'should execute to true when sublist is in list', ->
    @isIncluded.exec(@ctx).should.be.true

  it 'should execute to true when sublist is in list in different order', ->
    @isIncludedReversed.exec(@ctx).should.be.true

  it 'should execute to false when lists are the same', ->
    @isSame.exec(@ctx).should.be.false

  it 'should execute to false when sublist is not in list', ->
    @isNotIncluded.exec(@ctx).should.be.false

  it 'should execute to true when tuple sublist is in list', ->
    @tuplesIncluded.exec(@ctx).should.be.true

  it 'should execute to false when tuple sublist is not in list', ->
    @tuplesNotIncluded.exec(@ctx).should.be.false

  it 'should return null if either arg is null', ->
    should(@nullIncluded.exec(@ctx)).be.null
    should(@nullIncludes.exec(@ctx)).be.null

describe 'Expand', ->
  it 'should expand a list of lists', ->
    @listOfLists.exec(@ctx).should.eql [1, 2, 3, 4, 5, 6, 7, 8, 9, 9, 8, 7, 6, 5, 4, 3, 2, 1]

  it 'should return null for a null list', ->
    should(@nullValue.exec(@ctx)).be.null

describe 'Distinct', ->
  it 'should remove duplicates', ->
    @lotsOfDups.exec(@ctx).should.eql [1, 2, 3, 4, 5]

  it 'should do nothing to an already distinct array', ->
    @noDups.exec(@ctx).should.eql [2, 4, 6, 8, 10]

describe 'First', ->
  it 'should get first of a list of numbers', ->
    @numbers.exec(@ctx).should.equal 1

  it 'should get first of a list of letters', ->
    @letters.exec(@ctx).should.equal 'a'

  it 'should get first of a list of lists', ->
    @lists.exec(@ctx).should.eql ['a','b','c']

  it 'should get first of a list of tuples', ->
    @tuples.exec(@ctx).should.eql { a: 1, b: 2, c: 3 }

  it 'should get first of a list of unordered numbers', ->
    @unordered.exec(@ctx).should.equal 3

  it 'should return null for an empty list', ->
    should(@empty.exec(@ctx)).be.null

  it 'should return null for an empty list', ->
    should(@nullValue.exec(@ctx)).be.null

describe 'Last', ->
  it 'should get last of a list of numbers', ->
    @numbers.exec(@ctx).should.equal 4

  it 'should get last of a list of letters', ->
    @letters.exec(@ctx).should.equal 'c'

  it 'should get last of a list of lists', ->
    @lists.exec(@ctx).should.eql ['d','e','f']

  it 'should get last of a list of tuples', ->
    @tuples.exec(@ctx).should.eql { a: 24, b: 25, c: 26 }

  it 'should get last of a list of unordered numbers', ->
    @unordered.exec(@ctx).should.equal 2

  it 'should return null for an empty list', ->
    should(@empty.exec(@ctx)).be.null

  it 'should return null for an empty list', ->
    should(@nullValue.exec(@ctx)).be.null

describe 'Length', ->
  it 'should get length of a list of numbers', ->
    @numbers.exec(@ctx).should.equal 5

  it 'should get length of a list of lists', ->
    @lists.exec(@ctx).should.equal 4

  it 'should get length of a list of tuples', ->
    @tuples.exec(@ctx).should.equal 2

  it 'should get length of an empty list', ->
    @empty.exec(@ctx).should.equal 0

  it 'should return null for an empty list', ->
    should(@nullValue.exec(@ctx)).be.null

describe 'Literal', ->
  it 'should convert true to boolean true', ->
    @boolTrue.value.should.be.true

  it 'should execute true as true', ->
    @boolTrue.exec(@ctx).should.be.true

  it 'should convert false to boolean false', ->
    @boolFalse.value.should.be.false

  it 'should execute false as false', ->
    @boolFalse.exec(@ctx).should.be.false

  it 'should convert 1 to int 1', ->
    @intOne.value.should.equal 1

  it 'should execute 1 as 1', ->
    @intOne.exec(@ctx).should.equal 1

  it 'should convert .1 to decimal .1', ->
    @decimalTenth.value.should.equal 0.1

  it 'should execute .1 as .1', ->
    @decimalTenth.exec(@ctx).should.equal 0.1

  it 'should convert \'true\' to string \'true\'', ->
    @stringTrue.value.should.equal 'true'

  it 'should execute \'true\' as \'true\'', ->
    @stringTrue.exec(@ctx).should.equal 'true'

describe 'And', ->
  it 'should execute true and...', ->
    @tT.exec(@ctx).should.be.true
    @tF.exec(@ctx).should.be.false
    should(@tN.exec(@ctx)).be.null

  it 'should execute false and...', ->
    @fF.exec(@ctx).should.be.false
    @fT.exec(@ctx).should.be.false
    @fN.exec(@ctx).should.be.false

  it 'should execute null and...', ->
    should(@nN.exec(@ctx)).be.null
    should(@nT.exec(@ctx)).be.null
    @nF.exec(@ctx).should.be.false

describe 'Or', ->
  it 'should execute true or...', ->
    @tT.exec(@ctx).should.be.true
    @tF.exec(@ctx).should.be.true
    @tN.exec(@ctx).should.be.true

  it 'should execute false or...', ->
    @fF.exec(@ctx).should.be.false
    @fT.exec(@ctx).should.be.true
    should(@fN.exec(@ctx)).be.null

  it 'should execute null or...', ->
    should(@nN.exec(@ctx)).be.null
    @nT.exec(@ctx).should.be.true
    should(@nF.exec(@ctx)).be.null

describe 'Not', ->
  it 'should execute not true as false', ->
    @notTrue.exec(@ctx).should.be.false

  it 'should execute not false as true', ->
    @notFalse.exec(@ctx).should.be.true

  it 'should execute not null as null', ->
    should(@notNull.exec(@ctx)).be.null

describe 'XOr', ->
  it 'should execute true xor...', ->
    @tT.exec(@ctx).should.be.false
    @tF.exec(@ctx).should.be.true
    should(@tN.exec(@ctx)).be.null

  it 'should execute false xor...', ->
    @fF.exec(@ctx).should.be.false
    @fT.exec(@ctx).should.be.true
    should(@fN.exec(@ctx)).be.null

  it 'should execute null xor...', ->
    should(@nN.exec(@ctx)).be.null
    should(@nT.exec(@ctx)).be.null
    should(@nF.exec(@ctx)).be.null

describe 'Nil', ->
  it 'should execute as null', ->
    should(@nil.exec(@ctx)).be.null

describe 'IsNull', ->
  it 'should detect that null is null', ->
    @nullIsNull.exec(@ctx).should.be.true

  it 'should detect that null variable is null', ->
    @nullVarIsNull.exec(@ctx).should.be.true

  it 'should detect that string is not null', ->
    @stringIsNull.exec(@ctx).should.be.false

  it 'should detect that non-null variable is not null', ->
    @nonNullVarIsNull.exec(@ctx).should.be.false

describe.skip 'Coalesce', ->
  it 'should return first non-null when leading args are null', ->
    @nullNullHelloNullWorld.exec(@ctx).should.equal 'Hello'

  it 'should return first arg when it is non-null', ->
    @fooNullNullBar.exec(@ctx).should.equal 'Foo'

  it 'should return null when they are all null', ->
    should(@allNull.exec(@ctx)).be.null

describe 'ParameterDef', ->
    @param = @lib.parameters.MeasureYear

  it 'should have a name', ->
    @param.name.should.equal 'MeasureYear'

  it 'should execute to default value', ->
    @param.exec(@ctx).should.equal 2012

  it 'should execute to provided value', ->
    @param.exec(@ctx.withParameters { MeasureYear: 2013 }).should.equal 2013

  it 'should work with typed int parameters', ->
    intParam = @lib.parameters.IntParameter
    intParam.exec(@ctx.withParameters { IntParameter: 17 }).should.equal 17

  it 'should work with typed list parameters', ->
    listParam = @lib.parameters.ListParameter
    listParam.exec(@ctx.withParameters { ListParameter: {'a', 'b', 'c'} }).should.eql {'a', 'b', 'c'}

  it 'should work with typed tuple parameters', ->
    tupleParam = @lib.parameters.TupleParameter
    v = { a : 1, b : 'bee', c : true, d : [10, 9, 8], e : { f : 'eff', g : false}}
    tupleParam.exec(@ctx.withParameters { TupleParameter: v }).should.eql v

describe 'ParameterRef', ->
  it 'should have a name', ->
    @foo.name.should.equal 'FooP'

  it 'should execute to default value', ->
    @foo.exec(@ctx).should.equal 'Bar'

  it 'should execute to provided value', ->
    @foo.exec(@ctx.withParameters { FooP: 'Bah' }).should.equal 'Bah'
vsets = require './valuesets'
{ p1 } = require './patients'

describe 'DateRangeOptimizedQuery', ->
  , [ p1 ], vsets

  it 'should find encounters performed during the MP', ->
    e = @encountersDuringMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

  it 'should find ambulatory encounters performed during the MP', ->
    e = @ambulatoryEncountersDuringMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

  it 'should find ambulatory encounter performances included in the MP', ->
    e = @ambulatoryEncountersIncludedInMP.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

describe.skip 'IncludesQuery', ->
  , [ p1 ], vsets

  it 'should find ambulatory encounter performances included in the MP', ->
    e = @mPIncludedAmbulatoryEncounters.exec(@ctx)
    e.should.have.length(1)
    e[0].id().should.equal 'http://cqframework.org/3/5'

describe 'MultiSourceQuery', ->
  , [ p1 ], vsets

  it 'should find all Encounters performed and Conditions', ->
    e = @msQuery.exec(@ctx)
    e.should.have.length(6)

  it.skip 'should find encounters performed during the MP and All conditions', ->
    e = @msQueryWhere.exec(@ctx)
    e.should.have.length(2)

  it.skip 'should be able to filter items in the where clause', ->
    e = @msQueryWhere2.exec(@ctx)
    e.should.have.length(1)

describe.skip 'QueryRelationship', ->
  , [ p1 ]

  it 'should be able to filter items with a with clause', ->
    e = @withQuery.exec(@ctx)
    e.should.have.length(3)

  it 'with clause should filter out items not available', ->
    e = @withQuery2.exec(@ctx)
    e.should.have.length(0)

  it 'should be able to filter items with a without clause', ->
    e = @withOutQuery.exec(@ctx)
    e.should.have.length(3)

  it 'without clause should be able to filter items with a without clause', ->
    e = @withOutQuery2.exec(@ctx)
    e.should.have.length(0)

describe 'QueryDefine', ->
  , [ p1 ]

  it 'should be able to define a variable in a query and use it', ->
    e = @query.exec(@ctx)
    e.should.have.length(3)
    e[0]["a"].should.equal  e[0]["E"]
    e[1]["a"].should.equal  e[1]["E"]
    e[2]["a"].should.equal  e[2]["E"]

describe 'Tuple', ->
  , [ p1 ]

  it 'should be able to return tuple from a query', ->
    e = @query.exec(@ctx)
    e.should.have.length(3)

describe 'Sorting', ->
  , [ p1 ]

  it 'should be able to sort by a tuple field asc' , ->
    e = @tupleAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[2].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[2].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnTupleAsc.exec(@ctx)
    e.should.have.length(3)
    e[0].E.id().should.equal "http://cqframework.org/3/1"
    e[1].E.id().should.equal  "http://cqframework.org/3/3"
    e[2].E.id().should.equal  "http://cqframework.org/3/5"

  it 'should be able to sort by a tuple field desc', ->
    e = @tupleDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[0].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].id().should.equal "http://cqframework.org/3/1"
    e[1].id().should.equal  "http://cqframework.org/3/3"
    e[0].id().should.equal  "http://cqframework.org/3/5"

    e = @tupleReturnTupleDesc.exec(@ctx)
    e.should.have.length(3)
    e[2].E.id().should.equal "http://cqframework.org/3/1"
    e[1].E.id().should.equal  "http://cqframework.org/3/3"
    e[0].E.id().should.equal  "http://cqframework.org/3/5"

  it 'should be able to sort by number asc' , ->
    e = @numberAsc.exec(@ctx)
    e.should.eql [0, 3, 5, 6, 7, 8, 9]

  it 'should be able to sort by number desc' , ->
    e = @numberDesc.exec(@ctx)
    e.should.eql [9, 8, 7, 6, 5, 3, 0]

  it 'should be able to sort by string asc' , ->
    @stringAsc.exec(@ctx).should.eql ['change', 'dont', 'jenny', 'number', 'your']
    @stringReturnAsc.exec(@ctx).should.eql ['change', 'dont', 'jenny', 'number', 'your']

  it 'should be able to sort by string desc' , ->
    @stringDesc.exec(@ctx).should.eql ['your', 'number', 'jenny', 'dont', 'change']
    @stringReturnDesc.exec(@ctx).should.eql ['your', 'number', 'jenny', 'dont', 'change']

describe 'Distinct', ->
  it 'should return distinct by default' , ->
    @defaultNumbers.exec(@ctx).should.eql [1, 2, 3, 4]
    @defaultStrings.exec(@ctx).should.eql ['foo', 'bar', 'baz']
    @defaultTuples.exec(@ctx).should.eql [{a: 1, b:2}, {a: 2, b:3}]

  it 'should eliminate duplicates when returning distinct' , ->
    @distinctNumbers.exec(@ctx).should.eql [1, 2, 3, 4]
    @distinctStrings.exec(@ctx).should.eql ['foo', 'bar', 'baz']
    @distinctTuples.exec(@ctx).should.eql [{a: 1, b:2}, {a: 2, b:3}]

  it 'should not eliminate duplicates when returning all' , ->
    @allNumbers.exec(@ctx).should.eql [1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 3, 3, 3, 2, 2, 1]
    @allStrings.exec(@ctx).should.eql ['foo', 'bar', 'baz', 'bar']
    @allTuples.exec(@ctx).should.eql [{a: 1, b:2}, {a: 2, b:3}, {a: 1, b:2}]

describe 'ExpressionDef', ->
    @def = @lib.expressions.Foo

  it 'should have a name', ->
    @def.name.should.equal 'Foo'

  it 'should have the correct context', ->
    @def.context.should.equal 'Patient'

  it 'should execute to its value', ->
    @def.exec(@ctx).should.equal 'Bar'

describe 'ExpressionRef', ->
  it 'should have a name', ->
    @foo.name.should.equal 'Life'

  it 'should execute to expression value', ->
    @foo.exec(@ctx).should.equal 42

describe 'FunctionDefinitions', ->
  it 'should be able to define and use a simple function' , ->
    e = @testValue.exec(@ctx)
    e.should.equal 3
str = require '../../../lib/elm/string'
{ArrayIndexOutOfBoundsException} = require '../../../lib/elm/overloaded'

describe 'Concat', ->
  it.skip 'should be a Concat', ->
    @helloWorld.should.be.an.instanceOf(str.Concat)
    @helloWorldVariables.should.be.an.instanceOf(str.Concat)

  it 'should concat two strings', ->
    @helloWorld.exec(@ctx).should.equal 'HelloWorld'

  it 'should concat multiple strings', ->
    @sentence.exec(@ctx).should.equal 'The quick brown fox jumps over the lazy dog.'

  it 'should return null when an arg is null', ->
    should(@concatNull.exec(@ctx)).be.null

  it 'should concat variables', ->
    @helloWorldVariables.exec(@ctx).should.equal 'HelloWorld'

describe 'Combine', ->
  it.skip 'should be a Combine', ->
    @separator.should.be.an.instanceOf(str.Combine)

  it 'should combine strings with no separator', ->
    @noSeparator.exec(@ctx).should.equal 'abcdefghijkl'

  it 'should combine strings with a separator', ->
    @separator.exec(@ctx).should.equal 'abc;def;ghi;jkl'

  it 'should return null when the list is null', ->
    should(@combineNull.exec(@ctx)).be.null

  it 'should return null when an item in the list is null', ->
    should(@combineNullItem.exec(@ctx)).be.null

describe 'Split', ->
  it.skip 'should be a Split', ->
    @commaSeparated.should.be.an.instanceOf(str.Split)

  it 'should split strings on comma', ->
    @commaSeparated.exec(@ctx).should.eql ['a','b','c','','1','2','3']

  it 'should return single-item array when separator is not used', ->
    @separatorNotUsed.exec(@ctx).should.eql ['a,b,c,,1,2,3']

  it 'should return null when separating null', ->
    should(@separateNull.exec(@ctx)).be.null

  # TODO: Verify this assumption
  it 'should return null when the separator is null', ->
    should(@separateUsingNull.exec(@ctx)).be.null

describe 'Length', ->
  it.skip 'should be a Length', ->
    @elevenLetters.should.be.an.instanceOf(str.Length)

  it 'should count letters in string', ->
    @elevenLetters.exec(@ctx).should.equal 11

  it 'should return null when string is null', ->
    should(@nullString.exec(@ctx)).be.null

describe 'Upper', ->
  it.skip 'should be an Upper', ->
    @upperC.should.be.an.instanceOf(str.Upper)

  it 'should convert lower to upper', ->
    @lowerC.exec(@ctx).should.equal 'ABCDEFG123'

  it 'should leave upper as upper', ->
    @upperC.exec(@ctx).should.equal 'ABCDEFG123'

  it 'should convert camel to upper', ->
    @camelC.exec(@ctx).should.equal 'ABCDEFG123'

  it 'should return null when uppering null', ->
    should(@nullString.exec(@ctx)).be.null

describe 'Lower', ->
  it.skip 'should be a Lower', ->
    @lowerC.should.be.an.instanceOf(str.Lower)

  it 'should leave lower as lower', ->
    @lowerC.exec(@ctx).should.equal 'abcdefg123'

  it 'should convert upper to lower', ->
    @upperC.exec(@ctx).should.equal 'abcdefg123'

  it 'should convert camel to lower', ->
    @camelC.exec(@ctx).should.equal 'abcdefg123'

  it 'should return null when lowering null', ->
    should(@nullString.exec(@ctx)).be.null

# TODO: Verify behavior since its different than JS
describe 'Indexer', ->
  it 'should get letter at index', ->
    @helloWorldSix.exec(@ctx).should.equal 'W'

  it 'should error on index 0 (out of bounds)', ->
    try
      @helloWorldZero.exec(@ctx)
      should.fail()
    catch e
      e.should.be.instanceof ArrayIndexOutOfBoundsException

  it 'should error on index 20 (out of bounds)', ->
    try
      @helloWorldTwenty.exec(@ctx)
      should.fail()
    catch e
      e.should.be.instanceof ArrayIndexOutOfBoundsException

  it 'should return null when string is null', ->
    should(@nullString.exec(@ctx)).be.null

  it 'should return null when index is null', ->
    should(@nullIndex.exec(@ctx)).be.null

describe 'PositionOf', ->
  it.skip 'should be a PositionOf', ->
    @found.should.be.an.instanceOf(str.Pos)

  it 'should return 1-based position', ->
    @found.exec(@ctx).should.equal 3

  it 'should return 0 when not found', ->
    @notFound.exec(@ctx).should.equal 0

  it 'should return null when pattern is null', ->
    should(@nullPattern.exec(@ctx)).be.null

  it 'should return null when string is null', ->
    should(@nullString.exec(@ctx)).be.null

describe 'Substring', ->
  it.skip 'should be a Substring', ->
    @world.should.be.an.instanceOf(str.Substring)

  it 'should get substring to end', ->
    @world.exec(@ctx).should.equal 'World'

  it 'should get substring with length', ->
    @or.exec(@ctx).should.equal 'or'

  it 'should get substring with zero length', ->
    @zeroLength.exec(@ctx).should.equal ''

  it 'should error on index 0 (out of bounds)', ->
    try
      @startTooLow.exec(@ctx)
      should.fail()
    catch e
      # Good!

  it 'should error on too much length (out of bounds)', ->
    try
      @tooMuchLength.exec(@ctx)
      should.fail()
    catch e
      # Good!

  it 'should error on negative length', ->
    try
      @negativeLength.exec(@ctx)
      should.fail()
    catch e
      # Good!

  it 'should return null when string is null', ->
    should(@nullString.exec(@ctx)).be.null

  it 'should return null when start is null', ->
    should(@nullStart.exec(@ctx)).be.null

describe 'Tuple', ->
  it 'should be able to define a tuple', ->
    e = @tup.exec(@ctx)
    e["a"].should.equal 1
    e["b"].should.equal 2
