'use strict';
(function(exports) {

  exports.module = {
    meta: {
      id: 'Test'
    },

    constructor: function(system) {
      var r = system.runtime,
        _ = system._,
        pub = {},
        priv = {},
        context = {}
      pub['TestExpression'] =
        priv['TestExpression'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Add", [{
              simple: true,
              value: 2,
              $type: "Integer"
            }, {
              simple: true,
              value: 3,
              $type: "Integer"
            }]))
        }

      pub['Boolean1'] =
        priv['Boolean1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: true,
            $type: "Boolean"
          })
        }

      pub['Boolean2'] =
        priv['Boolean2'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: false,
            $type: "Boolean"
          })
        }

      pub['String1'] =
        priv['String1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: "blue",
            $type: "String"
          })
        }

      pub['String2'] =
        priv['String2'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: "red",
            $type: "String"
          })
        }

      pub['String3'] =
        priv['String3'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: "I'm worried",
            $type: "String"
          })
        }

      pub['DateTime1'] =
        priv['DateTime1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            $type: "DateTime",
            value: {
              year: 2014,
              month: 1,
              day: 25,
              timezoneOffset: system.now.utcOffset()
            }
          })
        }

      pub['DateTime2'] =
        priv['DateTime2'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            $type: "DateTime",
            value: {
              year: 2014,
              month: 1,
              day: 25,
              hour: 14,
              minute: 30,
              second: 14,
              millisecond: 559,
              timezoneOffset: system.now.utcOffset()
            }
          })
        }

      pub['Time1'] =
        priv['Time1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            $type: "Time",
            value: {
              hour: 12,
              minute: 0,
              second: 0,
              millisecond: 0,
              timezoneOffset: 0.0
            }
          })
        }

      pub['Time2'] =
        priv['Time2'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            $type: "Time",
            value: {
              hour: 14,
              minute: 30,
              second: 14,
              millisecond: 559,
              timezoneOffset: 0.0
            }
          })
        }

      pub['Quantity1'] =
        priv['Quantity1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            value: 6,
            unit: "gm/cm3",
            $type: "Quantity"
          })
        }

      pub['Quantity2'] =
        priv['Quantity2'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            value: 80,
            unit: "mm[Hg]",
            $type: "Quantity"
          })
        }

      pub['Quantity3'] =
        priv['Quantity3'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            value: 3,
            unit: "months",
            $type: "Quantity"
          })
        }

      pub['Concept1'] =
        priv['Concept1'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            display: "Type B viral hepatitis",
            code: [{
              "code": "66071002",
              "system": {
                "name": "SNOMED-CT:2014"
              }
            }, {
              "code": "B18.1",
              "system": {
                "name": "ICD-9-CM:2014"
              }
            }]
          })
        }

      pub['X'] =
        priv['X'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: 4,
            $type: "Integer"
          })
        }

      pub['Y'] =
        priv['Y'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: 5,
            $type: "Integer"
          })
        }

      pub['Z'] =
        priv['Z'] = function() {
          // type: undefined
          // deps: undefined
          return ({
            simple: true,
            value: 5.4,
            $type: "Decimal"
          })
        }

      pub['XPlusY'] =
        priv['XPlusY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Add", [priv['X'](), priv['Y']()]))
        }

      pub['XMinusY'] =
        priv['XMinusY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Subtract", [priv['X'](), priv['Y']()]))
        }

      pub['XTimesY'] =
        priv['XTimesY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Multiply", [priv['X'](), priv['Y']()]))
        }

      pub['XDivideY'] =
        priv['XDivideY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Divide", [r.Do("Convert", ["Decimal", priv['X']()]), r.Do("Convert", ["Decimal", priv['Y']()])]))
        }

      pub['XLogY'] =
        priv['XLogY'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Log", [r.Do("Convert", ["Decimal", {
              simple: true,
              value: 1000,
              $type: "Integer"
            }]), r.Do("Convert", ["Decimal", {
              simple: true,
              value: 10,
              $type: "Integer"
            }])]))
        }

      pub['XModuloY'] =
        priv['XModuloY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Modulo", [priv['X'](), priv['Y']()]))
        }

      pub['XPowerY'] =
        priv['XPowerY'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
          return ( //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
            r.Do("Power", [priv['X'](), priv['Y']()]))
        }

      pub['Precedence_24'] =
        priv['Precedence_24'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"expression":"X"},"1":{"expression":"Y"}},"1":{"expression":"X"}}
          return ( //deps: {"0":{"0":{"expression":"X"},"1":{"expression":"Y"}},"1":{"expression":"X"}}
            r.Do("Add", [ //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
              r.Do("Multiply", [priv['X'](), priv['Y']()]), priv['X']()
            ]))
        }

      pub['Precedence_24a'] =
        priv['Precedence_24a'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"0":{"expression":"X"},"1":{"expression":"Y"}}}
          return ( //deps: {"0":{"expression":"X"},"1":{"0":{"expression":"X"},"1":{"expression":"Y"}}}
            r.Do("Add", [priv['X'](), //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
              r.Do("Multiply", [priv['X'](), priv['Y']()])
            ]))
        }

      pub['Precedence_45'] =
        priv['Precedence_45'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"expression":"Y"}},"1":{"0":{"expression":"X"},"1":{"expression":"Y"}}}
          return ( //deps: {"0":{"0":{"expression":"Y"}},"1":{"0":{"expression":"X"},"1":{"expression":"Y"}}}
            r.Do("Add", [ //deps: {"0":{"expression":"Y"}}
              r.Do("Power", [priv['Y'](), {
                simple: true,
                value: 2,
                $type: "Integer"
              }]), //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
              r.Do("Multiply", [priv['X'](), priv['Y']()])
            ]))
        }

      pub['Precedence_45a'] =
        priv['Precedence_45a'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"expression":"X"},"1":{"expression":"Y"}},"1":{"0":{"expression":"Y"}}}
          return ( //deps: {"0":{"0":{"expression":"X"},"1":{"expression":"Y"}},"1":{"0":{"expression":"Y"}}}
            r.Do("Add", [ //deps: {"0":{"expression":"X"},"1":{"expression":"Y"}}
              r.Do("Multiply", [priv['X'](), priv['Y']()]), //deps: {"0":{"expression":"Y"}}
              r.Do("Power", [priv['Y'](), {
                simple: true,
                value: 2,
                $type: "Integer"
              }])
            ]))
        }

      pub['Precedence_36'] =
        priv['Precedence_36'] = function() {
          // type: undefined
          // deps: {"0":{"expression":"X"},"1":{"0":{"expression":"Y"},"1":{"expression":"X"}}}
          return ( //deps: {"0":{"expression":"X"},"1":{"0":{"expression":"Y"},"1":{"expression":"X"}}}
            r.Do("Multiply", [priv['X'](), //deps: {"0":{"expression":"Y"},"1":{"expression":"X"}}
              r.Do("Add", [priv['Y'](), priv['X']()])
            ]))
        }

      pub['And_IsTrue'] =
        priv['And_IsTrue'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['And_IsFalse'] =
        priv['And_IsFalse'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, {
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['And_IsFalse1'] =
        priv['And_IsFalse1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['And_IsAlsoFalse'] =
        priv['And_IsAlsoFalse'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }, null]))
        }

      pub['And_IsAlsoFalse1'] =
        priv['And_IsAlsoFalse1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [null, {
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['And_IsNull'] =
        priv['And_IsNull'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, null]))
        }

      pub['And_IsNull1'] =
        priv['And_IsNull1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("And", [null, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['NotIsTrue'] =
        priv['NotIsTrue'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Not", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['NotIsFalse'] =
        priv['NotIsFalse'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Not", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['NotIsNull'] =
        priv['NotIsNull'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Not", [null]))
        }

      pub['OrIsTrue'] =
        priv['OrIsTrue'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, {
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['OrIsTrue1'] =
        priv['OrIsTrue1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['OrIsTrue2'] =
        priv['OrIsTrue2'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['OrIsAlsoTrue'] =
        priv['OrIsAlsoTrue'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: true,
              $type: "Boolean"
            }, null]))
        }

      pub['OrIsAlsoTrue1'] =
        priv['OrIsAlsoTrue1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [null, {
              simple: true,
              value: true,
              $type: "Boolean"
            }]))
        }

      pub['OrIsFalse'] =
        priv['OrIsFalse'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }, {
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['OrIsNull'] =
        priv['OrIsNull'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [{
              simple: true,
              value: false,
              $type: "Boolean"
            }, null]))
        }

      pub['OrIsNull1'] =
        priv['OrIsNull1'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Or", [null, {
              simple: true,
              value: false,
              $type: "Boolean"
            }]))
        }

      pub['Three'] =
        priv['Three'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Add", [{
              simple: true,
              value: 1,
              $type: "Integer"
            }, {
              simple: true,
              value: 2,
              $type: "Integer"
            }]))
        }

      pub['IntList'] =
        priv['IntList'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            {
              $type: 'List',
              value: [{
                simple: true,
                value: 9,
                $type: "Integer"
              }, {
                simple: true,
                value: 7,
                $type: "Integer"
              }, {
                simple: true,
                value: 8,
                $type: "Integer"
              }]
            })
        }

      pub['StringList'] =
        priv['StringList'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            {
              $type: 'List',
              value: [{
                simple: true,
                value: "a",
                $type: "String"
              }, {
                simple: true,
                value: "bee",
                $type: "String"
              }, {
                simple: true,
                value: "see",
                $type: "String"
              }]
            })
        }

      pub['MixedList'] =
        priv['MixedList'] = function() {
          // type: undefined
          // deps: {"2":{"expression":"Three"}}
          return ( //deps: {"2":{"expression":"Three"}}
            {
              $type: 'List',
              value: [{
                simple: true,
                value: 1,
                $type: "Integer"
              }, {
                simple: true,
                value: "two",
                $type: "String"
              }, priv['Three']()]
            })
        }

      pub['EmptyList'] =
        priv['EmptyList'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            {
              $type: 'List',
              value: []
            })
        }

      pub['Exists_EmptyList_false'] =
        priv['Exists_EmptyList_false'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Exists", [ //deps: {}
              {
                $type: 'List',
                value: []
              }
            ]))
        }

      pub['Exists_FullList_true'] =
        priv['Exists_FullList_true'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Exists", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['EqualIntList'] =
        priv['EqualIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Equal", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['UnequalIntList'] =
        priv['UnequalIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Equal", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ReverseIntList'] =
        priv['ReverseIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Equal", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['EqualStringList'] =
        priv['EqualStringList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Equal", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "hello",
                  $type: "String"
                }, {
                  simple: true,
                  value: "world",
                  $type: "String"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "hello",
                  $type: "String"
                }, {
                  simple: true,
                  value: "world",
                  $type: "String"
                }]
              }
            ]))
        }

      pub['UnequalStringList'] =
        priv['UnequalStringList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Equal", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "hello",
                  $type: "String"
                }, {
                  simple: true,
                  value: "world",
                  $type: "String"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "foo",
                  $type: "String"
                }, {
                  simple: true,
                  value: "bar",
                  $type: "String"
                }]
              }
            ]))
        }

      pub['EqualTupleList'] =
        priv['EqualTupleList'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
          return ( //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
            r.Do("Equal", [ //deps: {"0":{"1":{}},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,{}]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: //deps: [null]
                      {
                        $type: 'Tuple',
                        value: {
                          c: {
                            simple: true,
                            value: 1,
                            $type: "Integer"
                          }
                        }
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      x: {
                        simple: true,
                        value: "y",
                        $type: "String"
                      },
                      z: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{"1":{}},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,{}]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: //deps: [null]
                      {
                        $type: 'Tuple',
                        value: {
                          c: {
                            simple: true,
                            value: 1,
                            $type: "Integer"
                          }
                        }
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      x: {
                        simple: true,
                        value: "y",
                        $type: "String"
                      },
                      z: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['UnequalTupleList'] =
        priv['UnequalTupleList'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
          return ( //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
            r.Do("Equal", [ //deps: {"0":{"1":{}},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,{}]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: //deps: [null]
                      {
                        $type: 'Tuple',
                        value: {
                          c: {
                            simple: true,
                            value: 1,
                            $type: "Integer"
                          }
                        }
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      x: {
                        simple: true,
                        value: "y",
                        $type: "String"
                      },
                      z: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{"1":{}},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,{}]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: //deps: [null]
                      {
                        $type: 'Tuple',
                        value: {
                          c: //deps: undefined
                            r.Do("Negate", [{
                            simple: true,
                            value: 1,
                            $type: "Integer"
                          }])
                        }
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      x: {
                        simple: true,
                        value: "y",
                        $type: "String"
                      },
                      z: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['not_EqualIntList'] =
        priv['not_EqualIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Not", [ //deps: {"0":{},"1":{}}
              r.Do("Equal", [ //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }]
                }, //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }]
                }
              ])
            ]))
        }

      pub['not_UnequalIntList'] =
        priv['not_UnequalIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Not", [ //deps: {"0":{},"1":{}}
              r.Do("Equal", [ //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }]
                }, //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }]
                }
              ])
            ]))
        }

      pub['not_ReverseIntList'] =
        priv['not_ReverseIntList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Not", [ //deps: {"0":{},"1":{}}
              r.Do("Equal", [ //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }]
                }, //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 2,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  }]
                }
              ])
            ]))
        }

      pub['not_EqualStringList'] =
        priv['not_EqualStringList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Not", [ //deps: {"0":{},"1":{}}
              r.Do("Equal", [ //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: "hello",
                    $type: "String"
                  }, {
                    simple: true,
                    value: "world",
                    $type: "String"
                  }]
                }, //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: "hello",
                    $type: "String"
                  }, {
                    simple: true,
                    value: "world",
                    $type: "String"
                  }]
                }
              ])
            ]))
        }

      pub['not_UnequalStringList'] =
        priv['not_UnequalStringList'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Not", [ //deps: {"0":{},"1":{}}
              r.Do("Equal", [ //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: "hello",
                    $type: "String"
                  }, {
                    simple: true,
                    value: "world",
                    $type: "String"
                  }]
                }, //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: "foo",
                    $type: "String"
                  }, {
                    simple: true,
                    value: "bar",
                    $type: "String"
                  }]
                }
              ])
            ]))
        }

      pub['not_EqualTupleList'] =
        priv['not_EqualTupleList'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
          return ( //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
            r.Do("Not", [ //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
              r.Do("Equal", [ //deps: {"0":{"1":{}},"1":{}}
                {
                  $type: 'List',
                  value: [ //deps: [null,{}]
                    {
                      $type: 'Tuple',
                      value: {
                        a: {
                          simple: true,
                          value: 1,
                          $type: "Integer"
                        },
                        b: //deps: [null]
                        {
                          $type: 'Tuple',
                          value: {
                            c: {
                              simple: true,
                              value: 1,
                              $type: "Integer"
                            }
                          }
                        }
                      }
                    }, //deps: [null,null]
                    {
                      $type: 'Tuple',
                      value: {
                        x: {
                          simple: true,
                          value: "y",
                          $type: "String"
                        },
                        z: {
                          simple: true,
                          value: 2,
                          $type: "Integer"
                        }
                      }
                    }
                  ]
                }, //deps: {"0":{"1":{}},"1":{}}
                {
                  $type: 'List',
                  value: [ //deps: [null,{}]
                    {
                      $type: 'Tuple',
                      value: {
                        a: {
                          simple: true,
                          value: 1,
                          $type: "Integer"
                        },
                        b: //deps: [null]
                        {
                          $type: 'Tuple',
                          value: {
                            c: {
                              simple: true,
                              value: 1,
                              $type: "Integer"
                            }
                          }
                        }
                      }
                    }, //deps: [null,null]
                    {
                      $type: 'Tuple',
                      value: {
                        x: {
                          simple: true,
                          value: "y",
                          $type: "String"
                        },
                        z: {
                          simple: true,
                          value: 2,
                          $type: "Integer"
                        }
                      }
                    }
                  ]
                }
              ])
            ]))
        }

      pub['not_UnequalTupleList'] =
        priv['not_UnequalTupleList'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
          return ( //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
            r.Do("Not", [ //deps: {"0":{"0":{"1":{}},"1":{}},"1":{"0":{"1":{}},"1":{}}}
              r.Do("Equal", [ //deps: {"0":{"1":{}},"1":{}}
                {
                  $type: 'List',
                  value: [ //deps: [null,{}]
                    {
                      $type: 'Tuple',
                      value: {
                        a: {
                          simple: true,
                          value: 1,
                          $type: "Integer"
                        },
                        b: //deps: [null]
                        {
                          $type: 'Tuple',
                          value: {
                            c: {
                              simple: true,
                              value: 1,
                              $type: "Integer"
                            }
                          }
                        }
                      }
                    }, //deps: [null,null]
                    {
                      $type: 'Tuple',
                      value: {
                        x: {
                          simple: true,
                          value: "y",
                          $type: "String"
                        },
                        z: {
                          simple: true,
                          value: 2,
                          $type: "Integer"
                        }
                      }
                    }
                  ]
                }, //deps: {"0":{"1":{}},"1":{}}
                {
                  $type: 'List',
                  value: [ //deps: [null,{}]
                    {
                      $type: 'Tuple',
                      value: {
                        a: {
                          simple: true,
                          value: 1,
                          $type: "Integer"
                        },
                        b: //deps: [null]
                        {
                          $type: 'Tuple',
                          value: {
                            c: //deps: undefined
                              r.Do("Negate", [{
                              simple: true,
                              value: 1,
                              $type: "Integer"
                            }])
                          }
                        }
                      }
                    }, //deps: [null,null]
                    {
                      $type: 'Tuple',
                      value: {
                        x: {
                          simple: true,
                          value: "y",
                          $type: "String"
                        },
                        z: {
                          simple: true,
                          value: 2,
                          $type: "Integer"
                        }
                      }
                    }
                  ]
                }
              ])
            ]))
        }

      pub['OneToTen'] =
        priv['OneToTen'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Union", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 7,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 9,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['OneToFiveOverlapped'] =
        priv['OneToFiveOverlapped'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Union", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Disjoint'] =
        priv['Disjoint'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Union", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['NestedToFifteen'] =
        priv['NestedToFifteen'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"0":{"0":{},"1":{}},"1":{}},"1":{}},"1":{}}
          return ( //deps: {"0":{"0":{"0":{"0":{},"1":{}},"1":{}},"1":{}},"1":{}}
            r.Do("Union", [ //deps: {"0":{"0":{"0":{},"1":{}},"1":{}},"1":{}}
              r.Do("Union", [ //deps: {"0":{"0":{},"1":{}},"1":{}}
                r.Do("Union", [ //deps: {"0":{},"1":{}}
                  r.Do("Union", [ //deps: {}
                    {
                      $type: 'List',
                      value: [{
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      }, {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }, {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }]
                    }, //deps: {}
                    {
                      $type: 'List',
                      value: [{
                        simple: true,
                        value: 4,
                        $type: "Integer"
                      }, {
                        simple: true,
                        value: 5,
                        $type: "Integer"
                      }, {
                        simple: true,
                        value: 6,
                        $type: "Integer"
                      }]
                    }
                  ]), //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 7,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 8,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 9,
                      $type: "Integer"
                    }]
                  }
                ]), //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 10,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 11,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 12,
                    $type: "Integer"
                  }]
                }
              ]), //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 13,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 14,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 15,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['NullUnion'] =
        priv['NullUnion'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("Union", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['UnionNull'] =
        priv['UnionNull'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Union", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['ExceptThreeFour'] =
        priv['ExceptThreeFour'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ThreeFourExcept'] =
        priv['ThreeFourExcept'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ExceptFiveThree'] =
        priv['ExceptFiveThree'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ExceptNoOp'] =
        priv['ExceptNoOp'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 7,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 9,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ExceptEverything'] =
        priv['ExceptEverything'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['SomethingExceptNothing'] =
        priv['SomethingExceptNothing'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: []
              }
            ]))
        }

      pub['NothingExceptSomething'] =
        priv['NothingExceptSomething'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: []
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['ExceptTuples'] =
        priv['ExceptTuples'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{}}}
            r.Do("Except", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{}}
              {
                $type: 'List',
                value: [ //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['ExceptNull'] =
        priv['ExceptNull'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Except", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['NullExcept'] =
        priv['NullExcept'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("Except", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['NoIntersection'] =
        priv['NoIntersection'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Intersect", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['IntersectOnFive'] =
        priv['IntersectOnFive'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Intersect", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 7,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['IntersectOnEvens'] =
        priv['IntersectOnEvens'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Intersect", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 7,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 9,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 0,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 12,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['IntersectOnAll'] =
        priv['IntersectOnAll'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Intersect", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['NestedIntersects'] =
        priv['NestedIntersects'] = function() {
          // type: undefined
          // deps: {"0":{"0":{"0":{},"1":{}},"1":{}},"1":{}}
          return ( //deps: {"0":{"0":{"0":{},"1":{}},"1":{}},"1":{}}
            r.Do("Intersect", [ //deps: {"0":{"0":{},"1":{}},"1":{}}
              r.Do("Intersect", [ //deps: {"0":{},"1":{}}
                r.Do("Intersect", [ //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 1,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 2,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 3,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 4,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 5,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 2,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 3,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 4,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 5,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 6,
                      $type: "Integer"
                    }]
                  }
                ]), //deps: {}
                {
                  $type: 'List',
                  value: [{
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 4,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 5,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 6,
                    $type: "Integer"
                  }, {
                    simple: true,
                    value: 7,
                    $type: "Integer"
                  }]
                }
              ]), //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 7,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['IntersectTuples'] =
        priv['IntersectTuples'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{},"2":{},"3":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{},"2":{},"3":{}}}
            r.Do("Intersect", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{},"2":{},"3":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 5,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "e",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['NullIntersect'] =
        priv['NullIntersect'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("Intersect", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['IntersectNull'] =
        priv['IntersectNull'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Intersect", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['IndexOfSecond'] =
        priv['IndexOfSecond'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("IndexOf", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: "b",
                $type: "String"
              }
            ]))
        }

      pub['IndexOfThirdTuple'] =
        priv['IndexOfThirdTuple'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
            r.Do("IndexOf", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }, //deps: [null]
              {
                $type: 'Tuple',
                value: {
                  a: {
                    simple: true,
                    value: 3,
                    $type: "Integer"
                  }
                }
              }
            ]))
        }

      pub['IndexOf_MultipleMatches'] =
        priv['IndexOf_MultipleMatches'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("IndexOf", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }, {
                  simple: true,
                  value: "e",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: "d",
                $type: "String"
              }
            ]))
        }

      pub['IndexOf_ItemNotFound'] =
        priv['IndexOf_ItemNotFound'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("IndexOf", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: "d",
                $type: "String"
              }
            ]))
        }

      pub['IndexOf_NullList'] =
        priv['IndexOf_NullList'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("IndexOf", [null, {
              simple: true,
              value: "a",
              $type: "String"
            }]))
        }

      pub['IndexOf_NullItem'] =
        priv['IndexOf_NullItem'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("IndexOf", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }]
              },
              null
            ]))
        }

      pub['SecondItem'] =
        priv['SecondItem'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Indexer", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: 2,
                $type: "Integer"
              }
            ]))
        }

      pub['ZeroIndex'] =
        priv['ZeroIndex'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Indexer", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: 0,
                $type: "Integer"
              }
            ]))
        }

      pub['OutOfBounds'] =
        priv['OutOfBounds'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Indexer", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              }, {
                simple: true,
                value: 100,
                $type: "Integer"
              }
            ]))
        }

      pub['NullList'] =
        priv['NullList'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Indexer", [null, {
              simple: true,
              value: 1,
              $type: "Integer"
            }]))
        }

      pub['NullIndexer'] =
        priv['NullIndexer'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Indexer", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }, {
                  simple: true,
                  value: "d",
                  $type: "String"
                }]
              },
              null
            ]))
        }

      pub['In_IsIn'] =
        priv['In_IsIn'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("In", [{
                simple: true,
                value: 4,
                $type: "Integer"
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['In_IsNotIn'] =
        priv['In_IsNotIn'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("In", [{
                simple: true,
                value: 4,
                $type: "Integer"
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['In_TupleIsIn'] =
        priv['In_TupleIsIn'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{},"1":{"0":{},"1":{},"2":{}}}
            r.Do("In", [ //deps: [null,null]
              {
                $type: 'Tuple',
                value: {
                  a: {
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  },
                  b: {
                    simple: true,
                    value: "c",
                    $type: "String"
                  }
                }
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['In_TupleIsNotIn'] =
        priv['In_TupleIsNotIn'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{},"1":{"0":{},"1":{},"2":{}}}
            r.Do("In", [ //deps: [null,null]
              {
                $type: 'Tuple',
                value: {
                  a: {
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  },
                  b: {
                    simple: true,
                    value: "c",
                    $type: "String"
                  }
                }
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['In_NullIn'] =
        priv['In_NullIn'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("In", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['In_Null'] =
        priv['In_Null'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("In", [{
              simple: true,
              value: 1,
              $type: "Integer"
            }, null]))
        }

      pub['C_IsIn'] =
        priv['C_IsIn'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Contains", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, {
                simple: true,
                value: 4,
                $type: "Integer"
              }
            ]))
        }

      pub['C_IsNotIn'] =
        priv['C_IsNotIn'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Contains", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }, {
                simple: true,
                value: 4,
                $type: "Integer"
              }
            ]))
        }

      pub['C_TupleIsIn'] =
        priv['C_TupleIsIn'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
            r.Do("Contains", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: [null,null]
              {
                $type: 'Tuple',
                value: {
                  a: {
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  },
                  b: {
                    simple: true,
                    value: "c",
                    $type: "String"
                  }
                }
              }
            ]))
        }

      pub['C_TupleIsNotIn'] =
        priv['C_TupleIsNotIn'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{}}
            r.Do("Contains", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: [null,null]
              {
                $type: 'Tuple',
                value: {
                  a: {
                    simple: true,
                    value: 1,
                    $type: "Integer"
                  },
                  b: {
                    simple: true,
                    value: "c",
                    $type: "String"
                  }
                }
              }
            ]))
        }

      pub['C_InNull'] =
        priv['C_InNull'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Contains", [null, {
              simple: true,
              value: 1,
              $type: "Integer"
            }]))
        }

      pub['C_NullIn'] =
        priv['C_NullIn'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Contains", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['I_IsIncluded'] =
        priv['I_IsIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Includes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['I_IsIncludedReversed'] =
        priv['I_IsIncludedReversed'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Includes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['I_IsSame'] =
        priv['I_IsSame'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Includes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['I_IsNotIncluded'] =
        priv['I_IsNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Includes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['I_TuplesIncluded'] =
        priv['I_TuplesIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
            r.Do("Includes", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['I_TuplesNotIncluded'] =
        priv['I_TuplesNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
            r.Do("Includes", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['I_NullIncluded'] =
        priv['I_NullIncluded'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("Contains", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['I_NullIncludes'] =
        priv['I_NullIncludes'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("Includes", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['II_IsIncluded'] =
        priv['II_IsIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("IncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['II_IsIncludedReversed'] =
        priv['II_IsIncludedReversed'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("IncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['II_IsSame'] =
        priv['II_IsSame'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("IncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['II_IsNotIncluded'] =
        priv['II_IsNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("IncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['II_TuplesIncluded'] =
        priv['II_TuplesIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
            r.Do("IncludedIn", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['II_TuplesNotIncluded'] =
        priv['II_TuplesNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
            r.Do("IncludedIn", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['II_NullIncludes'] =
        priv['II_NullIncludes'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("IncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['II_NullIncluded'] =
        priv['II_NullIncluded'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("In", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PI_IsIncluded'] =
        priv['PI_IsIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PI_IsIncludedReversed'] =
        priv['PI_IsIncludedReversed'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PI_IsSame'] =
        priv['PI_IsSame'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PI_IsNotIncluded'] =
        priv['PI_IsNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludes", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PI_TuplesIncluded'] =
        priv['PI_TuplesIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
            r.Do("ProperIncludes", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['PI_TuplesNotIncluded'] =
        priv['PI_TuplesNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
          return ( //deps: {"0":{"0":{},"1":{},"2":{}},"1":{"0":{},"1":{}}}
            r.Do("ProperIncludes", [ //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['PI_NullIncluded'] =
        priv['PI_NullIncluded'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("ProperContains", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['PI_NullIncludes'] =
        priv['PI_NullIncludes'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("ProperIncludes", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PII_IsIncluded'] =
        priv['PII_IsIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PII_IsIncludedReversed'] =
        priv['PII_IsIncludedReversed'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PII_IsSame'] =
        priv['PII_IsSame'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PII_IsNotIncluded'] =
        priv['PII_IsNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("ProperIncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }]
              }, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['PII_TuplesIncluded'] =
        priv['PII_TuplesIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
            r.Do("ProperIncludedIn", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['PII_TuplesNotIncluded'] =
        priv['PII_TuplesNotIncluded'] = function() {
          // type: undefined
          // deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
          return ( //deps: {"0":{"0":{},"1":{}},"1":{"0":{},"1":{},"2":{}}}
            r.Do("ProperIncludedIn", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }, //deps: {"0":{},"1":{},"2":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "d",
                        $type: "String"
                      }
                    }
                  }, //deps: [null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: "c",
                        $type: "String"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['PII_NullIncludes'] =
        priv['PII_NullIncludes'] = function() {
          // type: undefined
          // deps: {"0":{}}
          return ( //deps: {"0":{}}
            r.Do("ProperIncludedIn", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              },
              null
            ]))
        }

      pub['PII_NullIncluded'] =
        priv['PII_NullIncluded'] = function() {
          // type: undefined
          // deps: {"1":{}}
          return ( //deps: {"1":{}}
            r.Do("ProperIncludedIn", [null, //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Expand_ListOfLists'] =
        priv['Expand_ListOfLists'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{},"2":{},"3":{},"4":{},"5":{}}
          return ( //deps: {"0":{},"1":{},"2":{},"3":{},"4":{},"5":{}}
            r.Do("Expand", [ //deps: {"0":{},"1":{},"2":{},"3":{},"4":{},"5":{}}
              {
                $type: 'List',
                value: [ //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 1,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 2,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 3,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 4,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 5,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 6,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 7,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 8,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 9,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 9,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 8,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 7,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 6,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 5,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 4,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 3,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 2,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 1,
                      $type: "Integer"
                    }]
                  }
                ]
              }
            ]))
        }

      pub['Expand_NullValue'] =
        priv['Expand_NullValue'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Expand", [null]))
        }

      pub['Distinct_LotsOfDups'] =
        priv['Distinct_LotsOfDups'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Distinct", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 5,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Distinct_NoDups'] =
        priv['Distinct_NoDups'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Distinct", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['First_Numbers'] =
        priv['First_Numbers'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("First", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['First_Letters'] =
        priv['First_Letters'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("First", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }]
              }
            ]))
        }

      pub['First_Lists'] =
        priv['First_Lists'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("First", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: "a",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "b",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "c",
                      $type: "String"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: "d",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "e",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "f",
                      $type: "String"
                    }]
                  }
                ]
              }
            ]))
        }

      pub['First_Tuples'] =
        priv['First_Tuples'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("First", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 24,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 25,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 26,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['First_Unordered'] =
        priv['First_Unordered'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("First", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['First_Empty'] =
        priv['First_Empty'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("First", [ //deps: {}
              {
                $type: 'List',
                value: []
              }
            ]))
        }

      pub['First_NullValue'] =
        priv['First_NullValue'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("First", [null]))
        }

      pub['Last_Numbers'] =
        priv['Last_Numbers'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Last", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Last_Letters'] =
        priv['Last_Letters'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Last", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: "a",
                  $type: "String"
                }, {
                  simple: true,
                  value: "b",
                  $type: "String"
                }, {
                  simple: true,
                  value: "c",
                  $type: "String"
                }]
              }
            ]))
        }

      pub['Last_Lists'] =
        priv['Last_Lists'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Last", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: "a",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "b",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "c",
                      $type: "String"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: "d",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "e",
                      $type: "String"
                    }, {
                      simple: true,
                      value: "f",
                      $type: "String"
                    }]
                  }
                ]
              }
            ]))
        }

      pub['Last_Tuples'] =
        priv['Last_Tuples'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Last", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 24,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 25,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 26,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['Last_Unordered'] =
        priv['Last_Unordered'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Last", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 3,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 1,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Last_Empty'] =
        priv['Last_Empty'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Last", [ //deps: {}
              {
                $type: 'List',
                value: []
              }
            ]))
        }

      pub['Last_NullValue'] =
        priv['Last_NullValue'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Last", [null]))
        }

      pub['Length_Numbers'] =
        priv['Length_Numbers'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Length", [ //deps: {}
              {
                $type: 'List',
                value: [{
                  simple: true,
                  value: 2,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 4,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 6,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 8,
                  $type: "Integer"
                }, {
                  simple: true,
                  value: 10,
                  $type: "Integer"
                }]
              }
            ]))
        }

      pub['Length_Lists'] =
        priv['Length_Lists'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{},"2":{},"3":{}}
          return ( //deps: {"0":{},"1":{},"2":{},"3":{}}
            r.Do("Length", [ //deps: {"0":{},"1":{},"2":{},"3":{}}
              {
                $type: 'List',
                value: [ //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 1,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 2,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 3,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 4,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 5,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 6,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 7,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 8,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 9,
                      $type: "Integer"
                    }]
                  }, //deps: {}
                  {
                    $type: 'List',
                    value: [{
                      simple: true,
                      value: 10,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 11,
                      $type: "Integer"
                    }, {
                      simple: true,
                      value: 12,
                      $type: "Integer"
                    }]
                  }
                ]
              }
            ]))
        }

      pub['Length_Tuples'] =
        priv['Length_Tuples'] = function() {
          // type: undefined
          // deps: {"0":{},"1":{}}
          return ( //deps: {"0":{},"1":{}}
            r.Do("Length", [ //deps: {"0":{},"1":{}}
              {
                $type: 'List',
                value: [ //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 1,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 2,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 3,
                        $type: "Integer"
                      }
                    }
                  }, //deps: [null,null,null]
                  {
                    $type: 'Tuple',
                    value: {
                      a: {
                        simple: true,
                        value: 24,
                        $type: "Integer"
                      },
                      b: {
                        simple: true,
                        value: 25,
                        $type: "Integer"
                      },
                      c: {
                        simple: true,
                        value: 26,
                        $type: "Integer"
                      }
                    }
                  }
                ]
              }
            ]))
        }

      pub['Length_Empty'] =
        priv['Length_Empty'] = function() {
          // type: undefined
          // deps: {}
          return ( //deps: {}
            r.Do("Length", [ //deps: {}
              {
                $type: 'List',
                value: []
              }
            ]))
        }

      pub['Length_NullValue'] =
        priv['Length_NullValue'] = function() {
          // type: undefined
          // deps: undefined
          return ( //deps: undefined
            r.Do("Length", [null]))
        }

      return pub

    }
  }
})(typeof exports === 'undefined' ? this['Test'] = {} : exports)