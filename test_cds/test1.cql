library Test

codesystem "SNOMED-CT:2014": '2.16.840.1.113883.6.96' version '2031-09'
codesystem "ICD-9-CM:2014": '2.16.840.1.113883.6.103' version '2014'

valueset "Acute Pharyngitis": '2.16.840.1.113883.3.464.1003.102.12.1011'

parameter MeasurementPeriod Interval<DateTime>

define TestExpression: 2 + 3
define Boolean1: true
define Boolean2: false
define String1: 'blue'
define String2: 'red'
define String3: 'I''m worried'
define DateTime1: @2014-01-25
define DateTime2: @2014-01-25T14:30:14.559
define Time1: @T12:00:00.0Z
define Time2: @T14:30:14.559-07:00
define Quantity1: 6 'gm/cm3'
define Quantity2: 80 'mm[Hg]'
define Quantity3: 3 months

define Concept1: Concept
{
   Code '66071002' from "SNOMED-CT:2014",
         Code 'B18.1' from "ICD-9-CM:2014"
} display 'Type B viral hepatitis'

define X: 4
define Y: 5
define Z: 5.4
define X: 5.4

define XPlusY: X + Y
define XMinusY: X - Y
define XTimesY: X * Y
define XDivideY: X / Y
define XLogY: Log(1000, 10)
define XModuloY: X mod Y
define XPowerY: X ^ Y

define Precedence_24: X * Y + X
define Precedence_24a: X + X * Y
define Precedence_45: Y ^ 2 + X * Y
define Precedence_45a: X * Y + Y ^ 2
define Precedence_36: X * (Y + X)


define And_IsTrue: true and true
define And_IsFalse: true and false
define And_IsFalse1: false and true
define And_IsAlsoFalse: false and null
define And_IsAlsoFalse1: null and false
define And_IsNull: true and null
define And_IsNull1: null and true
define NotIsTrue: not false
define NotIsFalse: not true
define NotIsNull: not null
define OrIsTrue: true or false
define OrIsTrue1: false or true
define OrIsTrue2: true or true
define OrIsAlsoTrue: true or null
define OrIsAlsoTrue1: null or true
define OrIsFalse: false or false
define OrIsNull: false or null
define OrIsNull1: null or false

/*


// xor:
      TRUE FALSE NULL
TRUE  FALSE TRUE NULL
FALSE TRUE FALSE NULL
NULL   NULL NULL NULL

9.3.4 ToBoolean
Signature:
ToBoolean(argument String) Boolean
Description:
The ToBoolean operator converts the value of its argument to a Boolean value. The operator
accepts the following string representations:
String Representation
Boolean Value
true t yes y 1 => true
false f no n 0 => false

Note that the operator will ignore case when interpreting the string as a Boolean value.  If the input cannot be interpreted as a valid Boolean value, a run-time error is thrown.w If the argument is null , the result is null .

is null(argument Any) Boolean

The is null operator determines whether or not its argument evaluates to null . If the argument evaluates to null , the result is true ; otherwise, the result is false .

9.4.3 IsFalse

is false(argument Boolean) Boolean

The is false operator determines whether or not its argument evaluates to false . If the argument evaluates to false , the result is true ; otherwise, the result is false .

9.4.4 IsTrue

is true(argument Boolean) Boolean

The is true operator determines whether or not its argument evaluates to true . If the argument evaluates to true , the result is true ; otherwise, the result is false .
*/


// list tests taken from https://github.com/cqframework/clinical_quality_language/blob/master/Src/coffeescript/cql-execution/test/elm/list/data.cql

// @Test: List
define Three: 1 + 2
define IntList: { 9, 7, 8 }
define StringList: { 'a', 'bee', 'see' }
define MixedList: List<Any>{ 1, 'two', Three }
define EmptyList: List<Integer>{}


// @Test: Exists
define Exists_EmptyList_false: exists (List<Integer>{})
define Exists_FullList_true: exists ({ 1, 2, 3 })

// @Test: Equal
define EqualIntList: {1, 2, 3} = {1, 2, 3}
define UnequalIntList: {1, 2, 3} = {1, 2}
define ReverseIntList: {1, 2, 3} = {3, 2, 1}
define EqualStringList: {'hello', 'world'} = {'hello', 'world'}
define UnequalStringList: {'hello', 'world'} = {'foo', 'bar'}
define EqualTupleList: List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} } = List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} }
define UnequalTupleList:
List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} } =
List<Any>{ Tuple{a: 1, b: Tuple{c: -1}}, Tuple{x: 'y', z: 2} }

// @Test: NotEqual
define not_EqualIntList: {1, 2, 3} <> {1, 2, 3}
define not_UnequalIntList: {1, 2, 3} <> {1, 2}
define not_ReverseIntList: {1, 2, 3} <> {3, 2, 1}
define not_EqualStringList: {'hello', 'world'} <> {'hello', 'world'}
define not_UnequalStringList: {'hello', 'world'} <> {'foo', 'bar'}
define not_EqualTupleList: List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} } <> List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} }
define not_UnequalTupleList: List<Any>{ Tuple{a: 1, b: Tuple{c: 1}}, Tuple{x: 'y', z: 2} } <> List<Any>{ Tuple{a: 1, b: Tuple{c: -1}}, Tuple{x: 'y', z: 2} }

// @Test: Union
define OneToTen: {1, 2, 3, 4, 5} union {6, 7, 8, 9, 10}
define OneToFiveOverlapped: {1, 2, 3, 4} union {3, 4, 5}
define Disjoint: {1, 2} union {4, 5}
define NestedToFifteen: {1, 2, 3} union {4, 5, 6} union {7 ,8 , 9} union {10, 11, 12} union {13, 14, 15}
define NullUnion: null union {1, 2, 3}
define UnionNull: {1, 2, 3} union null

// @Test: Except
define ExceptThreeFour: {1, 2, 3, 4, 5} except {3, 4}
define ThreeFourExcept: {3, 4} except {1, 2, 3, 4, 5}
define ExceptFiveThree: {1, 2, 3, 4, 5} except {5, 3}
define ExceptNoOp: {1, 2, 3, 4, 5} except {6, 7, 8, 9, 10}
define ExceptEverything: {1, 2, 3, 4, 5} except {1, 2, 3, 4, 5}
define SomethingExceptNothing: {1, 2, 3, 4, 5} except List<Integer>{}
define NothingExceptSomething: List<Integer>{} except {1, 2, 3, 4, 5}
define ExceptTuples: {Tuple{a: 1}, Tuple{a: 2}, Tuple{a: 3}} except {Tuple{a: 2}}
define ExceptNull: {1, 2, 3, 4, 5} except null
define NullExcept: null except {1, 2, 3, 4, 5}

// @Test: Intersect
define NoIntersection: {1, 2, 3} intersect {4, 5, 6}
define IntersectOnFive: {4, 5, 6} intersect {1, 3, 5, 7}
define IntersectOnEvens: {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} intersect {0, 2, 4, 6, 8, 10, 12}
define IntersectOnAll: {1, 2, 3, 4, 5} intersect {5, 4, 3, 2, 1}
define NestedIntersects: {1, 2, 3, 4, 5} intersect {2, 3, 4, 5, 6} intersect {3, 4, 5, 6, 7} intersect {4, 5, 6, 7, 8}
define IntersectTuples: {Tuple{a:1, b:'d'}, Tuple{a:1, b:'c'}, Tuple{a:2, b:'c'}} intersect {Tuple{a:2, b:'d'}, Tuple{a:1, b:'c'}, Tuple{a:2, b:'c'}, Tuple{a:5, b:'e'}}
define NullIntersect: null intersect {1, 2, 3}
define IntersectNull: {1, 2, 3} intersect null

// @Test: IndexOf
define IndexOfSecond: IndexOf({'a', 'b', 'c', 'd'}, 'b')
define IndexOfThirdTuple: IndexOf({Tuple{a: 1}, Tuple{a: 2}, Tuple{a: 3}}, Tuple{a: 3})
define IndexOf_MultipleMatches: IndexOf({'a', 'b', 'c', 'd', 'd', 'e', 'd'}, 'd')
define IndexOf_ItemNotFound: IndexOf({'a', 'b', 'c'}, 'd')
define IndexOf_NullList: IndexOf(null, 'a')
define IndexOf_NullItem: IndexOf({'a', 'b', 'c'}, null)

// @Test: Indexer
define SecondItem: {'a', 'b', 'c', 'd'}[2]
define ZeroIndex: {'a', 'b', 'c', 'd'}[0]
define OutOfBounds: {'a', 'b', 'c', 'd'}[100]
define NullList: (null as List<String>)[1]
define NullIndexer: {'a', 'b', 'c', 'd'}[null]

// @Test: In
define In_IsIn: 4 in { 3, 4, 5 }
define In_IsNotIn: 4 in { 3, 5, 6 }
define In_TupleIsIn: Tuple{a: 1, b: 'c'} in {Tuple{a:1, b:'d'}, Tuple{a:1, b:'c'}, Tuple{a:2, b:'c'}}
define In_TupleIsNotIn: Tuple{a: 1, b: 'c'} in {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define In_NullIn: null in {1, 2, 3}
define In_Null: 1 in null

// @Test: Contains
define C_IsIn: { 3, 4, 5 } contains 4
define C_IsNotIn: { 3, 5, 6 } contains 4
define C_TupleIsIn: {Tuple{a:1, b:'d'}, Tuple{a:1, b:'c'}, Tuple{a:2, b:'c'}} contains Tuple{a: 1, b: 'c'}
define C_TupleIsNotIn: {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} contains Tuple{a: 1, b: 'c'}
define C_InNull: (null as List<Integer>) contains 1
define C_NullIn: {1, 2, 3} contains null

// @Test: Includes
define I_IsIncluded: {1, 2, 3, 4, 5} includes {2, 3, 4}
define I_IsIncludedReversed: {1, 2, 3, 4, 5} includes {4, 3, 2}
define I_IsSame: {1, 2, 3, 4, 5} includes {1, 2, 3, 4, 5}
define I_IsNotIncluded: {1, 2, 3, 4, 5} includes {4, 5, 6}
define I_TuplesIncluded: {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} includes {Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define I_TuplesNotIncluded: {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} includes {Tuple{a:2, b:'d'}, Tuple{a:3, b:'c'}}
define I_NullIncluded: {1, 2, 3, 4, 5} includes null
define I_NullIncludes: null includes {1, 2, 3, 4, 5}

// @Test: IncludedIn
define II_IsIncluded: {2, 3, 4} included in {1, 2, 3, 4, 5}
define II_IsIncludedReversed: {4, 3, 2} included in {1, 2, 3, 4, 5}
define II_IsSame: {1, 2, 3, 4, 5} included in {1, 2, 3, 4, 5}
define II_IsNotIncluded: {4, 5, 6} included in {1, 2, 3, 4, 5}
define II_TuplesIncluded: {Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} included in {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define II_TuplesNotIncluded: {Tuple{a:2, b:'d'}, Tuple{a:3, b:'c'}} included in {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define II_NullIncludes: {1, 2, 3, 4, 5} included in null
define II_NullIncluded: null included in {1, 2, 3, 4, 5}

// @Test: ProperIncludes
define PI_IsIncluded: {1, 2, 3, 4, 5} properly includes {2, 3, 4, 5}
define PI_IsIncludedReversed: {1, 2, 3, 4, 5} properly includes {5, 4, 3, 2}
define PI_IsSame: {1, 2, 3, 4, 5} properly includes {1, 2, 3, 4, 5}
define PI_IsNotIncluded: {1, 2, 3, 4, 5} properly includes {3, 4, 5, 6}
define PI_TuplesIncluded: {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} properly includes {Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define PI_TuplesNotIncluded: {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} properly includes {Tuple{a:2, b:'d'}, Tuple{a:3, b:'c'}}
define PI_NullIncluded: {1, 2, 3, 4, 5} properly includes null
define PI_NullIncludes: null properly includes {1, 2, 3, 4, 5}

// @Test: ProperIncludedIn
define PII_IsIncluded: {2, 3, 4} properly included in {1, 2, 3, 4, 5}
define PII_IsIncludedReversed: {4, 3, 2} properly included in {1, 2, 3, 4, 5}
define PII_IsSame: {1, 2, 3, 4, 5} properly included in {1, 2, 3, 4, 5}
define PII_IsNotIncluded: {4, 5, 6} properly included in {1, 2, 3, 4, 5}
define PII_TuplesIncluded: {Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}} properly included in {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define PII_TuplesNotIncluded: {Tuple{a:2, b:'d'}, Tuple{a:3, b:'c'}} properly included in {Tuple{a:1, b:'d'}, Tuple{a:2, b:'d'}, Tuple{a:2, b:'c'}}
define PII_NullIncludes: {1, 2, 3, 4, 5} properly included in null
define PII_NullIncluded: (null as List<Integer>) properly included in {1, 2, 3, 4, 5}

// @Test: Expand
define Expand_ListOfLists: expand { {1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {9, 8, 7, 6, 5}, {4}, {3, 2, 1} }
define Expand_NullValue: expand null

// @Test: Distinct
define Distinct_LotsOfDups: distinct {1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 5, 4, 3, 2, 1}
define Distinct_NoDups: distinct {2, 4, 6, 8, 10}

// @Test: First
define First_Numbers: First({1, 2, 3, 4})
define First_Letters: First({'a', 'b', 'c'})
define First_Lists: First({{'a','b','c'},{'d','e','f'}})
define First_Tuples: First({ Tuple{a: 1, b: 2, c: 3}, Tuple{a: 24, b: 25, c: 26} })
define First_Unordered: First({3, 1, 4, 2})
define First_Empty: First(List<Integer>{})
define First_NullValue: First(null as List<Integer>)

// @Test: Last
define Last_Numbers: Last({1, 2, 3, 4})
define Last_Letters: Last({'a', 'b', 'c'})
define Last_Lists: Last({{'a','b','c'},{'d','e','f'}})
define Last_Tuples: Last({ Tuple{a: 1, b: 2, c: 3}, Tuple{a: 24, b: 25, c: 26} })
define Last_Unordered: Last({3, 1, 4, 2})
define Last_Empty: Last(List<Integer>{})
define Last_NullValue: Last(null as List<Integer>)

// @Test: Length
define Length_Numbers: Length({2, 4, 6, 8, 10})
define Length_Lists: Length({ {1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}})
define Length_Tuples: Length({ Tuple{a: 1, b: 2, c: 3}, Tuple{a: 24, b: 25, c: 26} })
define Length_Empty: Length(List<Integer>{})
define Length_NullValue: Length(null as List<Integer>)
