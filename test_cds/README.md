These files were made thusly:

    cat Src/coffeescript/cql-execution/test/elm/*/test.coffee > ~/mater/mmla/node_app/cql/test_cds/rawtest.coffee
    cat Src/coffeescript/cql-execution/test/elm/*/*.cql > ~/mater/mmla/node_app/cql/test_cds/raw.cql


Copyright

The raw tests have been derived from the work of the ONC in this repository:

    https://github.com/cqframework/clinical_quality_language

As such they are partly Copyright 2014 The MITRE Corporation and licensed under the Apache License, Version 2.0 (the "License").

