# Goals

This project is an attempt to create a CQL-ELM to javascript (JS) transpiler that
  will play well with [FHIR](https://www.hl7.org/fhir)-based systems.

The result JS should be executable on node servers and in browsers by virtue of
allowing injection of FHIR and terminology services into compiled code.

The compiler will execute (initially at least) on node server and will function as:

* a nix style CLI tool piping text in and out to stdin and stdout
* a CLI tool that will read and write specified tools
* an standard NPM package with a clean API

The compiler should be simple enough to package into a browser compiler too.

As CQL runs in both a population and individual manner, it could add value in a
hospital context by facilitating audit and research in a population level as
well as allowing expression of individual decission support rules.

Because CQL is a HL7 standard the goal is to enable multiple hospitals to reuse and validate the same logic.

# Present status

This project started June 2016 and is in embryonic phase. Your help in making
the project a reality would be very welcome.

# Backgound

[HL7 Clinical Quality Language
(CQL)](http://wiki.hl7.org/index.php?title=Clinical_Quality_Language) allows
expression of clinical knowledge for both clinical decision support (CDS) and
clinical quality measurement (CQM).

The CQM specification lays out an intermediary step for CQM called ELM. ELM is
available in JSON (which this project uses) and XML (which this project does
not use). The universe is already blessed with and operational [CQL-ELM
converter](https://github.com/cqframework/clinical_quality_language/blob/master/Src/java/README.md)
written in java. In order to use this project on your own CQL files you will
need to use this (or a similar tool) to generate ELM files (in JSON format).

# Installation

This project aims to be a relatively standard node package so something like
this ought to get you going:

    npm install --save https://erichbschulz@bitbucket.org/erichbschulz/cql-compiler.git

This will make the `cql` command available to you which you can then run:

    cql <file>

Eg:

    cql ./CMS9v4_CQM.json

If you elect not install globally, (or are actively working on the compiler)
  then you can also directly call the CLI wrapper around the transpiler with
  this command:

    ./cli <file>

Eg:

    ./cli ./CMS9v4_CQM.json

# Using the transpiled code

(This is sometime from reality)

Compiled code should run in modern browsers and on node servers.

All dependancies and parameters are passed to the module constructor as a single parameter.

These parameters are passed in on construction:

* now - typically the result of calling `moment()` (injected to allow testing)
* parameters - any parameters required by the library

FHIR specific dependancies are (API to be defined):

* interface - implementation specific engine to support retrieve operations on FHIR (and other) resouces
* ontology - engine to support coding system operations
* runtime - common coded (will possibly require node and browser flavours)

External dependancies required for execution are:

* bluebird - handles asynchronous execution using promises
* lodash - general utility collection
* moment - time and date handling

These dependancies are all highly regarded and work in both browser and node.

    var module = require( './test_cds/test1').module
    var test1 = module.constructor(system)

### Interface object

The passed object should have a `Retrieve()` method that accepts a single object as a parameter:

* `type` - Type identifier for the requested object (eg "Patient", "Encounter")
* `typeUri` - Identify for the data model requested (eg "http://hl7.org/fhir")
* `codeProperty` - Property of requested objects to match with codes (?required if codes are specified
* `codes` - [optional] Definition of applicable codes for filter
* `dateProperty` - Property of requested objects to match with dateRange (?required if dateRange specified ?)
* `dateRange` - [optional] ?? array of [start, end] moment date
* `patient` - [optional] string identifier of patient

### Values

Internally, each value is handled as a POJO with these properties:

* value
* type
* simple boolean - if true indicates that the value can be quickly processed
* (more (eg for ranges))

# Development tools

The following are install globaly: mocha, [nodemon](https://www.npmjs.com/package/nodemon)

A full test-suite is on the cards (using mocha and chai).

But in interim this node command is useful to trigger a recompile on changing the compiler:

    nodemon --watch cql_compile.js bin/compile_on_change.js

(Note this watches the compiler not the code!)

Also:

    watch '/usr/local/bin/npm test' test_cds --wait=10 -d



or to active the debug logging in eg testfhir stream with color (`c`) and
breaking on first fail (`b`) and using "dot" reporter:

    DEBUG=testfhir watch '/usr/local/bin/npm test -- -c -b -R dot' --wait=10 -d

and, to recompile `.json` (ELM) to  `.js` files whenever the compiler changes:

    npm start

Full push-button automation will come soon.

The code will use the NPM [debug](https://www.npmjs.com/package/debug). To
activate console logging for debugging specific modules create an environment
variable `DEBUG` listing a comma or space delimeted list of modules to log:

    DEBUG="testfhir,module1"

### docker notes

These are specific to Erich's set up

    docker restart javao
    bin/compile_test.sh



# Useful related links

* The main CQL issue tracker is
  [here](https://jira.oncprojectracking.org/browse/CQLIT). (NB not for this
  project)
* [CQL Formatting and Usage Wiki](https://github.com/esacinc/CQL-Formatting-and-Usage-Wiki/wiki)

# Style

[Standard](https://github.com/feross/standard) style is the reference for this project.

# Licence

######The MIT License (MIT). Copyright (c) 2016, The Contributors.  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

######The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

######THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

